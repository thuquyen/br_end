import WaveQuangCao from "./Wave/WaveQuangCao";
import ShipController from "./Ship35";
import HandController from "./HandController";
import { Helper } from "./Helper";
import G from "./Global";

const { ccclass, property } = cc._decorator;

let GameState = {
    INIT: 0,
    READY: 1,
    PLAYING: 2,
    ENDGAME: 3
}

declare const window: any;

@ccclass
export default class SpaceShooterAd2 extends cc.Component {

    gameState: number = GameState.INIT;
    @property(cc.Node)
    logo: cc.Node = null;

    @property(cc.Node)
    endG: cc.Node = null;

    @property(cc.Node)
    txtMoveShip: cc.Node = null;

    @property(cc.Node)
    wave: cc.Node = null;

    @property(HandController)
    hand: HandController = null;

    @property(cc.Layout)
    bgLayout: cc.Layout = null;

    // @property(cc.Node)
    // buttonInstall: cc.Node = null;

    @property(cc.Node)
    titleMini: cc.Node = null;

    index: number = 1;

    nShip: number = 0;

    @property(ShipController)
    ship: ShipController = null;
    istouch: boolean = true;

    ironsource: boolean = false;

    // @property({ type: cc.AudioClip })
    // bgSound: cc.AudioClip = null;
    // temp: number = 0;

    check1st: boolean = false;
    check2st: boolean = false;
    check3st: boolean = false;

    nextBoss: number = 0;
    doneChoose: boolean = false;
    // @property(cc.Node)
    textFeatured: cc.Node = null;

    firstTouch: boolean = false;
    posX: number;
    posY: number;
    checkEnd: boolean = false;

    @property(cc.Node)
    tempBoss: cc.Node = null;
    isItemVip: boolean = false;
    ready: boolean = false;

    @property(cc.Node)
    cardEvo: cc.Node = null

    @property(cc.SpriteFrame)
    listCard: cc.SpriteFrame[] = [];

    @property(cc.Node)
    evoMenu: cc.Node = null;

    @property(cc.Node)
    boderCard: cc.Node[] = [];
    countChoose: number = 0;
    chooseDone: boolean = false;

    @property(cc.Node)
    cardChoose1: cc.Node = null;

    @property(cc.Node)
    cardChoose2: cc.Node = null;
    checkChooseCard_1: boolean = false;
    checkChooseCard_2: boolean = false;
    checkChooseCard_3: boolean = false;
    checkChooseCard_4: boolean = false;

    checkMerge: boolean = false;
    onLoad() {
        cc.view.enableAutoFullScreen(false);
        cc.director.getCollisionManager().enabled = false;

        this.txtMoveShip.zIndex = 100;
        this.ship.node.zIndex = 1;
        this.bgLayout.node.zIndex = 2;
        this.endG.zIndex = 3;
        this.cardChoose1.getComponent(cc.Sprite).spriteFrame = this.listCard[0]
        this.cardChoose2.getComponent(cc.Sprite).spriteFrame = this.listCard[0]
    }

    merger() {
        this.checkMerge = true;
        this.cardEvo.active = true;
        this.scheduleOnce(() => {
            G.startSpawnItem = true;
            this.wave.active = true;
            this.ship.node.runAction(cc.sequence(cc.moveTo(0.8, cc.v2(0, -280)).easing(cc.easeBackOut()), cc.callFunc(() => {
                this.node.getComponent("CanvasTouchManager").enabled = true;
                this.txtMoveShip.active = true;
                this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
                this.hand.node.opacity = 255;
                this.hand.node.zIndex = 12;
                this.posX = this.ship.node.x;
                this.posY = this.ship.node.y;
                this.gameState = GameState.READY;
                this.startGame();
                cc.director.getCollisionManager().enabled = true;
            })))
            // this.turnOnGuide();
        }, 3.5)
        // this.buttonInstall.active = true;
    }

    start() {
        // this.temp = cc.audioEngine.play(this.bgSound, true, 1);
        this.gameState = GameState.INIT;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);

        if (this.ironsource) {
            window.NUC.trigger.ready("ready")
        }

        // auto after 8s
        this.scheduleOnce(() => {
            if (!this.chooseDone) {
                this.card1();
                this.card3();
            }
        }, 6)
    }

    turnOnGuide() {
        this.txtMoveShip.active = true;
        this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
        this.hand.node.opacity = 255;
        this.hand.node.zIndex = 12;
    }

    onTouchStart(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                this.startGame();
                break;
            case GameState.PLAYING:
                if (!G.endG && !G.isBoss && !G.enableMouse) {
                    this.txtMoveShip.active = false;
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                    this.hand.stopSwipeAnim();
                    // this.unscheduleAllCallbacks();
                }
                break;
        }
    }

    onTouchMove(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                this.startGame();
                break;
            case GameState.PLAYING:
                if (!G.endG && !G.isBoss && !G.enableMouse) {
                    this.txtMoveShip.active = false;
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                    this.hand.stopSwipeAnim();
                    // this.unscheduleAllCallbacks();
                }
                break;
        }

    }

    onTouchEnd(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!G.endG && !G.isBoss) {
                    this.ship.setEnableBullet(false);
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
                }
                break;
        }
    }

    onTouchCancel(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!G.endG && !G.isBoss) {
                    this.ship.setEnableBullet(false);
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
                    console.log('cancel')
                }
                break;
        }
    }

    startGame() {
        if (this.ironsource)
            window.NUC.trigger.interaction();
        if (this.checkMerge) {
            this.gameState = GameState.PLAYING;
            this.hand.stopSwipeAnim();
            this.ship.setEnableMove(true);
        }

    }

    update(dt) {
        if (!this.chooseDone && this.countChoose == 2) {
            this.chooseDone = true;
            this.scheduleOnce(() => {
                this.evoMenu.destroy();
                this.merger();
            }, 0.5)

        }

        if (!this.check1st && G.totalE >= 24) {
            this.check1st = true;
            G.speedBg = -550;
            G.isBoss = true;
            this.ship.setEnableBullet(false);
            this.scheduleOnce(() => {
                this.tempBoss.active = true;
                G.speedBg = -100;
                this.tempBoss.runAction(cc.sequence(cc.moveTo(1, cc.v2(0, 230)),
                    cc.callFunc(() => {
                        G.hpBoss = true;
                        G.isBoss = false;
                        this.txtMoveShip.active = true;
                        this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
                    })
                ));
                this.ship.setEnableBullet(true);
            }, 1.2)
        }

        if (!G.endG && G.bossDie) {
            G.endG = true
            this.scheduleOnce(() => {
                this.endGame();
            }, 0.5)

        }
    }
    card1() {
        G.clickStart = true;
        if (!this.checkChooseCard_1) {
            // cc.audioEngine.playEffect(G.click, false);
            this.checkChooseCard_1 = true
            this.boderCard[0].active = true;
            if (this.countChoose == 0) {
                this.cardChoose1.getComponent(cc.Sprite).spriteFrame = this.listCard[0]
            }
            else if (this.countChoose == 1) {
                this.cardChoose2.getComponent(cc.Sprite).spriteFrame = this.listCard[0]
            }
            else
                return
            this.countChoose++;
        }
    }
    card2() {
        G.clickStart = true;
        if (!this.checkChooseCard_2) {
            // cc.audioEngine.playEffect(G.click, false);
            this.checkChooseCard_2 = true;
            this.boderCard[1].active = true;
            if (this.countChoose == 0) {
                this.cardChoose1.getComponent(cc.Sprite).spriteFrame = this.listCard[1]
            }
            else if (this.countChoose == 1) {
                this.cardChoose2.getComponent(cc.Sprite).spriteFrame = this.listCard[1]
            }
            else
                return
            this.countChoose++;
        }
    }
    card3() {
        G.clickStart = true;
        if (!this.checkChooseCard_3) {
            // cc.audioEngine.playEffect(G.click, false);
            this.checkChooseCard_3 = true;
            this.boderCard[2].active = true;
            if (this.countChoose == 0) {
                this.cardChoose1.getComponent(cc.Sprite).spriteFrame = this.listCard[2]
            }
            else if (this.countChoose == 1) {
                this.cardChoose2.getComponent(cc.Sprite).spriteFrame = this.listCard[2]
            }
            else
                return
            this.countChoose++;
        }
    }
    card4() {
        G.clickStart = true;
        if (!this.checkChooseCard_4) {
            // cc.audioEngine.playEffect(G.click, false);
            this.checkChooseCard_4 = true;
            this.boderCard[3].active = true;
            if (this.countChoose == 0) {
                this.cardChoose1.getComponent(cc.Sprite).spriteFrame = this.listCard[3]
            }
            else if (this.countChoose == 1) {
                this.cardChoose2.getComponent(cc.Sprite).spriteFrame = this.listCard[3]
            }
            else
                return
            this.countChoose++;
        }
    }
    endGame() {
        // cc.audioEngine.stop(this.temp);
        var findDeep = function (node, nodeName) {
            if (node.name == nodeName) return node;
            for (var i = 0; i < node.children.length; i++) {
                var res = findDeep(node.children[i], nodeName);
                if (res) {
                    res.destroy();
                }

                // return res;
            }
        }
        findDeep(cc.director.getScene(), "item_Upgrade");
        findDeep(cc.director.getScene(), "Bullet_medusa_3");
        findDeep(cc.director.getScene(), "Bullet_medusa_4");

        this.ship.node.runAction(cc.sequence(
            cc.moveBy(1.1, 0, 2000).easing(cc.easeBackIn()),
            cc.callFunc(() => {
                findDeep(cc.director.getScene(), "ship");
            }))
        )
        G.text = false;
        this.checkEnd = true;

        if (this.ironsource) {
            window.NUC.trigger.endGame('win')
        }
        this.scheduleOnce(() => {
            this.endG.active = true;
        }, 1)

        // if (this.buttonInstall)
        //     this.buttonInstall.active = false;
        this.titleMini.active = false;

        this.hand.node.active = false;
        this.txtMoveShip.active = false;
        this.ship.setEnableMove(false);
        this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    }
}