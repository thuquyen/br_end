
const { ccclass, property } = cc._decorator;

@ccclass
export default class BulletBeam extends cc.Component {
    @property(cc.Prefab)
    Bullet: cc.Prefab = null;
    // @property(cc.SpriteFrame)
    // SpritePurple: cc.SpriteFrame = null;
    @property(Number)
    fireTime: number = 2.5;

    onLoad() {
        this.moveAndSpawnerChildBullet();
    }
    moveAndSpawnerChildBullet() {
        // var Action = cc.sequence(cc.moveBy(this.fireTime, cc.v2(this.node.children[0].x, 3000)), cc.callFunc(() => {
        //     this.node.children[0].destroy();
        // }));
        // this.node.children[0].runAction(Action);
        // this.scheduleOnce(() => {
        //     var bullet = cc.instantiate(this.Bullet);
        //     bullet.parent = this.node.parent;
        //     bullet.x = this.node.x + 20;
        //     bullet.y = this.node.y - 20;
        // }, 0.02);
        // this.scheduleOnce(() => {
        //     var bullet = cc.instantiate(this.Bullet);
        //     bullet.parent = this.node.parent;
        //     bullet.x = this.node.x - 25;
        //     bullet.y = this.node.y - 40;
        // }, 0.04);
        this.scheduleOnce(() => {
            var bullet = cc.instantiate(this.Bullet);
            bullet.parent = this.node.parent;
            bullet.angle = 0;
            bullet.x = this.node.x;
            bullet.y = this.node.y;
        }, 0.03);
        this.scheduleOnce(() => {
            var bullet = cc.instantiate(this.Bullet);
            bullet.parent = this.node.parent;
            bullet.angle = -7;
            bullet.x = this.node.x - 7;
            bullet.y = this.node.y -4;
        }, 0.03);
        this.scheduleOnce(() => {
            var bullet = cc.instantiate(this.Bullet);
            bullet.parent = this.node.parent;
            bullet.angle = 5;
            bullet.x = this.node.x + 8;
            bullet.y = this.node.y - 4;
            // bullet.children[0].getComponent(cc.Sprite).spriteFrame = this.SpritePurple;
        }, 0.06);

        this.scheduleOnce(() => {
            var bullet = cc.instantiate(this.Bullet);
            bullet.parent = this.node.parent;
            bullet.angle = 8;
            bullet.x = this.node.x + 7;
            bullet.y = this.node.y +40;
        }, 0.09);
        this.scheduleOnce(() => {
            var bullet = cc.instantiate(this.Bullet);
            bullet.parent = this.node.parent;
            bullet.angle = -5;
            bullet.x = this.node.x - 7;
            bullet.y = this.node.y + 12;
            //bullet.children[0].getComponent(cc.Sprite).spriteFrame = this.SpritePurple;
        }, 0.12);
    }
}
