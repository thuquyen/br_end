const { ccclass, property } = cc._decorator;

@ccclass("BasePool")
export default class BasePool {

    private pool: cc.NodePool;

    @property(cc.Prefab)
    prefab: cc.Prefab = null;

    @property(Boolean)
    public reuse: boolean = true;

    constructor() {
        this.pool = new cc.NodePool();
        let initCount = 0;
        for (let i = 0; i < initCount; ++i) {
            let obj = cc.instantiate(this.prefab); // create node instance
            this.pool.put(obj); // populate your pool with put method
        }
    }

    createObject(parent: cc.Node): cc.Node {
        if (this.reuse) {
            let obj = null;
            if (this.pool.size() > 0) {
                obj = this.pool.get();
            } else {
                obj = cc.instantiate(this.prefab);
            }
            obj.parent = parent;
            return obj;
        } else {
            let obj = cc.instantiate(this.prefab);
            obj.parent = parent;
            return obj;
        }
    }

    killObject(obj: cc.Node) {
        if (this.reuse) {
            this.pool.put(obj);
        } else {
            obj.destroy();
        }
    }

    clearPool() {
        this.pool.clear();
    }

}