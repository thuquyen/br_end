const { ccclass, property } = cc._decorator;

@ccclass
export default class ScrollNew extends cc.Component {
    @property()
    speed = 80;
    @property()
    resetY = -1138;
    @property()
    YStart = 1138;
    @property(cc.Node)
    Bg1: cc.Node = null;
    @property(cc.Node)
    Bg2: cc.Node = null;
    Temp1: number = 0;
    Temp2: number = 0;
    update(dt) {
        var y = this.node.y;
        y -= this.speed * dt;
        if (y <= this.resetY) {
            if (this.Temp1 == 0) {
                this.Bg1.y = this.YStart;
                this.Temp1 = 1;
            }
            else {
                this.Bg1.y = 0;
                this.Temp1 = 0;
            }
            if (this.Temp2 == 0) {
                this.Bg2.y = 0;
                this.Temp2 = 1;
            }
            else {
                this.Bg2.y = this.YStart;
                this.Temp2 = 0;
            }
            y = 0;
        }
        this.node.y = y;
    }
}