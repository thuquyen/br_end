import G from "../Global";
const { ccclass, property } = cc._decorator;

@ccclass
export default class Scroll extends cc.Component {

    @property()
    private resetY = 1213;

    private y0;

    start() {
        this.y0 = this.node.y;
    }

    update(dt) {
        if (!G.endG) {
            var y = this.node.y;
            y += G.speedBg * dt;
            if (y <= this.resetY) {
                y = this.y0;
            }
            this.node.y = y;
        }
    }
}