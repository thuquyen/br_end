// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class HPBar extends cc.Component {

    @property(cc.Node)
    hp: cc.Node = null;
    @property(cc.Node)
    hpBg: cc.Node = null;

    scaleX0: number;
    scaleY0: number;

    onLoad(){
        this.scaleX0 = this.hpBg.scaleX;
        this.scaleY0 = this.hpBg.scaleY;
    }

    appear(){
        this.node.active = true;
        this.hp.active = true;
        this.hpBg.active = true;
        this.hp.scaleX = 0;
        this.hp.runAction(cc.scaleTo(0.5, this.scaleX0*1, this.scaleY0).easing(cc.easeSineOut()));
    }

    setPercent(p){
        if(p<0)
            p=0;
        this.hp.scaleX = p*this.scaleX0;
    }

}