import WaveQuangCao from "./Wave/WaveQuangCao";
import ShipController from "./Ship31";
import HandController from "./HandController";
import { Helper } from "./Helper";
import G from "./Global";

const { ccclass, property } = cc._decorator;

let GameState = {
    INIT: 0,
    READY: 1,
    PLAYING: 2,
    ENDGAME: 3
}

declare const window: any;

@ccclass
export default class SpaceShooterAd2 extends cc.Component {

    gameState: number = GameState.INIT;
    @property(cc.Node)
    logo: cc.Node = null;

    @property(cc.Node)
    txtMoveShip: cc.Node = null;

    @property(WaveQuangCao)
    wave: WaveQuangCao = null

    @property(cc.Node)
    boss1: cc.Node = null;

    @property(cc.Node)
    wave1: cc.Node[] = [];

    @property(HandController)
    hand: HandController = null;

    @property(cc.Layout)
    bgLayout: cc.Layout = null;

    // @property(cc.Node)
    // buttonInstall: cc.Node = null;

    index: number = 1;

    nShip: number = 0;

    @property(ShipController)
    ship: ShipController = null;

    istouch: boolean = true;

    ironsource: boolean = false;

    // @property({ type: cc.AudioClip })
    // bgSound: cc.AudioClip = null;
    // temp: number = 0;

    check1st: boolean = false;
    check2st: boolean = false;
    check3st: boolean = false;

    nextBoss: number = 0;
    doneChoose: boolean = false;
    // @property(cc.Node)
    textFeatured: cc.Node = null;

    firstTouch: boolean = false;
    posX: number;
    posY: number;
    checkEnd: boolean = false;

    @property(cc.Node)
    menuEnd: cc.Node = null;

    isItemVip: boolean = false;
    ready: boolean = false;

    checkCard_1: boolean = false;
    checkCard_2: boolean = false;
    checkCard_3: boolean = false;
    checkCard_4: boolean = false;

    countChoose: number = 0;
    chooseDone: boolean = false;

    checkMerge: boolean = false;

    checkInteraction: boolean = false;

    @property(cc.Node)
    btn_Con: cc.Node = null;

    @property(cc.Node)
    menu: cc.Node = null;

    @property(cc.Node)
    effectShip: cc.Node = null;
    
    onLoad() {
        cc.view.enableAutoFullScreen(false);
        this.btn_Con.zIndex = 200;
        this.txtMoveShip.zIndex = 100;
        this.ship.node.zIndex = 1;
        // this.bgLayout.node.zIndex = 2;
    }

    appearShip() {
        this.checkMerge = true;
        this.ship.node.runAction(cc.sequence(cc.moveTo(0.8, cc.v2(0, -270)).easing(cc.easeBackOut()),
            cc.callFunc(() => {
                this.ship.tranform1();
                this.effectShip.active = false;
                this.node.getComponent("CanvasTouchManager").enabled = true;
                cc.director.getCollisionManager().enabled = true;
                this.txtMoveShip.active = true;
                this.hand.node.opacity = 255;
                this.hand.node.zIndex = 12;
                this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 50);

                this.posX = this.ship.node.x;
                this.posY = this.ship.node.y;
                this.gameState = GameState.READY;
                this.startGame();
            })))
    }

    appearBoss() {
        this.boss1.active = true;
        this.boss1.runAction((cc.scaleTo(2, 1.05, 1.05)))
        this.boss1.runAction(cc.sequence(cc.moveBy(2, cc.v2(0, 200)),
            cc.callFunc(() => {
                this.boss1.opacity = 255;
                G.boss1 = true;
                G.hpBoss = true;
                G.isBoss = true;
                cc.director.getCollisionManager().enabled = true;
            }))
        )
    }

    start() {
        // this.wave.appear(() => { });
        // this.temp = cc.audioEngine.play(this.bgSound, true, 1);
        this.gameState = GameState.INIT;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);

        if (this.ironsource) {
            window.NUC.trigger.ready("ready")
        }
    }

    btnPlay() {
        this.menu.destroy();
        this.appearShip();
        this.wave1[0].active = true;
        this.wave1[1].active = true;
    }

    onTouchStart(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                if (!G.endG && !G.enableMouse) {
                    this.startGame();
                    this.Interaction();
                }
                break;

            case GameState.PLAYING:
                if (!G.endG && !G.enableMouse) {
                    this.txtMoveShip.active = false;
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                    this.hand.stopSwipeAnim();
                    // this.unscheduleAllCallbacks();
                }
                break;
        }
    }

    onTouchMove(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                if (!G.endG && !G.enableMouse) {
                    this.Interaction();
                    this.startGame();
                }
                break;
            case GameState.PLAYING:
                if (!G.endG && !G.enableMouse) {
                    this.txtMoveShip.active = false;
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                    this.hand.stopSwipeAnim();
                    // this.unscheduleAllCallbacks();
                }
                break;
        }

    }

    onTouchEnd(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!G.endG) {
                    this.ship.setEnableBullet(false);
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 50);
                }
                break;
        }
    }

    onTouchCancel(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!G.endG) {
                    this.ship.setEnableBullet(false);
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 50);
                    // console.log('cancel')
                }
                break;
        }
    }

    Interaction() {
        this.bgLayout.node.active = false;;
        if (this.ironsource && !this.checkInteraction) {
            this.checkInteraction = true
            window.NUC.trigger.interaction();
        }
    }

    startGame() {
        if (this.checkMerge) {
            this.gameState = GameState.PLAYING;
            // this.hand.stopSwipeAnim();
            this.ship.setEnableMove(true);
        }
    }

    update(dt) {
        if (!this.check1st && G.totalE >= 10) {
            this.check1st = true;
            this.ship.setEnableBullet(false);
            cc.director.getCollisionManager().enabled = false;
            G.speedBg = -550;
            this.scheduleOnce(() => {
                cc.director.getCollisionManager().enabled = true;
                G.speedBg = -80;
                this.appearBoss();
                this.ship.setEnableBullet(true);
            }, 1.2)
        }

        if (!this.check2st && G.totalE >= 11) {
            this.check2st = true;
            this.endGame();
            G.endG = true;
            this.ship.setEnableBullet(false);
            this.ship.setEnableMove(false);

            cc.director.getCollisionManager().enabled = false;

        }
    }

    endGame() {
        // cc.audioEngine.stop(this.temp);
        this.scheduleOnce(() => {
            this.btn_Con.active = true;
        }, 1)

        var findDeep = function (node, nodeName) {
            if (node.name == nodeName) return node;
            for (var i = 0; i < node.children.length; i++) {
                var res = findDeep(node.children[i], nodeName);
                if (res) {
                    res.destroy();
                }
                // return res;
            }
        }
        findDeep(cc.director.getScene(), "Bullet_42_2");
        findDeep(cc.director.getScene(), "bulletBoss6");
        this.ship.node.runAction(cc.sequence(
            cc.moveBy(1.1, 0, 2000).easing(cc.easeBackIn()),
            cc.callFunc(() => {
                findDeep(cc.director.getScene(), "ship");
                this.menuEnd.active = true;
            }))
        )
        G.text = false;
        this.checkEnd = true;

        if (this.ironsource) {
            window.NUC.trigger.endGame('win')
        }

        // if (this.buttonInstall)
        //     this.buttonInstall.active = false;

        this.hand.node.active = false;
        this.txtMoveShip.active = false;
        this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    }
}