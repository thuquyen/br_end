import Singleton from "./Singleton";

const { ccclass, property } = cc._decorator;

@ccclass
export default class CoinContainScripts extends Singleton<CoinContainScripts> {
    @property(cc.Node)
    MoveToThis: cc.Node = null;
    @property(cc.Prefab)
    Coin: cc.Prefab = null;

    @property(cc.Node)
    txtPoint: cc.Node = null;
    // @property(cc.Node)
    // txtPointCompetitor: cc.Node = null;
    @property(cc.Node)
    PoolManager: cc.Node = null;
    public xenemy: number = 0;
    public yenemy: number = 0;
    Point: number = 0;
    PointCompetitor: number = 0;
    Amount: number = 6;

    constructor() {
        super();
        CoinContainScripts._instance = this;
    }

    SpawnerCoin() {
        var arraycoin = [];
        for (let i = 0; i < this.Amount; i++) {
            var newCoin = cc.instantiate(this.Coin);
            newCoin.parent = this.PoolManager;
            newCoin.x = this.xenemy;
            newCoin.y = this.yenemy;
            arraycoin.push(newCoin);
        }

        var RandomDelay = 0;
        for (let i = 0; i < arraycoin.length; i++) {
            let Ran = Math.round(Math.random());
            if (Ran > 0.5) {
                do {
                    var RandomPosx2 = -150 * Math.random();
                } while (RandomPosx2 < -40);
                do {
                    var RandomPosy2 = -150 * Math.random();
                } while (RandomPosy2 < -40);
                arraycoin[i].runAction(cc.sequence(cc.moveTo(0.5, cc.v2(arraycoin[i].x + RandomPosx2, arraycoin[i].y + RandomPosy2)), cc.delayTime(RandomDelay), cc.moveTo(0.5, cc.v2(this.MoveToThis.x, this.MoveToThis.y)), cc.callFunc(() => {
                    //cc.audioEngine.playEffect(Global.Coin, false);

                    arraycoin[i].destroy();
                    this.Point += 100;
                    this.PointCompetitor += 80;
                    if (this.Point >= 1000) {
                        var a;
                        a = this.Point / 1000;
                        this.txtPoint.getComponent(cc.Label).string = a.toString() + "K";
                    }
                    else {
                        this.txtPoint.getComponent(cc.Label).string = this.Point.toString();
                    }
                    // if (this.PointCompetitor >= 1000) {
                    //     var a;
                    //     a = this.PointCompetitor / 1000;
                    //     this.txtPointCompetitor.getComponent(cc.Label).string = a.toString() + "K";
                    // }
                    // else {
                    //     this.txtPointCompetitor.getComponent(cc.Label).string = this.PointCompetitor.toString();
                    // }
                })));
                arraycoin[i].runAction(cc.moveTo(0.5, cc.v2(arraycoin[i].x + RandomPosx2, arraycoin[i].y + RandomPosy2)));
            }
            else {
                do {
                    var RandomPosx = 150 * Math.random();
                } while (RandomPosx > 40);
                do {
                    var RandomPosy = 150 * Math.random();
                } while (RandomPosy > 40);
                arraycoin[i].runAction(cc.sequence(cc.moveTo(0.5, cc.v2(arraycoin[i].x + RandomPosx, arraycoin[i].y + RandomPosy)), cc.delayTime(RandomDelay), cc.moveTo(0.5, cc.v2(this.MoveToThis.x, this.MoveToThis.y)), cc.callFunc(() => {

                    arraycoin[i].destroy();
                    this.Point += 100;
                    this.PointCompetitor += 80;
                    if (this.Point >= 1000) {
                        var a;
                        a = this.Point / 1000;
                        this.txtPoint.getComponent(cc.Label).string = a.toString() + "K";
                    }
                    else {
                        this.txtPoint.getComponent(cc.Label).string = this.Point.toString();
                    }
                    // if (this.PointCompetitor >= 1000) {
                    //     var a;
                    //     a = this.PointCompetitor / 1000;
                    //     this.txtPointCompetitor.getComponent(cc.Label).string = a.toString() + "K";
                    // }
                    // else {
                    //     this.txtPointCompetitor.getComponent(cc.Label).string = this.PointCompetitor.toString();
                    // }
                })));
                arraycoin[i].runAction(cc.moveTo(0.5, cc.v2(arraycoin[i].x + RandomPosx, arraycoin[i].y + RandomPosy)));
            }
            RandomDelay = RandomDelay + 0.01;
        }
    }
}
