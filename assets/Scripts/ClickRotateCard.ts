
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    card: cc.Node[] = []
    @property(cc.Node)
    questionMark: cc.Node[] = []
    @property(cc.Node)
    boder: cc.Node[] = []
    @property(cc.Node)
    evoMenu: cc.Node = null;
    @property(cc.Node)
    hand: cc.Node = null;
    @property(cc.Node)
    flare: cc.Node[] = []

    start() {

    }
    openCard1() {
        this.evoMenu.getComponent(cc.Animation).stop();
        this.hand.destroy();
        for (var i = 0; i < 3; i++) {
            this.flare[i].destroy();
        }
        this.boder[0].active = true;
        this.card[0].getComponent(cc.Animation).play();
        this.questionMark[0].getComponent(cc.Animation).play();
    }

    openCard2() {
        this.evoMenu.getComponent(cc.Animation).stop();
        this.hand.destroy();
        for (var i = 0; i < 3; i++) {
            this.flare[i].destroy();
        }
        this.boder[1].active = true;
        this.card[1].getComponent(cc.Animation).play();
        this.questionMark[1].getComponent(cc.Animation).play();
    }

    openCard3() {
        this.evoMenu.getComponent(cc.Animation).stop();
        this.hand.destroy();
        for (var i = 0; i < 3; i++) {
            this.flare[i].destroy();
        }
        this.boder[2].active = true;
        this.card[2].getComponent(cc.Animation).play();
        this.questionMark[2].getComponent(cc.Animation).play();
    }
    // update (dt) {}
}
