import WaveQuangCao from "./Wave/WaveQuangCao";
import ShipController from "./Ship44";
import HandController from "./HandController";
import { Helper } from "./Helper";
import G from "./Global";

const { ccclass, property } = cc._decorator;

let GameState = {
    INIT: 0,
    READY: 1,
    PLAYING: 2,
    ENDGAME: 3
}

declare const window: any;

@ccclass
export default class SpaceShooterAd2 extends cc.Component {

    gameState: number = GameState.INIT;

    @property(cc.Node)
    endG: cc.Node = null;

    @property(cc.Node)
    txtMoveShip: cc.Node = null;

    @property(HandController)
    hand: HandController = null;

    // @property(cc.Node)
    // buttonInstall: cc.Node = null;

    @property(cc.Node)
    titleMini: cc.Node = null;

    index: number = 1;

    nShip: number = 0;

    @property(ShipController)
    ship: ShipController = null;
    istouch: boolean = true;

    ironsource: boolean = false;

    @property({ type: cc.AudioClip })
    bgSound: cc.AudioClip = null;
    temp: number = 0;

    check1st: boolean = false;
    check2st: boolean = false;
    check3st: boolean = false;

    nextBoss: number = 0;
    doneChoose: boolean = false;
    // @property(cc.Node)
    textFeatured: cc.Node = null;

    firstTouch: boolean = false;
    posX: number;
    posY: number;
    checkEnd: boolean = false;

    // @property(cc.Node)
    // effectShip: cc.Node = null;

    isItemVip: boolean = false;
    ready: boolean = false;

    @property(cc.Node)
    menu: cc.Node = null;

    check_play: boolean = false;

    @property(cc.Node)
    wave: cc.Node[] = []

    @property(sp.SkeletonData)
    listShip: sp.SkeletonData[] = [];
    //openCard
    @property(cc.Node)
    cardEvo: cc.Node = null

    @property(cc.Node)
    card: cc.Node[] = []

    @property(cc.Node)
    questionMark: cc.Node[] = []

    @property(cc.Node)
    boder: cc.Node[] = []

    @property(cc.Node)
    hand_choose: cc.Node = null;

    @property(cc.Node)
    flare: cc.Node[] = []

    @property(cc.Node)
    btn_card: cc.Node[] = []

    checkCard_1: boolean = false;
    checkCard_2: boolean = false;

    countChoose: number = 0;
    chooseDone: boolean = false;
    @property(cc.Node)
    btn_Play: cc.Node = null;
    //

    onLoad() {
        this.txtMoveShip.zIndex = 100;
        cc.view.enableAutoFullScreen(false);
        cc.director.getCollisionManager().enabled = true;
        this.ship.node.zIndex = 1;
        this.endG.zIndex = 3;
    }

    btnPlay() {
        this.scheduleOnce(() => {
            this.menu.destroy();
            this.appearShip();
            this.chooseDone = true;
        }, 0.8)

    }

    open(boderNumber: number, cardNumber: number, questionMarkNum: number) {
        this.cardEvo.getComponent(cc.Animation).stop();
        this.hand_choose.destroy();
        for (var i = 0; i < 2; i++) {
            this.flare[i].destroy();
        }
        this.boder[boderNumber].active = true;
        this.card[cardNumber].getComponent(cc.Animation).play();
        this.questionMark[questionMarkNum].getComponent(cc.Animation).play();
    }

    openCard1() {
        if (!this.checkCard_1 && (this.countChoose < 1)) {

            this.countChoose++;
            this.checkCard_1 = true;
            this.open(0, 0, 0);
            this.scheduleOnce(() => {
                this.open(0, 1, 1);

                this.ship.node.scale = 1;
                this.ship.node.getComponent(sp.Skeleton).skeletonData = this.listShip[0];
                this.ship.node.getComponent(sp.Skeleton).setAnimation(1, 'fly', true)

                this.scheduleOnce(() => {
                    this.enableBtn_Play();
                    this.btnPlay();
                }, 0.8)

            }, 0.5)
        }
    }

    openCard2() {
        if (!this.checkCard_2 && (this.countChoose < 1)) {
            this.countChoose++;
            this.checkCard_2 = true;

            this.open(1, 1, 1);
            this.scheduleOnce(() => {
                this.open(1, 0, 0);

                this.ship.node.scale = 0.85;
                this.ship.node.getComponent(sp.Skeleton).skeletonData = this.listShip[1];
                this.ship.node.getComponent(sp.Skeleton).setAnimation(1, 'fly2', true)

                this.scheduleOnce(() => {
                    this.enableBtn_Play();
                    this.btnPlay();
                }, 0.8)

            }, 0.5)
        }
    }

    enableBtn_Play() {
        this.btn_Play.runAction((cc.sequence(
            cc.scaleTo(0.25, 0.55, 0.55),
            cc.scaleTo(0.25, 0.45, 0.45),
            cc.scaleTo(0.25, 0.55, 0.55),
        )))
    }
    appearShip() {
        // this.buttonInstall.active=true;
        this.wave[0].active = true;
        this.ship.node.runAction(cc.sequence(cc.moveTo(0.8, cc.v2(0, -290)).easing(cc.easeBackOut()), cc.callFunc(() => {
            this.ship.setEnableBullet(false);
     
            // this.effectShip.active = false;
            this.posX = this.ship.node.x;
            this.posY = this.ship.node.y;
            this.node.getComponent("CanvasTouchManager").enabled = true;
            this.gameState = GameState.READY;
        })))
        this.scheduleOnce(() => {
            this.turnOnGuide();
            G.startSpawnItem = true;
        }, 1)
    }

    start() {
        // this.temp = cc.audioEngine.play(this.bgSound, true, 1);

        // auto after 6s
        this.scheduleOnce(() => {
            if (!this.chooseDone) {
                this.openCard2()
            }
        }, 5)

        this.scheduleOnce(() => {
            this.gameState = GameState.INIT;
            this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
            this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
            this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
            this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);

            if (this.ironsource) {
                window.NUC.trigger.ready("ready")
            }
        }, 1)

    }

    turnOnGuide() {
        this.txtMoveShip.active = true;
        this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 40);
        this.hand.node.zIndex = 12;
    }

    endGame() {
        // this.buttonInstall.active=false;
        this.titleMini.active = false;
        cc.audioEngine.stop(this.temp);
        var findDeep = function (node, nodeName) {
            if (node.name == nodeName) return node;
            for (var i = 0; i < node.children.length; i++) {
                var res = findDeep(node.children[i], nodeName);
                if (res)
                    res.destroy();
                // return res;
            }
        }
        findDeep(cc.director.getScene(), "item_Upgrade");

        this.ship.node.runAction(cc.sequence(
            cc.moveBy(1.1, 0, 2000).easing(cc.easeBackIn()),
            cc.callFunc(() => {
                findDeep(cc.director.getScene(), "ship");
                findDeep(cc.director.getScene(), "Bullet_ship22new");
                findDeep(cc.director.getScene(), "Bullet1_ship22new");
                findDeep(cc.director.getScene(), "Bullet2_ship22new");
                findDeep(cc.director.getScene(), "Bullet3_ship22new");
                findDeep(cc.director.getScene(), "BulletRainbow");
            })
        )
        )
        G.text = false;
        this.checkEnd = true;

        if (this.ironsource) {
            window.NUC.trigger.ready("ready")
        }
        this.scheduleOnce(() => {
            this.endG.active = true;
        }, 2)

        // if (this.buttonInstall)
        //     this.buttonInstall.active = false;
        // this.titleMini.active = false;

        this.hand.node.active = false;
        this.txtMoveShip.active = false;
        this.ship.setEnableMove(false);
        this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    }

    onTouchStart(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                G.clickStart = true;
                cc.director.resume();
                this.txtMoveShip.active = false;
                this.hand.stopSwipeAnim();
                if (this.istouch && this.chooseDone) {
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                    this.istouch = false;
                    // this.unscheduleAllCallbacks();
                }
                break;
            case GameState.PLAYING:
                cc.director.resume();
                this.txtMoveShip.active = false;
                this.hand.stopSwipeAnim();
                if (this.chooseDone) {
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                    // this.unscheduleAllCallbacks();
                }
                break;
        }
    }

    onTouchMove(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                G.clickStart = true;
                cc.director.resume();
                this.txtMoveShip.active = false;
                this.hand.stopSwipeAnim();
                if (this.istouch && this.chooseDone) {
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                    this.istouch = false;
                    // this.unscheduleAllCallbacks();
                }
                break;
            case GameState.PLAYING:
                cc.director.resume();
                this.txtMoveShip.active = false;
                this.hand.stopSwipeAnim();
                if (this.chooseDone) {
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                    // this.unscheduleAllCallbacks();
                }
                break;
        }
    }

    onTouchEnd(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!this.ship.isDie && this.chooseDone) {
                    cc.director.pause();
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 40);
                }
                break;
        }
    }

    onTouchCancel(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!this.ship.isDie  && this.chooseDone) {
                    cc.director.pause()
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 40);
                }
                break;
        }
    }

    startGame() {
        if (this.ironsource)
            window.NUC.trigger.interaction();
        this.gameState = GameState.PLAYING;
        // this.hand.stopSwipeAnim();
    }

    update(dt) {
        if (!G.text && !this.firstTouch) {
            this.firstTouch = true;
            this.startGame();
        }

        if (G.totalE >= 36 && !this.check1st) {
            this.check1st = true;
            G.speedBg = -280;
            // this.ship.setEnableBullet(false);
            // this.scheduleOnce(()=>{
            //     G.speedBg=-80;
            // },1)

            this.wave[1].active = true;
        }

        if (G.totalE >= 60 && !this.check2st && !G.endG) {
            this.check2st = true;
            G.endG = true;
            this.endGame()

        }
    }
}