
const { ccclass, property } = cc._decorator;

@ccclass
export default class BulletBezier extends cc.Component {
    @property(String)
    StringMovingPath: String = "";
    OldVec: cc.Vec2 = null;

    @property(cc.Node)
    Bullet: cc.Node[] = [];

    @property()
    fireTime: number = 0;

    @property()
    scaleX: number = 0;

    @property()
    scaleY: number = 0;

    @property()
    temp1: number = 0;

    @property()
    temp2: number = 0;

    start() {
        if (this.Bullet[0].activeInHierarchy) {
            this.OldVec = cc.v2(this.Bullet[0].x, this.Bullet[0].y);

            if (this.StringMovingPath == "Left") {
                this.MovingPathLeft(this.Bullet[0], this.fireTime, -1500, 1000);
            }
            else {
                this.MovingPathRight(this.Bullet[0], this.fireTime, 1500, 1000);
            }
        }

    }

    MovingPathLeft(Temp: cc.Node, firetime: number, x: number, y: number) {
        var bezier = [cc.v2(Temp.x, Temp.y), cc.v2(Temp.x + this.temp1, Temp.y + 130), cc.v2(Temp.x + this.temp2, y)];
        var bezierTo = cc.bezierBy(firetime / 2, bezier);
        Temp.runAction(cc.sequence((bezierTo).easing(cc.easeSineOut()), cc.callFunc(() => {
            Temp.destroy();
        })));
        Temp.runAction(cc.scaleTo(1, this.scaleX, this.scaleY));

    }

    MovingPathRight(Temp: cc.Node, firetime: number, x: number, y: number) {
        var bezier = [cc.v2(Temp.x, Temp.y), cc.v2(Temp.x + this.temp1, Temp.y + 130), cc.v2(Temp.x + this.temp2, y)];
        var bezierTo = cc.bezierBy(firetime / 2, bezier);
        Temp.runAction(cc.sequence((bezierTo).easing(cc.easeSineOut()), cc.callFunc(() => {
            Temp.destroy();
        })));
        Temp.runAction(cc.scaleTo(1, this.scaleX, this.scaleY));
    }

    betweenDegree(comVec, dirVec) {
        let angleDeg = Math.atan2(dirVec.y - comVec.y, dirVec.x - comVec.x) * 180 / Math.PI;
        return angleDeg;
    }

    update(dt) {
        if (this.Bullet[0].activeInHierarchy) {
            if (this.Bullet[0].y != this.OldVec.y) {
                //khi con rồng thì vị trí của nó sẽ # vs vị trí ban đầu đc lưu, tính vector và góc lệch giữa 2 vị trí để quay góc rồng
                let degree = this.betweenDegree(this.OldVec, cc.v2(this.Bullet[0].x, this.Bullet[0].y)) - 90;
                // - 90;
                this.Bullet[0].angle = degree;
                //cc.misc.lerp(this.Bullet[0].angle, degree, 0.1);
                this.OldVec = cc.v2(this.Bullet[0].x, this.Bullet[0].y);
            }
        }
    }
}
