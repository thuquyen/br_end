import { Helper } from "./Helper";
import G from "./Global";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SoundManager extends cc.Component {
    // @property({ type: cc.AudioClip })
    // bg: cc.AudioClip = null;

    // @property(cc.AudioClip)
    // bossExplosion: cc.AudioClip = null;

    // @property(cc.AudioClip)
    // bossRun: cc.AudioClip = null;
    @property({ type: cc.AudioClip })
    enemyDie: cc.AudioClip = null;

    @property({ type: cc.AudioClip })
    finalWin: cc.AudioClip = null;

    // @property({ type: cc.AudioClip })
    // flyDie: cc.AudioClip = null;

    // @property({ type: cc.AudioClip })
    // clickS: cc.AudioClip = null;
    // @property({ type: cc.AudioClip })
    // getItem: cc.AudioClip = null;
    // @property({ type: cc.AudioClip })
    // hitBullet: cc.AudioClip = null;
    // @property(cc.AudioClip)
    // playerExplosion: cc.AudioClip = null;
    // @property(cc.AudioClip)
    // revive: cc.AudioClip = null;
    // @property(cc.AudioClip)
    // warning: cc.AudioClip = null;

    onLoad() {
        //cc.loader.loadRes('Audio', this.bg);
        //cc.audioEngine.playMusic(this.bg, true);

        // G.bgSound = this.bg;
        G.enemyDie = this.enemyDie;
        G.finalWin = this.finalWin;
        // G.flyDie = this.flyDie;
    //     G.click = this.clickS;
    //     // G.hitBullet = this.hitBullet;
    //     G.item = this.getItem;
    }
}
