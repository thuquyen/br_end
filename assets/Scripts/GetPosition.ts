
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    enemy: cc.Node[] = [];

    min: number = 0;

    currentNumber: number = 0;

    onLoad() {
        for (let i = 0; i < this.enemy.length; i++) {
            this.getPositionEnemy(this.enemy[i]);
        }

    }

    getPositionEnemy(e: cc.Node) {
        if (this.node.parent.width > this.node.parent.height) {
            this.min = this.node.parent.height;
            this.currentNumber = this.node.parent.width / 960 * 640;
        } else {
            this.min = this.node.parent.width;
            this.currentNumber = 640;
        }
        for (let i = 0; i < e.childrenCount; i++) {
            let x = e.children[i].x / this.currentNumber * this.min;
            let y = e.children[i].y;
            e.children[i].setPosition(cc.v2(x, y));
        }
    }
}
