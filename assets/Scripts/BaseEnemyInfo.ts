import BaseEnemy from "./Enemy/BaseEnemy";

const {ccclass, property} = cc._decorator;

@ccclass
export default class BaseEnemyInfo extends cc.Component {

    @property(cc.Prefab)
    prefabEnemy: cc.Prefab = null;

    @property(Number)
    hp: number = 2;

    @property(Number)
    dmg: number = 0;

    spawnEnemy(): cc.Node {
        var enemy = cc.instantiate(this.prefabEnemy);
        var baseEnemy = enemy.getComponent(BaseEnemy);
        enemy.parent = this.node;
        baseEnemy.hp = this.hp;
        baseEnemy.dmg = this.dmg;
        return enemy;
    }

}