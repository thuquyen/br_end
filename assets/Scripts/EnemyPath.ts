import SequencePoints from "./SequencePoints";
import { Helper } from "./Helper";

const {ccclass, property} = cc._decorator;

@ccclass
export default class EnemyPath extends cc.Component {

    @property(cc.Prefab)
    enemyPrefab: cc.Prefab = null;

    @property(Number)
    n: number = 5;

    @property(Number)
    speed: number = 300;

    @property(Number)
    distance: number = 100;

    @property(Number)
    delay: number = 0;

    @property(SequencePoints)
    splinePath: SequencePoints = null;

    // @property(cc.Component.EventHandler)
    // completeHandler: cc.Component.EventHandler = null;

    start () {
        this.scheduleOnce(()=>{
            this.startAttack();    
        }, this.delay);
    }

    startAttack(){
        for(var i=0; i<this.n; i++) {
            var enemy = cc.instantiate(this.enemyPrefab);
            enemy.parent = this.node.parent;
            Helper.splineFollowAndRotate(enemy, this.splinePath.points, this.speed, i*this.distance/this.speed, 90, null);
        }
    }

}