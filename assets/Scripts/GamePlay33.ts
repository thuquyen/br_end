import WaveQuangCao from "./Wave/WaveQuangCao";
import ShipController from "./ShipController";
import HandController from "./HandController";
import { Helper } from "./Helper";
import G from "./Global";

const { ccclass, property } = cc._decorator;

let GameState = {
    INIT: 0,
    READY: 1,
    PLAYING: 2,
    ENDGAME: 3
}

declare const window: any;

@ccclass
export default class SpaceShooterAd2 extends cc.Component {

    gameState: number = GameState.INIT;

    // @property(cc.Node)
    // btnContinue: cc.Node = null;

    @property(cc.Node)
    txtMoveShip: cc.Node = null;

    @property(cc.Node)
    loadLevel: cc.Node[] = [];

    @property(cc.Node)
    ships: cc.Node = null;

    @property(cc.Node)
    handOld: cc.Node = null;

    @property(cc.Node)
    wave: cc.Node[] = [];

    @property(WaveQuangCao)
    wave2: WaveQuangCao = null

    @property(HandController)
    hand: HandController = null;

    @property(cc.Layout)
    bgLayout: cc.Layout = null;

    // @property(cc.Node)
    // buttonInstall: cc.Node = null;

    @property({ type: cc.AudioClip })
    bgSound: cc.AudioClip = null;
    temp: number = 0;

    @property(cc.Prefab)
    itemChangeShip: cc.Prefab[] = [];

    index: number = 1;

    nShip: number = 0;

    ship: ShipController = null;

    istouch: boolean = true;

    ironsource: boolean = false;
    mindworks: boolean = true;

    checkEndG: boolean = false;
    level2: boolean = false;
    @property(cc.Node)
    womenLayout: cc.Node = null;
    eAppeared: boolean = false;
    tempHand: number = 80;
    @property(cc.Node)
    header: cc.Node = null;

    onLoad() {
        cc.view.enableAutoFullScreen(false);
        cc.director.getCollisionManager().enabled = false;
    }

    start() {
        this.wave2.onComplete = () => {
            this.checkEndG = true;
            this.EndGame();
        }

        this.temp = cc.audioEngine.play(this.bgSound, true, 1);
        this.txtMoveShip.zIndex = 6;
        this.loadLevel[0].zIndex = 10;
        this.loadLevel[1].zIndex = 10;
        this.header.zIndex = 20;
        this.hand.node.zIndex = 22;
        
        this.gameState = GameState.INIT;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
        this.ships.parent.getComponent(cc.Animation).play('animChooseShip');
        this.handOld.zIndex = 5;

        if (this.ironsource) {
            window.NUC.trigger.ready("ready")
        }
        for (let i = 0; i < this.ships.childrenCount; i++) {
            this.ships.children[i].on('click', () => {
                this.chooseShipClick(i);
                if (this.ironsource) {
                    if (i == 0) {
                        window.NUC.event.send('ship', 's11')
                    }
                    else if (i == 1) {
                        window.NUC.event.send('ship', 's21')
                    }
                    else {
                        window.NUC.event.send('ship', 's22_new')
                    }
                }
                this.unscheduleAllCallbacks();
            }, this);
        }

        this.scheduleOnce(() => {
            if (this.ironsource) {
                window.NUC.trigger.autoplay();
                window.NUC.event.send('autoChoose');
            }
            this.chooseShipClick(2);
        }, 6);
    }
    update() {
        if (G.totalE >= 30 && !this.level2) {
            this.level2 = true;
            if (this.ironsource) {
                window.NUC.trigger.endLevel();
            }
            this.bgLayout.node.runAction(cc.sequence(cc.fadeTo(0.2, 150), cc.callFunc(() => {
                this.loadLevel[0].active = true;
                this.ship.setEnableBullet(false);
                this.txtMoveShip.opacity = 0;
            })));
            this.scheduleOnce(() => {
                this.bgLayout.node.runAction(cc.sequence(cc.fadeTo(0.2, 0), cc.callFunc(() => {
                    this.loadLevel[0].active = false;
                    this.wave2.appear(() => {
                        G.hpBoss = true;
                    });
                    this.ship.setEnableBullet(true);
                    this.txtMoveShip.opacity = 255;
                })));
            }, 1);

        }
    }
    EndGame() {
        cc.audioEngine.stop(this.temp);
        if(this.mindworks){
            window.gameEnd && window.gameEnd();
        }
        if (this.ironsource)
            window.NUC.trigger.endGame('win')
        this.bgLayout.node.zIndex = 10;
        this.bgLayout.node.runAction(cc.sequence(cc.fadeTo(0.2, 150), cc.callFunc(() => {
            this.scheduleOnce(() => {
                this.loadLevel[1].active = true;
            }, 0.3)
        })));
        // if (this.buttonInstall)
        //     this.buttonInstall.active = false;
        this.hand.node.active = false;
        this.txtMoveShip.active = false;
        this.ship.node.active = false;
        this.ship.setEnableMove(false);
        this.ship.setEnableBullet(false);
        this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    }
    //choose ship
    chooseShipClick(n) {
        this.wave[0].active = true;
        this.scheduleOnce(() => {
            this.eAppeared = true;
        }, 0.5)
        if (this.ironsource) {
            window.NUC.trigger.startLevel();
        }
        this.womenLayout.active = false;;
        this.bgLayout.node.opacity = 0;
        if (n == 0) {
            this.ships.children[0].scale = 0.75;
        }
        else if (n == 1) {
            this.ships.children[1].scale = 0.95;
        }
        else {
            this.ships.children[2].scale = 0.62;
        }
        this.ships.children[n].runAction(cc.sequence(cc.moveTo(0.5, cc.v2(0, -297)), cc.callFunc(() => {
            this.txtMoveShip.active = true;
            this.ship = this.ships.children[n].getComponent("ShipController");
            this.node.getComponent("CanvasTouchManager").enabled = true;
            this.ships.children[n].getComponent(cc.Button).enabled = false;
            this.startGame();
            this.gameState = GameState.READY;
            this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - this.tempHand);
            this.scheduleOnce(() => {
                cc.director.getCollisionManager().enabled = true;
            }, 2)

        })));
        for (let i = 0; i < this.ships.childrenCount; i++) {
            if (i != n) {
                this.ships.parent.getComponent(cc.Animation).play('animChooseShipOff');
                this.ships.children[i].active = false;
            } else {
                this.ships.children[i].active = true;
            }
        }
    }

    onTouchStart(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                this.startGame();
                break;
            case GameState.PLAYING:
                this.txtMoveShip.active = false;
                this.hand.stopSwipeAnim();
                this.ship.setEnableBullet(true);
                break;
        }
    }

    onTouchMove(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                this.startGame();
                break;
            case GameState.PLAYING:
                this.txtMoveShip.active = false;
                this.hand.stopSwipeAnim();
                this.ship.setEnableBullet(true);
                break;
        }
    }

    onTouchEnd(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!this.ship.isDie) {
                    this.ship.setEnableBullet(false);
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - this.tempHand);
                }
                break;
        }
    }

    onTouchCancel(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!this.ship.isDie) {
                    this.ship.setEnableBullet(false);
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - this.tempHand);
                }
                break;
        }
    }

    startGame() {
        if (this.ironsource)
            window.NUC.trigger.interaction();
        this.gameState = GameState.PLAYING;
        this.hand.stopSwipeAnim();
        this.ship.setEnableMove(true);
    }

    changeShip() {
        let prefabShip = this.itemChangeShip[this.nShip];
        this.nShip++;
        if (this.nShip >= this.itemChangeShip.length)
            this.nShip = 0;
        var newItem = cc.instantiate(prefabShip);
        newItem.parent = this.node;
        newItem.x = (this.node.x - 150) * (2 * Math.random() - 1);
        newItem.y = (this.node.y - 200) * Math.random();
        newItem.scale = 0.2;
        newItem.runAction(cc.spawn(cc.scaleTo(0.2, 1), cc.moveBy(10, cc.v2(0, -1000))));
    }

    updateChangeShip() {
        this.schedule(() => {
            this.changeShip();
        }, 6, cc.macro.REPEAT_FOREVER, 5);
    }

    clickIns1() {
        if (this.ironsource) {
            window.NUC.event.send('clickIns', 'btnInsInGame')
        }
    }
    clickIns2() {
        if (this.ironsource) {
            window.NUC.event.send('clickIns', 'btnInsEndGame')
        }
    }
    clickIns3() {
        if (this.ironsource) {
            window.NUC.event.send('clickIns', 'btnIconLogo')
        }
    }

}