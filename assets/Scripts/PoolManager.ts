import BasePool from "./common/BasePool";
import { Helper } from "./Helper";
import { EnemyType } from "./Enemy/EnemyType";

const {ccclass, property} = cc._decorator;

@ccclass('EnemyPool')
export class EnemyPool {
    
    @property({type: cc.Enum(EnemyType)})
    id: EnemyType = EnemyType.E1;

    @property(BasePool)
    pool: BasePool = null;

}

@ccclass
export default class PoolManager extends cc.Component {

    @property(BasePool)
    eBullet: BasePool = null;

    @property(BasePool)
    bBullet: BasePool = null;

    @property(BasePool)
    eExplosion: BasePool = null;

    @property([EnemyPool])
    ePools: EnemyPool[] = [];

    ePoolsMap: any;

    constructor(){
        super();
        Helper.poolManager = this;
        this.initEnemyPoolMap();
    }

    private initEnemyPoolMap(){
        this.ePoolsMap = {};
        for(var i=0; i<this.ePools.length; i++) {
            var ePool = this.ePools[i];
            this.ePoolsMap[ePool.id] = ePool.pool;
        }
    }

    getEnemyPool(eType: EnemyType): BasePool {
        return this.ePoolsMap[eType];
    }

    createEnemy(){
        
    }

    destroyEnemy(){

    }

}