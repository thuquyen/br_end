import WaveQuangCao from "./Wave/WaveQuangCao";
import ShipController from "./Ship29";
import HandController from "./HandController";
import { Helper } from "./Helper";
import G from "./Global";

const { ccclass, property } = cc._decorator;

let GameState = {
    INIT: 0,
    READY: 1,
    PLAYING: 2,
    ENDGAME: 3
}

declare const window: any;

@ccclass
export default class SpaceShooterAd2 extends cc.Component {

    gameState: number = GameState.INIT;
    @property(cc.Node)
    logo: cc.Node = null;

    @property(cc.Node)
    endG: cc.Node = null;

    @property(cc.Node)
    txtMoveShip: cc.Node = null;

    @property(cc.Node)
    wave: cc.Node[] = []

    @property(HandController)
    hand: HandController = null;

    @property(cc.Layout)
    bgLayout: cc.Layout = null;

    // @property(cc.Node)
    // buttonInstall: cc.Node = null;

    @property(cc.Prefab)
    item: cc.Prefab = null;

    index: number = 1;

    nShip: number = 0;

    @property(ShipController)
    ship: ShipController = null;
    istouch: boolean = true;

    ironsource: boolean = false;

    // @property({ type: cc.AudioClip })
    // bgSound: cc.AudioClip = null;
    // temp: number = 0;

    check1st: boolean = false;
    check2st: boolean = false;
    check3st: boolean = false;

    nextBoss: number = 0;
    doneChoose: boolean = false;
    // @property(cc.Node)
    textFeatured: cc.Node = null;

    firstTouch: boolean = false;
    posX: number;
    posY: number;
    checkEnd: boolean = false;

    @property(cc.Node)
    effectShip: cc.Node = null;
    onLoad() {
        cc.view.enableAutoFullScreen(false);
        cc.director.getCollisionManager().enabled = false;
        this.ship.node.zIndex = 1;
        this.bgLayout.node.zIndex = 2;
        this.endG.zIndex = 3;
    }
    start() {
        // this.temp = cc.audioEngine.play(this.bgSound, true, 1);
        this.ship.node.runAction(cc.sequence(cc.moveTo(0.9, cc.v2(0, -297)).easing(cc.easeBackOut()), cc.callFunc(() => {
            // this.scheduleOnce(() => {
            this.ship.setEnableBullet(true);
            // }, 5)
            this.effectShip.active = false;
            this.posX = this.ship.node.x;
            this.posY = this.ship.node.y;
            this.node.getComponent("CanvasTouchManager").enabled = true;
            this.gameState = GameState.READY;
        })))

        this.scheduleOnce(() => {
            this.txtMoveShip.active = true;
            this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 20);
            this.txtMoveShip.zIndex = 6;
            this.hand.node.zIndex = 12;
        }, 1)

        this.gameState = GameState.INIT;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);

        if (this.ironsource) {
            window.NUC.trigger.ready("ready")
        }
    }

    endGame() {
        // cc.audioEngine.stop(this.temp);
        var findDeep = function (node, nodeName) {
            if (node.name == nodeName) return node;
            for (var i = 0; i < node.children.length; i++) {
                var res = findDeep(node.children[i], nodeName);
                if (res)
                    res.destroy();
                // return res;
            }
        }
        findDeep(cc.director.getScene(), "item_Upgrade");
        // findDeep(cc.director.getScene(), "Coin");
        // findDeep(cc.director.getScene(), "bullet_Skade_purple_r");

        this.ship.node.runAction(
            cc.moveBy(1.1, 0, 2000).easing(cc.easeBackIn())
        )
        G.text = false;
        this.checkEnd = true;

        if (this.ironsource) {
            window.NUC.trigger.endGame('win')
        }
        this.scheduleOnce(() => {
            this.endG.active = true;
        }, 1)

        // if (this.buttonInstall)
        //     this.buttonInstall.active = false;

        this.hand.node.active = false;
        this.txtMoveShip.active = false;
        this.ship.setEnableMove(false);
        this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    }

    onTouchStart(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                for (var i = 0; i < 4; i++) {
                    this.wave[i].active = true;
                }

                cc.director.getCollisionManager().enabled = true;
                G.clickStart = true;
                break;
            case GameState.PLAYING:
                cc.director.resume();
                this.txtMoveShip.active = false;
                this.hand.stopSwipeAnim();
                if (this.istouch) {
                    this.ship.setEnableBullet(true);
                    this.istouch = false;
                    this.unscheduleAllCallbacks();
                }
                break;
        }
    }

    onTouchMove(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                break;
            case GameState.PLAYING:
                cc.director.resume();
                this.txtMoveShip.active = false;
                // this.ship.setEnableBullet(true);
                this.hand.stopSwipeAnim();
                if (this.istouch) {
                    this.ship.setEnableBullet(true);
                    this.istouch = false;
                    this.unscheduleAllCallbacks();
                }
                break;
        }
    }

    onTouchEnd(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!this.ship.isDie) {
                    cc.director.pause();
                    // this.ship.setEnableBullet(false);
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 20);
                }
                break;
        }
    }

    onTouchCancel(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!this.ship.isDie) {
                    cc.director.pause()
                    // this.ship.setEnableBullet(false);
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 20);
                }
                break;
        }
    }

    startGame() {
        if (this.ironsource)
            window.NUC.trigger.interaction();
        this.gameState = GameState.PLAYING;
        // this.hand.stopSwipeAnim();
        this.ship.setEnableMove(true);
    }

    spawnItem() {
        var newItem = cc.instantiate(this.item);
        newItem.parent = this.node;
        newItem.zIndex = this.index;
        newItem.x = (this.node.x - 100) * (2 * Math.random() - 1);
        newItem.y = (this.node.y - 150) * Math.random();
    }

    updateItem() {
        this.schedule(() => {
            if (!this.check2st)
                this.spawnItem();
        }, 4, cc.macro.REPEAT_FOREVER, 3);
    }
    clickIns1() {
        if (this.ironsource) {
            window.NUC.trigger.convert()
            window.NUC.event.send('clickIns', 'btnInsStart')
        }
    }
    clickIns2() {
        if (this.ironsource) {
            window.NUC.trigger.convert()
            window.NUC.event.send('clickIns', 'btnEnd')
        }
    }

    update(dt) {
        if (!G.text && !this.firstTouch) {
            this.firstTouch = true;
            this.startGame();
            this.bgLayout.node.opacity = 0;
        }

        if (!this.check1st && G.totalE >= 24) {
            this.ship.setEnableBullet(false);
            this.check1st = true;
            var findDeep = function (node, nodeName) {
                if (node.name == nodeName) return node;
                for (var i = 0; i < node.children.length; i++) {
                    var res = findDeep(node.children[i], nodeName);
                    if (res)
                        res.destroy();
                    // return res;
                }
            }
            findDeep(cc.director.getScene(), "BulletAddRow0_b");
            findDeep(cc.director.getScene(), "BulletAddRow1_g");
            findDeep(cc.director.getScene(), "BulletAddRow2_g");
            findDeep(cc.director.getScene(), "BulletAddRow3_g");

            this.wave[4].active = true;
            this.scheduleOnce(() => {
                this.ship.setEnableBullet(true);
            }, 2)
        }
        if (G.totalE >= 44 && !this.check2st && !G.endG) {
            this.check2st = true;
            this.ship.setEnableBullet(false);
            G.endG = true;
            this.endGame();
        }
    }
}