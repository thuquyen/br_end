import BasePool from "./common/BasePool";

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    bullet: cc.Node[] = [];

    @property()
    public timeFire: number = 0;

    @property()
    ratioScaleBullet: number = 0;

    @property()
    round: boolean = false;

    onLoad() {
        if (this.node.name == "bulletBoss2_2" || this.node.name == 'bulletBoss5_2') {
            for (let i = 0; i < this.bullet.length; i++) {
                this.bulletShipScale(this.bullet[i]);
            }
        } else if (this.node.name == "bulletBoss_octopus") {
            for (let i = 0; i < this.bullet.length; i++) {
                this.bulletZigZag(this.bullet[i]);
            }
        }
        else if (this.node.name == 'bulletBoss7_2') {
            for (let i = 0; i < this.bullet.length; i++) {
                this.bulletTempleScale(this.bullet[i], 0.85, 0.85);
            }
        }
        else if (this.node.name == 'bulletBoss7_1') {
            for (let i = 0; i < this.bullet.length; i++) {
                this.bulletZZ(this.bullet[i]);
            }
        }
        else
            if (!this.round) {
                for (let i = 0; i < this.bullet.length; i++) {
                    this.bulletShip(this.bullet[i]);
                }
            }
            else {
                //bullet up difference xTemp - 
                // bullet horizontal need angle diff 90 and -90 because it lead to tan =0
                for (let i = 0; i < 3; i++) {
                    this.bulletShipRound(8, -2000, this.bullet[i], 2000);
                }
                for (let i = 3; i < 6; i++) {
                    this.bulletShipRound(8, 2000, this.bullet[i], -2000);
                }
                for (let i = 6; i < 8; i++) {
                    this.bulletShipRound(300, 2000, this.bullet[i], 0);
                }

                // for (let i = 0; i < 4; i++) {
                //     this.bulletShipRound_1(6, -2000, this.bullet[i], 2000);
                // }
                // for (let i = 4; i < 8; i++) {
                //     this.bulletShipRound_1(6, 2000, this.bullet[i], -2000);
                // }

            }

    }

    bulletShipRound_1(timeFire: number, xTemp: number, fire: cc.Node, yAxis: number) {
        fire.runAction(cc.sequence(cc.spawn(cc.moveBy(this.timeFire, cc.v2(xTemp * Math.tan(fire.angle * Math.PI / 180), yAxis)), cc.scaleTo(this.timeFire, 0.45, 0.45)), cc.callFunc(() => {
            fire.destroy();
        })));
    }

    bulletShip(fire: cc.Node) {
        fire.runAction(cc.sequence((cc.moveBy(this.timeFire, cc.v2(2000 * Math.tan(fire.angle * Math.PI / 180), -2000))), cc.callFunc(() => {
            fire.destroy();
        })));
    }

    bulletShipRound(timeFire: number, xTemp: number, fire: cc.Node, yAxis: number) {
        fire.runAction(cc.sequence(cc.moveBy(timeFire, cc.v2(xTemp * Math.tan(fire.angle * Math.PI / 180), yAxis)), cc.callFunc(() => {
            fire.destroy();
        })));
    }

    bulletShipScale(fire: cc.Node) {
        fire.runAction(cc.sequence(cc.spawn(cc.moveBy(this.timeFire, cc.v2(2000 * Math.tan(fire.angle * Math.PI / 180), -2000)), cc.scaleTo(this.timeFire, 5.5, 7)), cc.callFunc(() => {
            fire.destroy();
        })));
    }

    bulletTempleScale(fire: cc.Node, xScale, yScale) {
        fire.runAction(cc.sequence(cc.spawn(cc.moveBy(this.timeFire, cc.v2(2000 * Math.tan(fire.angle * Math.PI / 180), -2000)), cc.scaleTo(xScale, yScale)), cc.callFunc(() => {
            fire.destroy();
        })));
    }

    bulletZigZag(fire: cc.Node) {
        fire.runAction(cc.sequence(cc.moveBy(0.6, cc.v2((Math.random() * 2 - 1) * 70, -50)), cc.moveBy(0.6, cc.v2((Math.random() * 2 - 1) * 70, -100)), cc.moveBy(0.6, cc.v2((Math.random() * 2 - 1) * 70, -200)), cc.moveBy(this.timeFire, cc.v2(2000 * Math.tan(fire.angle * Math.PI / 180), -2000)), cc.callFunc(() => {
            fire.destroy();
        })));
    }
    bulletZZ(fire: cc.Node) {
        var ran = Math.round((Math.random())) * 2 - 1;
        fire.runAction(cc.sequence(cc.moveBy(0.5, cc.v2((Math.random() * 2 - 1) * 70, -50)),
            cc.moveBy(1.2, cc.v2(ran * this.node.parent.width / 2, -100)),
            cc.moveBy(1.2, cc.v2(-ran * this.node.parent.width / 2, -200)),
            cc.moveBy(this.timeFire, cc.v2(2000 * Math.tan(fire.angle * Math.PI / 180), -2000)),
            cc.callFunc(() => {
                fire.destroy();
            })));
    }
    // update (dt) {}
}
