
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    onLoad () {
        cc.view.enableAutoFullScreen(false);
        cc.director.getCollisionManager().enabled = true;
    }

}