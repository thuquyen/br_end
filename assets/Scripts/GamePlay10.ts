import WaveQuangCao from "./Wave/WaveQuangCao";
import ShipController from "./ShipController";
import HandController from "./HandController";
import { Helper } from "./Helper";
import G from "./Global";

const { ccclass, property } = cc._decorator;

let GameState = {
    INIT: 0,
    READY: 1,
    PLAYING: 2,
    ENDGAME: 3
}

declare const window: any;

@ccclass
export default class SpaceShooterAd2 extends cc.Component {

    gameState: number = GameState.INIT;

    @property(cc.Node)
    btnContinue: cc.Node = null;

    @property(cc.Node)
    txtMoveShip: cc.Node = null;

    @property(cc.Node)
    txtChooseShip: cc.Node = null;

    @property(cc.Node)
    loadLevel: cc.Node = null;

    @property(cc.Node)
    ships: cc.Node = null;

    @property(cc.Node)
    handOld: cc.Node = null;

    @property(WaveQuangCao)
    wave1: WaveQuangCao = null;

    @property(WaveQuangCao)
    wave2: WaveQuangCao = null

    @property(HandController)
    hand: HandController = null;

    // @property(cc.Node)
    // winPopup: cc.Node = null;

    @property(cc.Layout)
    bgLayout: cc.Layout = null;

    @property(cc.Node)
    buttonInstall: cc.Node = null;

    // @property(cc.Node)
    // textExcerllent: cc.Node = null;

    @property(cc.Prefab)
    item: cc.Prefab = null;

    @property(cc.Prefab)
    itemChangeShip: cc.Prefab[] = [];

    @property(cc.Node)
    popupEndGame: cc.Node = null;

    @property(cc.Label)
    lblScore: cc.Label = null;

    index: number = 1;

    nShip: number = 0;

    ship: ShipController = null;

    istouch: boolean = true;

    ironsource: boolean = false;

    // start() {
    //     // this.buttonA.active = true;
    //     this.textExcerllent.zIndex = 1;
    //     this.buttonInstall.zIndex = 3;
    //     this.hand.node.zIndex = 4;
    //     this.winPopup.zIndex = 3;

    //     this.gameState = GameState.INIT;
    //     this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
    //     this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
    //     this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
    //     this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);

    //     this.updateItem();
    //     this.updateChangeShip();

    //     this.wave1.appear(() => {
    //         Helper.runShowAnim(() => {
    //             this.ship.node.runAction(cc.moveBy(0.5, 0, 300).easing(cc.easeBackOut()));
    //             this.scheduleOnce(() => {
    //                 this.gameState = GameState.READY;
    //                 var pos = this.ship.node.getPosition();
    //                 this.hand.runSwipeAnim(pos.x + 30, pos.y - 50);
    //             }, 0.5);
    //         });
    //     });

    //     this.wave1.onComplete = () => {

    //         this.textExcerllent.active = true;
    //         this.textExcerllent.runAction(cc.spawn(cc.fadeIn(0.2), cc.scaleBy(0.2, 6).easing(cc.easeBackOut())));
    //         this.scheduleOnce(() => {
    //             this.textExcerllent.runAction(cc.spawn(cc.fadeOut(0.2), cc.scaleBy(0.2, 1/6).easing(cc.easeBackIn())));
    //             this.wave3.appear(() => {

    //             });
    //         }, 0.5);
    //     }

    //     this.wave3.onComplete = () => {
    //         this.gameState = GameState.ENDGAME;
    //         G.check = true;
    //         this.ship.flyOut(() => {
    //             this.showWinPopup();
    //         });
    //     }

    //     this.ship.onDie = () => {
    //         this.scheduleOnce(() => {
    //             this.showLosePopup();
    //         }, 0.5);
    //     }
    // }

    setPositionMiniShip(ship: cc.Node, miniShip: cc.Node, pos: cc.Vec2) {
        miniShip.x = ship.width / 2 + pos.x;
        miniShip.y = pos.y;
    }

    start() {

        this.txtMoveShip.zIndex = 6;
        this.loadLevel.zIndex = 20;
        this.hand.node.zIndex = 4;
        this.gameState = GameState.INIT;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
        this.ships.parent.getComponent(cc.Animation).play('animChooseShip');
        this.handOld.zIndex = 5;
        for (let i = 0; i < this.ships.childrenCount; i++) {
            this.ships.children[i].on('click', () => {
                this.chooseShipClick(i);
                this.unscheduleAllCallbacks();
            }, this);
        }

        this.wave1.onComplete = () => {
            this.wave2.appear(() => { });
        }

        this.wave2.onComplete = () => {
            if (this.ironsource)
                window.NUC.trigger.endLevel();
            this.bgLayout.node.zIndex = 10;
            this.bgLayout.node.runAction(cc.sequence(cc.fadeTo(0.2, 150), cc.callFunc(() => {
                this.loadLevel.active = true;
            })));
            this.scheduleOnce(() => {
                this.bgLayout.node.runAction(cc.sequence(cc.fadeTo(0.2, 0), cc.callFunc(() => {
                    if (this.ironsource)
                        window.NUC.trigger.endGame('win');
                    this.loadLevel.active = false;
                    this.btnContinue.active = true;
                    this.btnContinue.zIndex = 100;
                    this.popupEndGame.active = true;
                    this.ship.node.active = true;
                    this.ship.node.setPosition(cc.v2(0, -70));
                    this.ship.node.setScale(1.2);
                    this.lblScore.string = (800 + Math.floor(Math.random() * 199)).toString();
                    // this.wave2.appear(() => { });
                })));
            }, 0.8);
            if (this.buttonInstall)
                this.buttonInstall.active = false;
            this.hand.node.active = false;
            this.txtMoveShip.active = false;
            this.ship.node.active = false;
            this.ship.setEnableMove(false);
            this.ship.setEnableBullet(false);
            cc.director.getCollisionManager().enabled = false;
            this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
            this.node.off(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
            this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
            this.node.off(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
        }

        this.scheduleOnce(() => {
            if (this.ironsource)
                window.NUC.trigger.autoplay();
            this.chooseShipClick(1);
        }, 5.5);
    }

    chooseShipClick(n) {
        if (this.ironsource)
            window.NUC.trigger.startLevel();

        if (this.ironsource) {
            switch (n) {
                case 0:
                    window.NUC.event.send('choice', 'ship vang');
                    break;

                case 1:
                    window.NUC.event.send('choice', 'ship xanh');
                    break;

                case 2:
                    window.NUC.event.send('choice', 'ship trang');
                    break;

                case 3:
                    window.NUC.event.send('choice', 'ship do');
                    break;
            }
        }

        this.txtChooseShip.active = false;
        this.bgLayout.node.opacity = 0;
        this.ships.children[n].runAction(cc.sequence(cc.moveTo(0.5, cc.v2(0, -297)), cc.callFunc(() => {
            this.txtMoveShip.active = true;
            this.node.getComponent("CanvasTouchManager").enabled = true;
            this.ship = this.ships.children[n].getComponent("ShipController");
            this.ships.children[n].getComponent(cc.Button).enabled = false;
            this.startGame();
            this.gameState = GameState.READY;
            this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 50);
            this.wave1.appear(() => {
                this.scheduleOnce(() => {
                    this.ship.setEnableBullet(true);
                }, 2);
            });
        })));
        for (let i = 0; i < this.ships.childrenCount; i++) {
            if (i != n) {
                this.ships.parent.getComponent(cc.Animation).play('animChooseShipOff');
                this.ships.children[i].active = false;
            } else {
                this.ships.children[i].active = true;
            }
        }


    }

    onTouchStart(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                this.startGame();
                break;
            case GameState.PLAYING:
                this.txtMoveShip.active = false;
                // this.ship.setEnableBullet(true);
                this.hand.stopSwipeAnim();
                if (this.istouch) {
                    this.ship.setEnableBullet(true);
                    this.istouch = false;
                    this.unscheduleAllCallbacks();
                }
                break;
        }
    }

    onTouchMove(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                this.startGame();
                break;
            case GameState.PLAYING:
                this.txtMoveShip.active = false;
                // this.ship.setEnableBullet(true);
                this.hand.stopSwipeAnim();
                if (this.istouch) {
                    this.ship.setEnableBullet(true);
                    this.istouch = false;
                    this.unscheduleAllCallbacks();
                }
                break;
        }
    }

    onTouchEnd(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!this.ship.isDie) {
                    // this.ship.setEnableBullet(false);
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 50);
                }
                break;
        }
    }

    onTouchCancel(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:

                break;
        }
    }

    startGame() {
        if (this.ironsource)
            window.NUC.trigger.interaction();
        this.gameState = GameState.PLAYING;
        this.hand.stopSwipeAnim();
        this.ship.setEnableMove(true);

        this.wave1.startWave();
    }

    // showWinPopup() {
    //     G.stateWin = true;
    //     G.lazeShip = false;
    //     this.hand.node.active = false;
    //     G.check = true;
    //     this.index = -10;

    //     this.buttonInstall.active = false;
    //     cc.audioEngine.playEffect(G.finalWin, false);
    //     this.winPopup.active = true;
    //     this.winPopup.opacity = 0;
    //     setTimeout(() => {
    //         this.winPopup.opacity = 255;
    //     }, 1);
    //     this.winPopup.getComponent(sp.Skeleton).setAnimation(0, 'start_win', false);
    //     this.winPopup.getComponent(sp.Skeleton).setCompleteListener(() => {
    //         this.winPopup.getComponent(sp.Skeleton).setAnimation(0, 'loop_win', true);
    //         this.winPopup.getComponent(sp.Skeleton).setCompleteListener(null);
    //     });
    // }

    // showLosePopup() {
    //     G.stateWin = true;
    //     G.lazeShip = false;
    //     this.hand.node.active = false;
    //     G.check = true;
    //     this.index = -10;

    //     this.buttonInstall.active = false;
    //     cc.audioEngine.playEffect(G.finalWin, false);
    //     this.winPopup.active = true;
    //     this.winPopup.opacity = 0;
    //     setTimeout(() => {
    //         this.winPopup.opacity = 255;
    //     }, 1);
    //     this.winPopup.getComponent(sp.Skeleton).setAnimation(0, 'start_lose', false);
    //     this.winPopup.getComponent(sp.Skeleton).setCompleteListener(() => {
    //         this.winPopup.getComponent(sp.Skeleton).setAnimation(0, 'loop_lose', true);
    //         this.winPopup.getComponent(sp.Skeleton).setCompleteListener(null);
    //     });
    // }

    spawnItem() {
        var newItem = cc.instantiate(this.item);
        newItem.parent = this.node;
        newItem.zIndex = this.index;
        newItem.x = (this.node.x - 100) * (2 * Math.random() - 1);
        newItem.y = (this.node.y - 150) * Math.random();
    }

    updateItem() {
        this.schedule(() => {
            if (G.stateItem)
                this.spawnItem();
        }, 4, cc.macro.REPEAT_FOREVER, 2);
    }

    changeShip() {
        let prefabShip = this.itemChangeShip[this.nShip];
        this.nShip++;
        if (this.nShip >= this.itemChangeShip.length)
            this.nShip = 0;
        var newItem = cc.instantiate(prefabShip);
        newItem.parent = this.node;
        newItem.x = (this.node.x - 150) * (2 * Math.random() - 1);
        newItem.y = (this.node.y - 200) * Math.random();
        newItem.scale = 0.2;
        newItem.runAction(cc.spawn(cc.scaleTo(0.2, 1), cc.moveBy(10, cc.v2(0, -1000))));
    }

    updateChangeShip() {
        this.schedule(() => {
            this.changeShip();
        }, 6, cc.macro.REPEAT_FOREVER, 5);
    }

}