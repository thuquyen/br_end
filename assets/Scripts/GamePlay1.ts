import WaveQuangCao from "./Wave/WaveQuangCao";
import ShipController from "./ShipController";
import HandController from "./HandController";
import { Helper } from "./Helper";
import G from "./Global";

const { ccclass, property } = cc._decorator;

let GameState = {
    INIT: 0,
    READY: 1,
    PLAYING: 2,
    ENDGAME: 3
}

@ccclass
export default class SpaceShooterAd2 extends cc.Component {

    gameState: number = GameState.INIT;

    @property(WaveQuangCao)
    wave1: WaveQuangCao = null;

    @property(WaveQuangCao)
    wave2: WaveQuangCao = null;

    @property(WaveQuangCao)
    wave3: WaveQuangCao = null;

    @property(ShipController)
    ship: ShipController = null;

    @property(HandController)
    hand: HandController = null;

    @property(cc.Node)
    winPopup: cc.Node = null;

    @property(cc.Layout)
    bgLayout: cc.Layout = null;

    @property(cc.Node)
    buttonInstall: cc.Node = null;

    @property(cc.Node)
    textExcerllent: cc.Node = null;

    @property(cc.Prefab)
    item: cc.Prefab = null;

    @property(cc.Prefab)
    itemChangeShip: cc.Prefab[] = [];

    index: number = 1;

    nShip: number = 0;

    start() {
        // this.buttonA.active = true;
        this.textExcerllent.zIndex = 1;
        this.buttonInstall.zIndex = 3;
        this.hand.node.zIndex = 4;
        this.winPopup.zIndex = 3;

        this.gameState = GameState.INIT;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);

        this.updateItem();
        this.updateChangeShip();

        this.wave1.appear(() => {
            Helper.runShowAnim(() => {
                this.ship.node.runAction(cc.moveBy(0.5, 0, 300).easing(cc.easeBackOut()));
                this.scheduleOnce(() => {
                    this.gameState = GameState.READY;
                    var pos = this.ship.node.getPosition();
                    this.hand.runSwipeAnim(pos.x + 30, pos.y - 50);
                }, 0.5);
            });
        });

        this.wave1.onComplete = () => {
            this.textExcerllent.active = true;
            this.textExcerllent.runAction(cc.sequence(cc.spawn(cc.fadeIn(0.2), cc.scaleTo(0.2, 0.5).easing(cc.easeBackOut())), cc.callFunc(() => {
                
                cc.Camera.main.node.getComponent(cc.Animation).play("animCamera");
            })));
            this.scheduleOnce(() => {
                this.wave2.appear(() => {
                    this.textExcerllent.runAction(cc.fadeOut(0.2));
                });
            }, 0.8);
        }

        this.wave2.onComplete = () => {
            this.textExcerllent.active = true;
            this.textExcerllent.runAction(cc.sequence(cc.spawn(cc.fadeIn(0.2), cc.scaleTo(0.2, 0.5).easing(cc.easeBackOut())), cc.callFunc(() => {
                cc.Camera.main.node.getComponent(cc.Animation).play("animCamera");
            })));
            this.scheduleOnce(() => {
                this.wave3.appear(() => {
                    this.textExcerllent.runAction(cc.fadeOut(0.2));
                });
            }, 0.8);
        }

        this.wave3.onComplete = () => {
            this.gameState = GameState.ENDGAME;
            G.check = true;
            this.ship.flyOut(() => {
                this.showWinPopup();
            });
        }

        this.ship.onDie = () => {
            this.scheduleOnce(() => {
                this.showLosePopup();
            }, 0.5);
        }
    }

    onTouchStart(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                this.startGame();
                break;
            case GameState.PLAYING:
                this.hand.stopSwipeAnim();
                break;
        }
    }

    onTouchMove(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                this.startGame();
                break;
            case GameState.PLAYING:
                this.hand.stopSwipeAnim();
                break;
        }
    }

    onTouchEnd(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!this.ship.isDie) {
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 50);
                }
                break;
        }
    }

    onTouchCancel(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                break;
        }
    }

    startGame() {
        this.gameState = GameState.PLAYING;
        this.hand.stopSwipeAnim();
        this.ship.setEnableMove(true);
        this.ship.setEnableBullet(true);
        this.wave1.startWave();
    }

    showWinPopup() {
        G.stateWin = true;
        G.lazeShip = false;
        this.hand.node.active = false;
        G.check = true;
        this.index = -10;

        this.buttonInstall.active = false;
        cc.audioEngine.playEffect(G.finalWin, false);
        this.winPopup.active = true;
        this.winPopup.opacity = 0;
        setTimeout(() => {
            this.winPopup.opacity = 255;
        }, 1);
        this.winPopup.getComponent(sp.Skeleton).setAnimation(0, 'start_win', false);
        this.winPopup.getComponent(sp.Skeleton).setCompleteListener(() => {
            this.winPopup.getComponent(sp.Skeleton).setAnimation(0, 'loop_win', true);
            this.winPopup.getComponent(sp.Skeleton).setCompleteListener(null);
        });
    }

    showLosePopup() {
        G.stateWin = true;
        G.lazeShip = false;
        this.hand.node.active = false;
        G.check = true;
        this.index = -10;

        this.buttonInstall.active = false;
        cc.audioEngine.playEffect(G.finalWin, false);
        this.winPopup.active = true;
        this.winPopup.opacity = 0;
        setTimeout(() => {
            this.winPopup.opacity = 255;
        }, 1);
        this.winPopup.getComponent(sp.Skeleton).setAnimation(0, 'start_lose', false);
        this.winPopup.getComponent(sp.Skeleton).setCompleteListener(() => {
            this.winPopup.getComponent(sp.Skeleton).setAnimation(0, 'loop_lose', true);
            this.winPopup.getComponent(sp.Skeleton).setCompleteListener(null);
        });
    }

    spawnItem() {
        var newItem = cc.instantiate(this.item);
        newItem.parent = this.node;
        newItem.zIndex = this.index;
        newItem.x = (this.node.x - 100) * (2 * Math.random() - 1);
        newItem.y = (this.node.y - 150) * Math.random();
    }

    updateItem() {
        this.schedule(() => {
            if (G.stateItem)
                this.spawnItem();
        }, 4, cc.macro.REPEAT_FOREVER, 2);
    }

    changeShip() {
        let prefabShip = this.itemChangeShip[this.nShip];
        this.nShip++;
        if (this.nShip >= this.itemChangeShip.length)
            this.nShip = 0;
        var newItem = cc.instantiate(prefabShip);
        newItem.parent = this.node;
        newItem.x = (this.node.x - 150) * (2 * Math.random() - 1);
        newItem.y = (this.node.y - 200) * Math.random();
        newItem.scale = 0.2;
        newItem.runAction(cc.spawn(cc.scaleTo(0.2, 1), cc.moveBy(10, cc.v2(0, -1000))));
    }

    updateChangeShip() {
        this.schedule(() => {
            this.changeShip();
        }, 6, cc.macro.REPEAT_FOREVER, 5);
    }

}