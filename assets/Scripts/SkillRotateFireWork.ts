
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    bullet: cc.Node[] = [];

    // @property()
    // public timeFire: number = 0;
    start() {
        this.scheduleOnce(() => {
            this.fire()
        }, 0.5)
    }
    fire() {
        if (this.node.activeInHierarchy) {
            this.node.scale = 0.4
            this.node.runAction(cc.scaleTo(1, 0.9, 0.9).easing(cc.easeSineOut()));
            this.node.runAction(cc.sequence(
                cc.moveTo(2, cc.v2(this.node.x, 1000)),
                cc.callFunc(() => {
                    this.node.getComponent(cc.Animation).enabled = false;
                    this.node.destroy();
                })
            ))
        }
    }

    private bulletShip(fire: cc.Node, axisY: number, timeFire: number) {
        if (fire.activeInHierarchy)
            fire.runAction(cc.sequence(cc.moveBy(timeFire, cc.v2(-2000 * Math.tan(fire.angle * Math.PI / 180), axisY)), cc.callFunc(() => {
                fire.destroy();
            })));
    }

}
