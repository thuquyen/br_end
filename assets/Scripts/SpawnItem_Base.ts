import G from "./Global";
const { ccclass, property } = cc._decorator;

@ccclass
export default class SpawnItem extends cc.Component {


    @property(cc.Prefab)
    item: cc.Prefab[] = [];
    check1: number = 0;
    check2: number = 0;
    check3: number = 0;

    @property(cc.Prefab)
    txt: cc.Prefab[] = [];
    // LIFE-CYCLE CALLBACKS:
    txt2: boolean = false;
    txt1: boolean = false;

    start() {
        this.updateItem();
    }

    spawnItem() {
        var item1 = cc.instantiate(this.item[0]);
        item1.parent = this.node;
        item1.zIndex = 1;
        item1.x = (this.node.x - 100) * (2 * Math.random() - 1);
        item1.y = (this.node.y - 20) * Math.random();

        if (!this.txt1) {
            this.txt1 = true;
            var txt1 = cc.instantiate(this.txt[0]);
            txt1.parent = this.node;
            txt1.zIndex = 1;
            txt1.x = 0;
            txt1.y = -150;
            txt1.runAction(cc.sequence(cc.moveBy(2, cc.v2(0, -80)),
                cc.callFunc(() => {
                    txt1.destroy();
                })));
        }


        // this.scheduleOnce(() => {
        //     if (this.check1 <= 1) {
        //         this.check1++;
        //         var item0 = cc.instantiate(this.item[1]);
        //         item0.parent = this.node;
        //         item0.zIndex = 10;
        //         item0.x = (this.node.x - 90) * (2 * Math.random() - 1);
        //         item0.y = (this.node.y - 20) * Math.random();

        //         if (!this.txt2) {
        //             this.txt2 = true;
        //             var txt0 = cc.instantiate(this.txt[1]);
        //             txt0.parent = this.node;
        //             txt0.zIndex = 1;
        //             txt0.x = 0;
        //             txt0.y = -150;
        //             txt0.runAction(cc.sequence(cc.moveBy(2, cc.v2(0, -80)),
        //                 cc.callFunc(() => {
        //                     txt0.destroy();
        //                 })));
        //         }

        //     }
        // }, 6)
    }

    updateItem() {
        this.schedule(() => {
            if (!G.endG && G.startSpawnItem) {
                this.spawnItem();
            }
        }, 5, cc.macro.REPEAT_FOREVER, 1);
    }
}
