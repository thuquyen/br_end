import WaveQuangCao from "./Wave/WaveQuangCao";
import ShipController from "./Ship45";
import HandController from "./HandController";
import { Helper } from "./Helper";
import G from "./Global";
import SpawnItem from "./SpawnItem_Base";

const { ccclass, property } = cc._decorator;

let GameState = {
    INIT: 0,
    READY: 1,
    PLAYING: 2,
    ENDGAME: 3
}

declare const window: any;

@ccclass
export default class SpaceShooterAd2 extends cc.Component {

    gameState: number = GameState.INIT;

    @property(cc.Node)
    endG: cc.Node = null;

    @property(cc.Node)
    Layout_bg: cc.Node = null;

    @property(cc.Node)
    txtMoveShip: cc.Node = null;

    @property(HandController)
    hand: HandController = null;

    @property(cc.Node)
    buttonInstall: cc.Node = null;

    @property(cc.Node)
    titleMini: cc.Node = null;

    index: number = 1;

    nShip: number = 0;

    @property(ShipController)
    ship: ShipController = null;
    istouch: boolean = true;

    ironsource: boolean = false;

    mindworks: boolean = true;

    @property({ type: cc.AudioClip })
    bgSound: cc.AudioClip = null;
    temp: number = 0;

    check1st: boolean = false;
    check2st: boolean = false;

    firstTouch: boolean = false;
    posX: number;
    posY: number;
    checkEnd: boolean = false;

    @property(cc.Node)
    tempBoss: cc.Node = null;

    @property(cc.Node)
    menu: cc.Node = null;

    check_play: boolean = false;

    @property(cc.Prefab)
    rocket_Prefab: cc.Prefab;

    @property(cc.Prefab)
    bullet_axis: cc.Prefab[] = [];

    @property(cc.Node)
    axis: cc.Node = null;

    @property(cc.Node)
    axis_children: cc.Node[] = [];

    onLoad() {
        cc.view.enableAutoFullScreen(false);
    }

    start() {
        this.temp = cc.audioEngine.play(this.bgSound, true, 1);
        this.scheduleOnce(() => {
            if (!this.check_play) {
                this.check_play = true;
                this.btnPlay();
            }
        }, 5.5)
        this.txtMoveShip.zIndex = 100;
        cc.director.getCollisionManager().enabled = true;
        this.ship.node.zIndex = 1;
        // this.endG.zIndex = 3;

        this.gameState = GameState.INIT;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
        if (this.ironsource) {
            window.NUC.trigger.ready("ready")
        }
    }

    rocket_fire() {
        this.scheduleOnce(() => {
            this.spawn_rocket(this.node.width / 3);
            this.scheduleOnce(() => {
                this.spawn_rocket(-this.node.width / 4);
                this.scheduleOnce(() => {
                    this.spawn_rocket(0);
                    this.scheduleOnce(() => {
                        this.spawn_rocket(-this.node.width / 3);
                        this.scheduleOnce(() => {
                            this.spawn_rocket(this.node.width / 4);
                            this.scheduleOnce(() => {
                                this.spawn_rocket(this.node.width / 5);
                                this.scheduleOnce(() => {
                                    this.spawn_rocket(-this.node.width / 3);
                                }, 0.8)
                                this.scheduleOnce(() => {
                                    this.spawn_rocket(0);
                                    this.scheduleOnce(() => {
                                        this.spawn_rocket(this.node.width / 4);
                                        this.scheduleOnce(() => {
                                            cc.director.getCollisionManager().enabled = false;
                                            this.tempBoss.runAction(cc.sequence(cc.moveBy(0.5, cc.v2(0, 85)), cc.scaleTo(0.5, 0.9, 0.9),
                                                cc.callFunc(() => {
                                                    for (var i = 0; i < this.axis_children.length; i++) {
                                                        this.axis_children[i].getComponent(cc.BoxCollider).enabled = true;
                                                    }
                                                    G.allow_fire = true;
                                                    cc.director.getCollisionManager().enabled = true;
                                                    this.axis_fire();
                                                })));
                                        }, 1)
                                    }, 0.7)
                                }, 1)
                            }, 0.7)
                        }, 0.8)
                    }, 0.5)
                }, 0.7)
            }, 0.5)
        }, 1)
    }

    btnPlay() {
        G.allow_fire = false;
        this.check_play = true;
        this.menu.destroy();
        this.appearShip();
        this.tempBoss.runAction(cc.sequence(cc.moveBy(0.5, cc.v2(0, -50)), cc.scaleTo(0.5, 0, 0)));
        this.rocket_fire();
    }

    axis_fire() {
        this.schedule(() => {
            if (!G.endG && this.axis.activeInHierarchy && !G.text) {
                this.spawn_bullet_axis(this.bullet_axis[0]);
                this.scheduleOnce(() => {
                    this.spawn_bullet_axis(this.bullet_axis[0]);
                    this.scheduleOnce(() => {
                        this.spawn_bullet_axis(this.bullet_axis[0]);
                        this.scheduleOnce(() => {
                            this.spawn_bullet_axis(this.bullet_axis[1]);
                            this.scheduleOnce(() => {
                                this.spawn_bullet_axis(this.bullet_axis[1]);
                            }, 1.2)
                        }, 1)
                    }, 0.4)
                }, 0.4)
            }
        }, 5, cc.macro.REPEAT_FOREVER, 1)
    }

    spawn_bullet_axis(bullet_Prefab: cc.Prefab) {
        if (this.axis.activeInHierarchy) {
            var bullet = cc.instantiate(bullet_Prefab);
            bullet.parent = cc.Canvas.instance.node;
            bullet.x = this.axis.x;
            bullet.y = this.axis.y + 70;
        }
    }

    appearShip() {
        this.buttonInstall.active=true;
        this.ship.node.runAction(cc.sequence(cc.moveTo(0.8, cc.v2(0, -290)).easing(cc.easeBackOut()), cc.callFunc(() => {
            this.ship.setEnableBullet(true);
            G.startSpawnItem = true;
            this.posX = this.ship.node.x;
            this.posY = this.ship.node.y;
            this.node.getComponent("CanvasTouchManager").enabled = true;
            this.gameState = GameState.READY;
        })))
        this.scheduleOnce(() => {
            this.turnOnGuide();
        }, 1)
    }

    turnOnGuide() {
        this.txtMoveShip.active = true;
        this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
        this.hand.node.zIndex = 12;
    }

    endGame() {
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        this.ship.flyOut();
        this.buttonInstall.active=false;
        cc.audioEngine.stop(this.temp);
        cc.audioEngine.playEffect(G.finalWin, false);
        this.Layout_bg.active = true;
        G.text = false;
        this.checkEnd = true;

        if (this.ironsource) {
            window.NUC.trigger.endGame('win')
        }
        this.scheduleOnce(() => {
            this.endG.active = true;
        }, 1.2)

        this.hand.node.active = false;
        this.txtMoveShip.active = false;
        this.ship.setEnableMove(false);

        this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    }

    onTouchStart(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                G.clickStart = true;
                this.startGame();
                break;
            case GameState.PLAYING:
                this.txtMoveShip.active = false;
                this.hand.stopSwipeAnim();
                if (this.check_play) {
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                }
                break;
        }
    }

    onTouchMove(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                G.clickStart = true;
                this.startGame();
                break;
            case GameState.PLAYING:
                this.txtMoveShip.active = false;
                this.hand.stopSwipeAnim();
                if (this.check_play) {
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                }
                break;
        }
    }

    onTouchEnd(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!this.ship.isDie && this.check_play) {
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
                    this.ship.setEnableBullet(false);
                }
                break;
        }
    }

    onTouchCancel(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!this.ship.isDie && this.check_play) {
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
                    this.ship.setEnableBullet(false);
                }
                break;
        }
    }

    startGame() {
        if (this.ironsource)
            window.NUC.trigger.interaction();
        this.gameState = GameState.PLAYING;
        this.hand.stopSwipeAnim();
        this.ship.setEnableMove(true);

    }

    update(dt) {
        if (G.totalE >= 5 && !this.check1st) {
            this.check1st = true;
            this.axis.getComponent(cc.CircleCollider).enabled = true;
        }
        if (G.totalE >= 6 && !G.endG) {
            G.endG = true;
            this.endGame();
        }
    }

    spawn_rocket(xTemp: number) {
        var rocket = cc.instantiate(this.rocket_Prefab);
        rocket.parent = cc.Canvas.instance.node;
        rocket.x = xTemp;
        rocket.y = this.node.height / 2
    }
}