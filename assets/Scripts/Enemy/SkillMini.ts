import G from "../Global";

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Prefab)
    bullet: cc.Prefab = null;
    start() {
        this.scheduleOnce(() => {
            this.schedule(() => {
                // if (G.clickStart && !G.isWheel) {
                if (G.clickStart) {
                    this.fire();
                }
            }, 1, cc.macro.REPEAT_FOREVER, 0.1)
        }, 3)
    }
    fire() {
        var bullet = cc.instantiate(this.bullet);
        bullet.parent = cc.Canvas.instance.node;
        bullet.x = this.node.x;
        bullet.y = this.node.y;
    }
    // update (dt) {}
}
