import G from "../Global";
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Prefab)
    bullet_axis: cc.Prefab[] = [];

    @property(cc.Node)
    axis: cc.Node = null;

    @property(cc.Node)
    axis_children: cc.Node[] = [];

    check_axis: boolean = false;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {
        this.node.runAction(cc.sequence(cc.moveBy(0.8, cc.v2(0, 85)), cc.scaleTo(0.8, 0.95, 0.95)));
        for (var i = 0; i < this.axis_children.length; i++) {
            this.axis_children[i].getComponent(cc.BoxCollider).enabled = true;
        }
        this.scheduleOnce(()=>{
            this.axis_fire();
        },1.5)
       
    }

    axis_fire() {
        this.schedule(() => {
            // if (!G.endG && this.axis.activeInHierarchy && !G.text)
            if (!G.endG && this.axis.activeInHierarchy) {
                this.spawn_bullet_axis(this.bullet_axis[0]);
                this.scheduleOnce(() => {
                    this.spawn_bullet_axis(this.bullet_axis[0]);
                    this.scheduleOnce(() => {
                        this.spawn_bullet_axis(this.bullet_axis[0]);
                        this.scheduleOnce(() => {
                            this.spawn_bullet_axis(this.bullet_axis[1]);
                            this.scheduleOnce(() => {
                                this.spawn_bullet_axis(this.bullet_axis[1]);
                            }, 0.4)
                        }, 1.2)
                    }, 0.3)
                }, 0.3)
            }
        }, 4, cc.macro.REPEAT_FOREVER, 0.1)
    }

    spawn_bullet_axis(bullet_Prefab: cc.Prefab) {
        if (this.axis.activeInHierarchy) {
            var bullet = cc.instantiate(bullet_Prefab);
            bullet.parent = cc.Canvas.instance.node;
            bullet.x = this.axis.x;
            bullet.y = this.axis.y + 70;
        }
    }

    update(dt) {
        if (!this.check_axis && G.e_groupBoss >= 5){
            this.check_axis = true;
            this.axis.getComponent(cc.CircleCollider).enabled = true;
        }
    }
}
