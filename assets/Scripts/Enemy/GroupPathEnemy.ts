import BaseEnemy, { EnemyEvent } from "./BaseEnemy";
import { Helper } from "../Helper";
import IEnemy from "./IEnemy";
import ESkill from "./ESkill";
import ESkillSpawnBullet, { ConfigSpawnBullet } from "./ESkillSpawnBullet";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GroupPathEnemy extends IEnemy {
    @property(cc.Node)
    public nodeStartPath: cc.Node = null;

    @property(cc.Node)
    public nodeEndPoints: cc.Node = null;

    public startPath: cc.Vec2[] = [];

    public endPoints: cc.Vec2[] = [];

    @property(Boolean)
    public loopForever: boolean = false;

    // @property(cc.Prefab)
    // public prefab: cc.Prefab = null;

    // @property([cc.Prefab])
    // public prefabs: cc.Prefab[] = [];

    @property(Number)
    public delay: number = 0;
    @property(Number)
    public timeDistance: number = 0.2;
    @property(Number)
    public speed: number = 200;
    @property(Number)
    public angleOffset: number = 90;
    @property(Boolean)
    public isRotate: boolean = true;

    public enemies: BaseEnemy[] = null;

    private _nDie: number = 0;
    private _nAppear: number = 0;

    public onAppearDone: Function = null;

    constructor() {
        super();
        this.enemies = [];
    }

    onLoad() {
        if (this.nodeStartPath) {
            this.nodeStartPath.active = false;
        }
        if (this.nodeEndPoints) {
            this.nodeEndPoints.active = false;
        }
    }

    start() {
        this.appear();
    }

    appear() {
        if (((this.startPath === null) || (this.startPath.length == 0)) && (this.nodeStartPath != null)) {
            this.startPath = Helper.getSequencePoints(this.nodeStartPath);
        }
        if (((this.endPoints === null) || (this.endPoints.length == 0)) && (this.endPoints != null)) {
            this.endPoints = Helper.getSequencePoints(this.nodeEndPoints);
        }

        this._nDie = this.endPoints.length;
        this._nAppear = this.endPoints.length;

        if (this.loopForever) {
            this.appearLoop();
        } else {
            this.appearNormal();
        }
    }

    checkAppearDone() {
        return this._nAppear <= 0;
    }

    appearNormal() {
        this.scheduleOnce(() => {
            this.endPoints.forEach((endPoint, i) => {
                this.scheduleOnce(() => {
                    var e = this.spawnEnemy(i);
                    (<any>e)._appear = false;
                    e.x = 1000;
                    e.y = 1000;
                    e.parent = cc.Canvas.instance.node;
                    e.getComponent(BaseEnemy).onDie = () => {
                        if ((<any>e)._appear === false) {
                            this._nAppear--;
                            if (this.checkAppearDone()) {
                                this.emitAppearedAllEnemy();
                                if (this.onAppearDone)
                                    this.onAppearDone();
                            }
                        }
                        this._nDie--;
                        if (this._nDie <= 0) {
                            if (this.onDie)
                                this.onDie();
                        }
                    };
                    this.enemies.push(e.getComponent(BaseEnemy));
                    if (this.startPath.length == 1) {
                        e.x = this.startPath[0].x;
                        e.y = this.startPath[0].y;
                        if (this.isRotate) {
                            Helper.moveToAndAdjustAngle(e, endPoint, this.speed / 1.5, () => {
                                this._nAppear--;
                                (<any>e)._appear = true;
                                if (this.checkAppearDone()) {
                                    this.emitAppearedAllEnemy();
                                    if (this.onAppearDone)
                                        this.onAppearDone();
                                }
                            });
                        } else {
                            Helper.moveTo(e, endPoint, this.speed / 1.5, () => {
                                this._nAppear--;
                                (<any>e)._appear = true;
                                if (this.checkAppearDone()) {
                                    this.emitAppearedAllEnemy();
                                    if (this.onAppearDone)
                                        this.onAppearDone();
                                }
                            });
                        }
                    } else {
                        if (this.isRotate) {
                            Helper.splineFollowAndRotate(e, this.startPath, this.speed, 0, this.angleOffset, () => {
                                Helper.moveToAndAdjustAngle(e, endPoint, this.speed / 1.5, () => {
                                    this._nAppear--;
                                    if (this.checkAppearDone()) {
                                        this.emitAppearedAllEnemy();
                                        if (this.onAppearDone)
                                            this.onAppearDone();
                                    }
                                });
                            });
                        } else {
                            Helper.splineFollow(e, this.startPath, this.speed, 0, this.angleOffset, () => {
                                Helper.moveTo(e, endPoint, this.speed / 1.5, () => {
                                    this._nAppear--;
                                    (<any>e)._appear = true;
                                    if (this.checkAppearDone()) {
                                        this.emitAppearedAllEnemy();
                                        if (this.onAppearDone)
                                            this.onAppearDone();
                                    }
                                });
                            });
                        }
                    }
                }, i * this.timeDistance);
            });
        }, this.delay);
    }

    appearLoop() {
        this.scheduleOnce(() => {
            this.endPoints.forEach((endPoint, i) => {
                if (this.isRotate) {
                    this.scheduleOnce(() => {
                        var e = this.spawnEnemy(i);
                        e.x = 1000;
                        e.y = 1000;
                        e.parent = cc.Canvas.instance.node;
                        e.getComponent(BaseEnemy).onDie = () => {
                            this._nDie--;
                            if (this._nDie <= 0) {
                                if (this.onDie)
                                    this.onDie();
                            }
                        };
                        this.enemies.push(e.getComponent(BaseEnemy));
                        if (this.startPath.length == 1) {
                            e.x = this.startPath[0].x;
                            e.y = this.startPath[0].y;
                            Helper.moveToAndAdjustAngle(e, endPoint, this.speed / 1.5, () => {
                                this._nAppear--;
                                if (this._nAppear <= 0) {
                                    this.emitAppearedAllEnemy();
                                    if (this.onAppearDone)
                                        this.onAppearDone();
                                }
                            });
                        } else {
                            Helper.splineFollowAndRotateLoop(e, this.startPath, this.speed, 0, this.angleOffset);
                        }
                    }, i * this.timeDistance);
                } else {
                    this.scheduleOnce(() => {
                        var e = this.spawnEnemy(i);
                        e.x = 1000;
                        e.y = 1000;
                        e.parent = cc.Canvas.instance.node;
                        e.getComponent(BaseEnemy).onDie = () => {
                            this._nDie--;
                            if (this._nDie <= 0) {
                                if (this.onDie)
                                    this.onDie();
                            }
                        };
                        this.enemies.push(e.getComponent(BaseEnemy));
                        if (this.startPath.length == 1) {
                            e.x = this.startPath[0].x;
                            e.y = this.startPath[0].y;
                            Helper.moveTo(e, endPoint, this.speed / 1.5, () => {
                                this._nAppear--;
                                if (this._nAppear <= 0) {
                                    this.emitAppearedAllEnemy();
                                    if (this.onAppearDone)
                                        this.onAppearDone();
                                }
                            });
                        } else {
                            Helper.splineFollowLoop(e, this.startPath, this.speed, 0, this.angleOffset);
                        }
                    }, i * this.timeDistance);
                }
            });
        }, this.delay);
    }

    private emitAppearedAllEnemy() {
        for (var i = 0; i < this.enemies.length; i++) {
            if (this.enemies[i] && this.enemies[i].node && this.enemies[i].node.isValid)
                this.enemies[i].node.emit(EnemyEvent.APPEARED.toString());
        }
    }

    private spawnEnemy(i) {
        // if(this.prefabs && this.prefabs.length > 0 && this.prefabs.length>i) {
        //     return cc.instantiate(this.prefabs[i]);
        // } else if(this.prefab) {
        //     return cc.instantiate(this.prefab);
        // } else {
        //     var node = this.nodeEndPoints.children[0];
        //     node.removeFromParent();
        //     return node;
        // }

        var node = this.nodeEndPoints.children[0];
        node.removeFromParent();
        return node;
    }

}
