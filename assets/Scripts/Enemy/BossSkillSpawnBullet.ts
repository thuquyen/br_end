import ESkill from "./ESkill";
import G from "../Global";

const { ccclass, property } = cc._decorator;

@ccclass('ConfigSpawnBullet1')
export class ConfigSpawnBullet1 {

    @property(Number)
    delay: number = 3.5;

    @property(Number)
    interval: number = 3;

    @property(Number)
    deltaInterval: number = 1;

    @property(Number)
    speed: number = 300;

    @property(Number)
    deltaSpeed: number = 100;

    @property(String)
    pattern: String = '9';

}

@ccclass
export default class ESkillSpawnBullet extends ESkill {

    @property(cc.Prefab)
    bullet: cc.Prefab[] = [];

    @property()
    time: number = 0;

    @property()
    iBullet: number = 0;

    private skillBoss: cc.Prefab = null;

    @property(ConfigSpawnBullet1)
    config: ConfigSpawnBullet1 = new ConfigSpawnBullet1();
    private i = 0;
    strAnimCurrent: string = '';
    temp: number = 0;
    temp1: number = 0;
    onLoad() {
        this.node.zIndex = 10;
    }
    start() {
        if (this.node.name == "boss_S" || this.node.name == "boss_S_2" || this.node.name == "boss_S_mini") {
            this.getComponent(sp.Skeleton).setMix('attack1', 'idle', 0.2);
            this.getComponent(sp.Skeleton).setMix('idle', 'attack1', 0.1);
        }
        else if (this.name == "boss_S_4" || this.name == 'boss_S_4_mini') {
            this.getComponent(sp.Skeleton).setMix('attack5', 'idle', 0.2);
            this.getComponent(sp.Skeleton).setMix('idle', 'attack5', 0.1);
        }
        else if (this.node.name == "boss_S_1") {
            this.getComponent(sp.Skeleton).setMix('attack3', 'idle', 0.1);
            this.getComponent(sp.Skeleton).setMix('idle', 'attack3', 0.1);
            this.getComponent(sp.Skeleton).setMix('attack4', 'idle', 0.1);
            this.getComponent(sp.Skeleton).setMix('idle', 'attack4', 0.1);
            this.getComponent(sp.Skeleton).setMix('attack3', 'attack4', 0.1);
            this.getComponent(sp.Skeleton).setMix('attack4', 'attack3', 0.1);
        }
        else if (this.node.name == 'boss28_oragane' || this.node.name == 'boss_event') {
            this.getComponent(sp.Skeleton).setMix('attack', 'idle', 0.1);
            this.getComponent(sp.Skeleton).setMix('idle', 'attack', 0.1);
            this.changeAnimation(1, 'idle', true);
        }

        else if (this.node.name == 'boss_S_006') {
            this.getComponent(sp.Skeleton).setMix('attack1', 'idle', 0.1);
            this.getComponent(sp.Skeleton).setMix('idle', 'attack1', 0.1);
        }
        else if (this.node.name == 'boss_octopus') {
            this.getComponent(sp.Skeleton).setMix('idle2', 'tranform2', 0.1);
            this.getComponent(sp.Skeleton).setMix('tranform2', 'idle', 0.1);
            this.changeAnimation(1, 'idle2', true);
            this.scheduleOnce(() => {
                this.skill5();
            }, 2.4)
        }
        else if (this.node.name == "boss_35") {
            this.getComponent(sp.Skeleton).setMix('idle', 'skill_1_1', 0.1);
            this.getComponent(sp.Skeleton).setMix('skill_1_1', 'skill_1_2', 0.1);
            this.getComponent(sp.Skeleton).setMix('skill_1_2', 'skill_1_3', 0.1);
            this.getComponent(sp.Skeleton).setMix('skill_1_3', 'idle', 0.1);
            this.getComponent(sp.Skeleton).setMix('idle', 'skill_5_1', 0.1);
            this.getComponent(sp.Skeleton).setMix('skill_5_1', 'skill_5_2', 0.1);
            this.getComponent(sp.Skeleton).setMix('skill_5_2', 'skill_5_3', 0.1);
            this.getComponent(sp.Skeleton).setMix('skill_5_3', 'idle', 0.1);
        }

        // this.changeAnimation(1, 'idle', true);

        this.scheduleOnce(() => {
            this.schedule(() => {
                if (!G.text) {
                // let x = cc.Canvas.instance.node.width / 2 * (Math.random() - 0.5);
                // let x = (Math.random() * 2 - 1) * 55;
                let x = (Math.random() * 2 - 1) * 50;
                // console.log("postion Boss " + x)
                this.node.runAction(cc.sequence(cc.moveTo(1, cc.v2(x, this.node.y)), cc.callFunc(() => {
                    if (this.node.name == "boss_S_1" || this.node.name == 'w9_bossEvent') {
                        if (G.hpBoss)
                            this.bulletBoss1(120, 105);
                    }
                    else if (this.node.name == "boss_S" || this.node.name == "boss_S_mini") {
                        if (G.hpBoss)
                            this.bulletBoss3();
                    }
                    else if (this.node.name == "boss_S_2" || this.node.name == 'boss_S_006') {
                        if (G.hpBoss)
                            this.bulletBoss2();
                    }
                    else if (this.node.name == "boss_S_4" || this.name == 'boss_S_4_mini') {
                        this.bulletBoss4();
                    }
                    else if (this.node.name == 'boss28_oragane') {
                        this.bulletBoss3();
                    }
                    else if (this.node.name == 'boss_octopus') {
                        if (G.boss1) {
                            this.bulletBossOctopus(100, -60)
                        }
                    }
                    else if (this.node.name == 'boss_35') {
                        // if (G.boss1) {
                        this.scheduleOnce(() => {
                            this.bulletBoss5();
                        }, 0.5)

                        // }
                    }
                })));
                }
            }, this.config.interval, cc.macro.REPEAT_FOREVER, this.config.delay);
        }, this.config.delay);
    }

    skill5() {
        this.changeAnimation(1, 'tranform2', false);
        this.scheduleOnce(() => {
            this.changeAnimation(2, 'idle', true);
        }, 0.5)
    }

    skill3() {
        if (this.iBullet === 1) {
            this.changeAnimation(1, 'attack', true);
            this.scheduleOnce(() => {
                this.changeAnimation(1, 'idle', true);
            }, 3);
        } else if (this.iBullet === 0) {
            this.changeAnimation(1, 'idle', true);
        }
    }
    skill1() {
        if (this.iBullet === 1) {
            this.changeAnimation(1, 'attack3', true);
            this.scheduleOnce(() => {
                this.changeAnimation(1, 'attack4', true);
                this.scheduleOnce(() => {
                    this.changeAnimation(1, 'idle', true);
                }, 2.5);
            }, 2.1);
        }
        // else if (this.iBullet === 0) {
        //     this.changeAnimation(1, 'idle', true);
        // }
    }
    skill2() {
        if (this.iBullet === 1) {
            this.changeAnimation(1, 'attack1', true);
            this.scheduleOnce(() => {
                this.changeAnimation(1, 'idle', true);
            }, 3.5);
        } else if (this.iBullet === 0) {
            this.changeAnimation(1, 'idle', true);
        }
    }
    skill4() {
        if (this.iBullet === 1) {
            this.changeAnimation(1, 'attack5', true);
            this.scheduleOnce(() => {
                this.changeAnimation(1, 'idle', true);
            }, 1);
        } else if (this.iBullet === 0) {
            this.changeAnimation(1, 'idle', true);
        }
    }

    //boss35
    skill6() {
        if (this.iBullet === 0) {
            this.changeAnimation(1, 'skill_1_1', true);
            this.scheduleOnce(() => {
                this.changeAnimation(1, 'skill_1_2', true);
                this.scheduleOnce(() => {
                    this.changeAnimation(1, 'skill_1_3', true);
                    this.scheduleOnce(() => {
                        this.changeAnimation(1, 'idle', true);
                    }, 0.6);
                }, 1.9);
            }, 0.65);
        }
        // else if (this.iBullet === 0) {
        //     this.changeAnimation(1, 'idle', true);
        // }
        else if (this.iBullet === 1) {
            this.changeAnimation(1, 'skill_5_1', true);
            this.scheduleOnce(() => {
                this.changeAnimation(1, 'skill_5_2', true);
                this.scheduleOnce(() => {
                    this.changeAnimation(1, 'skill_5_3', true);
                    this.scheduleOnce(() => {
                        this.changeAnimation(1, 'idle', true);
                    }, 0.4);
                }, 1.5);
            }, 0.45);
        }
    }

    changeAnimation(track: number, str: string, bool: boolean) {
        if (str === this.strAnimCurrent) {
            return;
        }
        this.getComponent(sp.Skeleton).setAnimation(track, str, bool);
        this.strAnimCurrent = str;
    }
    public bulletBossOctopus(xTemp: number, yTemp: number) {
        this.scheduleOnce(() => {
            this.skillBoss = this.bullet[0];
            var newBullet = cc.instantiate(this.skillBoss);
            newBullet.parent = cc.Canvas.instance.node;
            newBullet.x = this.node.x + + (Math.random() * 2 - 1) * 45;
            newBullet.y = this.node.y + yTemp;
            newBullet.zIndex = 2;
            this.scheduleOnce(() => {
                this.skillBoss = this.bullet[0];
                var newBullet = cc.instantiate(this.skillBoss);
                newBullet.parent = cc.Canvas.instance.node;
                newBullet.zIndex = 2;
                newBullet.x = this.node.x + (Math.random() * 2 - 1) * 45;
                newBullet.y = this.node.y + yTemp;
                this.scheduleOnce(() => {
                    this.skillBoss = this.bullet[0];
                    var newBullet = cc.instantiate(this.skillBoss);
                    newBullet.parent = cc.Canvas.instance.node;
                    newBullet.zIndex = 2;
                    newBullet.x = this.node.x + (Math.random() * 2 - 1) * 45;
                    newBullet.y = this.node.y + yTemp;
                    this.scheduleOnce(() => {
                        this.skillBoss = this.bullet[0];
                        var newBullet = cc.instantiate(this.skillBoss);
                        newBullet.parent = cc.Canvas.instance.node;
                        newBullet.zIndex = 2;
                        newBullet.x = this.node.x + (Math.random() * 2 - 1) * 45;
                        newBullet.y = this.node.y + yTemp;
                    }, 0.6)
                }, 0.4)
            }, 0.5)
        })
    }
    public bulletBoss1(xTemp: number, yTemp: number) {
        // this.skill1();
        this.skill3();
        this.iBullet++;
        if (this.iBullet == this.bullet.length) {
            this.iBullet = 0;
        }

        if (this.iBullet == 0) {
            this.scheduleOnce(() => {
                this.skillBoss = this.bullet[this.iBullet];
                var newBullet = cc.instantiate(this.skillBoss);
                newBullet.parent = cc.Canvas.instance.node;
                newBullet.x = this.node.x + xTemp;
                newBullet.y = this.node.y - yTemp;
                newBullet.zIndex = 10;
                this.scheduleOnce(() => {
                    this.skillBoss = this.bullet[this.iBullet];
                    var newBullet = cc.instantiate(this.skillBoss);
                    newBullet.parent = cc.Canvas.instance.node;
                    newBullet.zIndex = 10;
                    newBullet.x = this.node.x + xTemp;
                    newBullet.y = this.node.y - yTemp;
                    this.scheduleOnce(() => {
                        this.skillBoss = this.bullet[this.iBullet];
                        var newBullet = cc.instantiate(this.skillBoss);
                        newBullet.parent = cc.Canvas.instance.node;
                        newBullet.zIndex = 10;
                        newBullet.x = this.node.x + xTemp;
                        newBullet.y = this.node.y - yTemp;
                        this.scheduleOnce(() => {
                            this.skillBoss = this.bullet[this.iBullet];
                            var newBullet = cc.instantiate(this.skillBoss);
                            newBullet.parent = cc.Canvas.instance.node;
                            newBullet.zIndex = 10;
                            newBullet.x = this.node.x + xTemp;
                            newBullet.y = this.node.y - yTemp;
                        }, 0.35)
                    }, 0.35)
                }, 0.38)

                this.skillBoss = this.bullet[this.iBullet];
                var newBullet = cc.instantiate(this.skillBoss);
                newBullet.parent = cc.Canvas.instance.node;
                newBullet.zIndex = 10;
                newBullet.x = this.node.x - xTemp;
                newBullet.y = this.node.y - yTemp;
                this.scheduleOnce(() => {
                    this.skillBoss = this.bullet[this.iBullet];
                    var newBullet = cc.instantiate(this.skillBoss);
                    newBullet.parent = cc.Canvas.instance.node;
                    newBullet.zIndex = 10;
                    newBullet.x = this.node.x - xTemp;
                    newBullet.y = this.node.y - yTemp;
                    this.scheduleOnce(() => {
                        this.skillBoss = this.bullet[this.iBullet];
                        var newBullet = cc.instantiate(this.skillBoss);
                        newBullet.parent = cc.Canvas.instance.node;
                        newBullet.zIndex = 10;
                        newBullet.x = this.node.x - xTemp;
                        newBullet.y = this.node.y - yTemp;
                    }, 0.38)
                }, 0.38)
            }, 0.2)
        }
        else {
            this.scheduleOnce(() => {
                this.skillBoss = this.bullet[this.iBullet];
                var newBullet = cc.instantiate(this.skillBoss);
                newBullet.parent = cc.Canvas.instance.node;
                newBullet.x = this.node.x;
                newBullet.y = this.node.y;

                this.scheduleOnce(() => {
                    this.skillBoss = this.bullet[this.iBullet];
                    var newBullet = cc.instantiate(this.skillBoss);
                    newBullet.parent = cc.Canvas.instance.node;
                    newBullet.x = this.node.x;
                    newBullet.y = this.node.y;
                }, 0.55)
            }, 0.65)

        }
    }
    public bulletBoss2() {
        this.iBullet++;
        if (this.iBullet == this.bullet.length) {
            this.iBullet = 0;
            this.temp = 0;
        }
        this.skill2();
        if (this.iBullet == 0) {
            this.skillBoss = this.bullet[this.iBullet];
            var newBullet = cc.instantiate(this.skillBoss);
            newBullet.parent = cc.Canvas.instance.node;
            newBullet.x = this.node.x + (Math.random() * 2 - 1) * 85;
            newBullet.y = this.node.y + (Math.random() * 2 - 1) * 20;
            this.scheduleOnce(() => {
                var newBullet = cc.instantiate(this.skillBoss);
                newBullet.parent = cc.Canvas.instance.node;
                newBullet.x = this.node.x + (Math.random() * 2 - 1) * 65;
                newBullet.y = this.node.y + (Math.random() * 2 - 1) * 15;
                this.scheduleOnce(() => {
                    var newBullet = cc.instantiate(this.skillBoss);
                    newBullet.parent = cc.Canvas.instance.node;
                    newBullet.x = this.node.x + (Math.random() * 2 - 1) * 70;
                    newBullet.y = this.node.y + (Math.random() * 2 - 1) * 20;
                    this.scheduleOnce(() => {
                        var newBullet = cc.instantiate(this.skillBoss);
                        newBullet.parent = cc.Canvas.instance.node;
                        newBullet.x = this.node.x + (Math.random() * 2 - 1) * 60;
                        newBullet.y = this.node.y + (Math.random() * 2 - 1) * 30;
                        this.scheduleOnce(() => {
                            var newBullet = cc.instantiate(this.skillBoss);
                            newBullet.parent = cc.Canvas.instance.node;
                            newBullet.x = this.node.x + (Math.random() * 2 - 1) * 75;
                            newBullet.y = this.node.y + (Math.random() * 2 - 1) * 20;
                            this.scheduleOnce(() => {
                                var newBullet = cc.instantiate(this.skillBoss);
                                newBullet.parent = cc.Canvas.instance.node;
                                newBullet.x = this.node.x + (Math.random() * 2 - 1) * 40;
                                newBullet.y = this.node.y + (Math.random() * 2 - 1) * 25;
                                this.scheduleOnce(() => {
                                    var newBullet = cc.instantiate(this.skillBoss);
                                    newBullet.parent = cc.Canvas.instance.node;
                                    newBullet.x = this.node.x + (Math.random() * 2 - 1) * 90;
                                    newBullet.y = this.node.y + (Math.random() * 2 - 1) * 20;
                                    this.scheduleOnce(() => {
                                        var newBullet = cc.instantiate(this.skillBoss);
                                        newBullet.parent = cc.Canvas.instance.node;
                                        newBullet.x = this.node.x + (Math.random() * 2 - 1) * 85;
                                        newBullet.y = this.node.y + (Math.random() * 2 - 1) * 20;
                                    }, 0.3)
                                }, 0.3)
                            }, 0.3)
                        }, 0.3)
                    }, 0.3)
                }, 0.3)
            }, 0.1)

        }
        else {
            this.skillBoss = this.bullet[this.iBullet];
            var newBullet = cc.instantiate(this.skillBoss);
            newBullet.parent = cc.Canvas.instance.node;
            newBullet.x = this.node.x;
            newBullet.y = this.node.y;
            this.scheduleOnce(() => {
                this.skillBoss = this.bullet[this.iBullet];
                var newBullet = cc.instantiate(this.skillBoss);
                newBullet.parent = cc.Canvas.instance.node;
                newBullet.x = this.node.x;
                newBullet.y = this.node.y;
                this.scheduleOnce(() => {
                    this.skillBoss = this.bullet[this.iBullet];
                    var newBullet = cc.instantiate(this.skillBoss);
                    newBullet.parent = cc.Canvas.instance.node;
                    newBullet.x = this.node.x;
                    newBullet.y = this.node.y;
                    this.scheduleOnce(() => {
                        this.skillBoss = this.bullet[this.iBullet];
                        var newBullet = cc.instantiate(this.skillBoss);
                        newBullet.parent = cc.Canvas.instance.node;
                        newBullet.x = this.node.x;
                        newBullet.y = this.node.y;
                        this.scheduleOnce(() => {
                            this.skillBoss = this.bullet[this.iBullet];
                            var newBullet = cc.instantiate(this.skillBoss);
                            newBullet.parent = cc.Canvas.instance.node;
                            newBullet.x = this.node.x;
                            newBullet.y = this.node.y;
                            this.scheduleOnce(() => {
                                this.skillBoss = this.bullet[this.iBullet];
                                var newBullet = cc.instantiate(this.skillBoss);
                                newBullet.parent = cc.Canvas.instance.node;
                                newBullet.x = this.node.x;
                                newBullet.y = this.node.y;
                            }, 0.2)
                        }, 0.2)
                    }, 0.2)
                }, 0.2)
            }, 0.2)
            //

        }
    }
    public bulletBoss3() {
        this.iBullet++;
        if (this.iBullet == this.bullet.length) {
            this.iBullet = 0;
        }
        // this.skill3();
        if (this.iBullet == 0) {
            this.skillBoss = this.bullet[this.iBullet];
            var newBullet = cc.instantiate(this.skillBoss);
            newBullet.parent = cc.Canvas.instance.node;
            newBullet.x = this.node.x;
            newBullet.y = this.node.y - 30;
            newBullet.zIndex = 0;
            this.scheduleOnce(() => {
                this.skillBoss = this.bullet[this.iBullet];
                var newBullet = cc.instantiate(this.skillBoss);
                newBullet.parent = cc.Canvas.instance.node;
                newBullet.x = this.node.x;
                newBullet.y = this.node.y - 30;
                newBullet.zIndex = 0;
            }, 0.7)
        }
        else {
            this.scheduleOnce(() => {
                this.skillBoss = this.bullet[this.iBullet];
                var newBullet = cc.instantiate(this.skillBoss);
                newBullet.parent = cc.Canvas.instance.node;
                newBullet.x = this.node.x;
                newBullet.y = this.node.y - 30;
                newBullet.zIndex = 0;
                this.scheduleOnce(() => {
                    this.skillBoss = this.bullet[this.iBullet];
                    var newBullet = cc.instantiate(this.skillBoss);
                    newBullet.parent = cc.Canvas.instance.node;
                    newBullet.x = this.node.x;
                    newBullet.y = this.node.y - 30;
                    newBullet.zIndex = 0;
                }, 0.4)
            }, 0.2)

        }
    }
    public bulletBoss4() {
        this.iBullet++;
        if (this.iBullet == this.bullet.length) {
            this.iBullet = 0;
        }
        this.skill4();
        if (this.iBullet == 0) {
            this.skillBoss = this.bullet[this.iBullet];
            var newBullet = cc.instantiate(this.skillBoss);
            newBullet.parent = cc.Canvas.instance.node;
            newBullet.zIndex = 2;
            newBullet.x = this.node.x + (Math.random() * 2 - 1) * 55;
            newBullet.y = this.node.y + (Math.random() * 2 - 1) * 25;
            this.scheduleOnce(() => {
                var newBullet = cc.instantiate(this.skillBoss);
                newBullet.parent = cc.Canvas.instance.node;
                newBullet.x = this.node.x + (Math.random() * 2 - 1) * 55;
                newBullet.y = this.node.y + (Math.random() * 2 - 1) * 25;
                this.scheduleOnce(() => {
                    var newBullet = cc.instantiate(this.skillBoss);
                    newBullet.parent = cc.Canvas.instance.node;
                    newBullet.x = this.node.x + (Math.random() * 2 - 1) * 55;
                    newBullet.y = this.node.y + (Math.random() * 2 - 1) * 25;
                    this.scheduleOnce(() => {
                        var newBullet = cc.instantiate(this.skillBoss);
                        newBullet.parent = cc.Canvas.instance.node;
                        newBullet.x = this.node.x + (Math.random() * 2 - 1) * 55;
                        newBullet.y = this.node.y + (Math.random() * 2 - 1) * 25;
                        this.scheduleOnce(() => {
                            var newBullet = cc.instantiate(this.skillBoss);
                            newBullet.parent = cc.Canvas.instance.node;
                            newBullet.x = this.node.x + (Math.random() * 2 - 1) * 55;
                            newBullet.y = this.node.y + (Math.random() * 2 - 1) * 25;
                        }, 0.3)
                    }, 0.3)
                }, 0.3)
            }, 0.3)
        }
        else {
            this.scheduleOnce(() => {
                this.skillBoss = this.bullet[this.iBullet];
                var newBullet = cc.instantiate(this.skillBoss);
                newBullet.parent = cc.Canvas.instance.node;
                newBullet.x = this.node.x;
                newBullet.y = this.node.y;
                this.scheduleOnce(() => {
                    this.skillBoss = this.bullet[this.iBullet];
                    var newBullet = cc.instantiate(this.skillBoss);
                    newBullet.parent = cc.Canvas.instance.node;
                    newBullet.x = this.node.x;
                    newBullet.y = this.node.y;
                }, 0.5)
            }, 0.3)
        }
    }

    spawnE1(skillBoss: cc.Prefab, xTemp: number, yTemp: number, zIndex: number) {
        var newBullet = cc.instantiate(skillBoss);
        newBullet.parent = cc.Canvas.instance.node;
        newBullet.zIndex = zIndex;
        newBullet.x = this.node.x + xTemp;
        newBullet.y = this.node.y + yTemp;
    }

    public bulletBoss5() {
        this.iBullet++;
        if (this.iBullet == this.bullet.length) {
            this.iBullet = 0;
        }
        this.skill6();
        if (this.iBullet == 1) {
            this.skillBoss = this.bullet[this.iBullet];
            this.scheduleOnce(() => {
                this.spawnE1(this.skillBoss, 108, -50, 10);
                this.spawnE1(this.skillBoss, -108, -50, 10);
                this.scheduleOnce(() => {
                    this.spawnE1(this.skillBoss, 108, -50, 10);
                    this.spawnE1(this.skillBoss, -108, -50, 10);
                    this.scheduleOnce(() => {
                        this.spawnE1(this.skillBoss, 108, -50, 10);
                        this.spawnE1(this.skillBoss, -108, -50, 10);
                        this.scheduleOnce(() => {
                            this.spawnE1(this.skillBoss, 108, -50, 10);
                            this.spawnE1(this.skillBoss, -108, -50, 10);
                        }, 0.45)
                    }, 0.45)
                }, 0.45)
            }, 0.45)
        }
        else {
            this.scheduleOnce(() => {
                this.skillBoss = this.bullet[this.iBullet];
                this.spawnE1(this.skillBoss, 0, 0, 0);
                this.scheduleOnce(() => {
                    this.spawnE1(this.skillBoss, 0, 0, 0);
                    this.scheduleOnce(() => {
                        this.spawnE1(this.skillBoss, 0, 0, 0);
                        this.scheduleOnce(() => {
                            this.spawnE1(this.skillBoss, 0, 0, 0);
                            this.scheduleOnce(() => {
                                this.spawnE1(this.skillBoss, 0, 0, 0);
                            }, 0.3)
                        }, 0.35)
                    }, 0.4)
                }, 0.6)
            }, 0.3)
        }
    }
    hitBullet() {
        this.node.runAction(cc.sequence(cc.callFunc(() => {
            this.node.color = cc.Color.RED;
        }), cc.callFunc(() => {
            this.scheduleOnce(() => this.node.color = cc.Color.WHITE, 0.1);
        })));
    }

}