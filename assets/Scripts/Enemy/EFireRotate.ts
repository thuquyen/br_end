import G from "../Global";
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
    @property(cc.Prefab)
    bullet: cc.Prefab = null;

    @property()
    interval: number = 0;

    @property()
    delay: number = 0;

    @property()
    public timeFire: number = 0;

    @property(cc.Node)
    axis: cc.Node = null;

    start() {
        this.axis.zIndex = 10;
        this.node.zIndex = 10;
        this.scheduleOnce(() => {
            this.schedule(() => {
                if (G.allow_fire) {
                    this.spawn_bullet();

                }
            }, this.interval, cc.macro.REPEAT_FOREVER, this.delay)
        }, this.delay)
    }

    // update (dt) {}
    spawn_bullet() {
        var bullet = cc.instantiate(this.bullet);
        bullet.parent = this.node.parent;
        bullet.zIndex = 10;
        bullet.x = this.node.x
        bullet.y = this.node.y;
        bullet.angle = this.node.angle;
        bullet.runAction(cc.sequence((cc.moveTo(this.timeFire, cc.v2(this.axis.x, this.axis.y))), cc.callFunc(() => {
            bullet.destroy();
        })));
        // bullet.runAction(cc.sequence((cc.moveTo(this.timeFire, cc.v2(this.axis.x + this.axis.height / 3, this.axis.y + this.axis.height / 5))), cc.callFunc(() => {
        //     bullet.destroy();
        // })));
    }
}
