import ESkill from "./ESkill";
import { Helper } from "../Helper";
import G from "../Global";

const { ccclass, property } = cc._decorator;

@ccclass('ConfigSpawnBullet')
export class ConfigSpawnBullet {

    @property(Number)
    delay: number = 3.5;

    @property(Number)
    interval: number = 3;

    @property(Number)
    deltaInterval: number = 1;

    @property(Number)
    speed: number = 300;

    @property(Number)
    deltaSpeed: number = 100;

    @property(String)
    pattern: String = '9';

}

@ccclass
export default class ESkillSpawnBullet extends ESkill {

    @property(ConfigSpawnBullet)
    config: ConfigSpawnBullet = new ConfigSpawnBullet();

    private i = 0;

    // currentViewScreen: number = 0;

    // currentPosition: number = 0;

    // positionEnemyStart: cc.Vec2 = cc.v2(0,0);

    start() {
        // this.enabled = false;

        // this.positionEnemyStart.x = cc.view.getFrameSize().width;
        // this.positionEnemyStart.y = cc.view.getFrameSize().height;
        this.scheduleOnce(() => {
            this.scheduleTick();
            this.schedule(() => {
                this.scheduleTick();
            }, this.config.interval, cc.macro.REPEAT_FOREVER);
        }, this.config.delay);

        // if (cc.view.getFrameSize().width > cc.view.getFrameSize().height) {
        //     this.scheduleOnce(() => {
        //         this.currentPosition = this.node.x / this.positionEnemyStart.x * 960 / 640 * this.positionEnemyStart.y;
        //         this.enabled = true;
        //     }, 1.8);
        // } else if (cc.view.getFrameSize().width < cc.view.getFrameSize().height) {
        //     this.scheduleOnce(() => {
        //         this.currentPosition = this.node.x / this.positionEnemyStart.x * 960 / 640 * this.positionEnemyStart.y;
        //         this.enabled = true;
        //     }, 1);
        // }

        // this.currentViewScreen = cc.view.getFrameSize().width;
    }

    // eventRotateScreen() {
    //     if (cc.view.getFrameSize().width != this.currentViewScreen) {
    //         this.node.x = this.currentPosition * cc.view.getFrameSize().width / cc.view.getFrameSize().height * 640 / 960;
    //         this.currentViewScreen = cc.view.getFrameSize().width;
    //         this.currentPosition = this.node.x / cc.view.getFrameSize().width * 960 / 640 * cc.view.getFrameSize().height;
    //     }
    // }

    // update() {
    //     this.eventRotateScreen();
    // }

    private scheduleTick() {
        var time = Math.random() * this.config.deltaInterval;
        this.scheduleOnce(() => {
            this.checkPattern();
            this.i++;
        }, time);
    }

    private checkPattern() {
        var k = this.i % this.config.pattern.length;
        var p = parseInt(this.config.pattern[k]) / 9;
        if (Math.random() <= p) {
            this.dropBullet();
        }
    }

    private dropBullet() {
        if (G.e == false) {
        // cc.audioEngine.playEffect(G.enemyShoot, false);
        var speed = this.config.speed + Math.random() * this.config.deltaSpeed;
        var eBullet = Helper.poolManager.eBullet.createObject(this.node.parent);
        eBullet.x = this.node.x;
        eBullet.y = this.node.y - 25;
        if (this.node.name == 't') {
            var move = cc.moveBy(1500 / speed, 0, 1500);
        }
        else {
            var move = cc.moveBy(1500 / speed, 0, -1500);
        }

        eBullet.runAction(cc.sequence(
            move,
            cc.callFunc(() => {
                Helper.poolManager.eBullet.killObject(eBullet);
            })
        ));
        this.scheduleOnce(() => {
            eBullet.zIndex = 2;
        }, 100);
        } else {
            return;
        }
    }



}