import BasePool from "../common/BasePool";
import { Helper } from "../Helper";
import IEnemy from "./IEnemy";
import G from "../Global";

const { ccclass, property } = cc._decorator;

export enum EnemyEvent {
    INIT,
    APPEARED,
    HIT_BULLET,
    DIE
};

@ccclass
export default class BaseEnemy extends IEnemy {

    _isDie: boolean = false;
    // @property(cc.Prefab)
    // protectedExplosion: cc.Prefab = null;
    // @property(BasePool)
    // explosionPool: BasePool = null;
    @property(cc.Prefab)
    explosionPool: cc.Prefab = null;

    @property(BasePool)
    hitExplosionPool: BasePool = null;

    centrePos: cc.Vec2 = null;

    @property(cc.Vec2)
    adjustExplosion: cc.Vec2 = null;

    @property(Number)
    scaleExplosion: number = 1.1;

    @property(Number)
    speedExplosion: number = 2;

    @property(Number)
    public hp: number = 1;

    @property(Number)
    dmg: number = 0;

    setOnDie: Function = null;

    @property(cc.Prefab)
    coinPrefab: cc.Prefab = null;
    @property(cc.Prefab)
    private enemy: cc.Prefab[] = [];
    @property()
    private time: number = 2;

    private e: cc.Node[] = [];
    @property()
    timeSpawnEChild: number = 0.5
    posX: number = 0;
    posY: number = 0;

    checkTurn1: boolean = false;
    checkTurn2: boolean = false;

    start() {
        if (this.node.name == "enemy_26") {
            this.schedule(() => {
                this.node.runAction(cc.sequence(
                    cc.moveBy(2.5, cc.v2(0, -300)).easing(cc.easeSineOut()),
                    cc.moveBy(2.5, cc.v2(0, 300)).easing(cc.easeSineOut()),
                ))
            }, 5.1, cc.macro.REPEAT_FOREVER, 3)
        }

        if (this.node.name == "eSwinging") {
            this.schedule(() => {
                this.node.runAction(cc.sequence(
                    cc.moveBy(0.8, 30, 0).easing(cc.easeSineOut()),
                    cc.moveBy(0.8, -30, 0).easing(cc.easeSineIn()),
                    cc.moveBy(0.8, -30, 0).easing(cc.easeSineOut()),
                    cc.moveBy(0.8, 30, 0).easing(cc.easeSineIn())
                ))
            }, 3.2, cc.macro.REPEAT_FOREVER, 1)
        }

        var centrePos = this.node.getChildByName('centrePos');
        if (centrePos != null) {
            this.centrePos = centrePos.getPosition();
        } else {
            if (this.adjustExplosion != null) {
                this.centrePos = this.adjustExplosion;
            } else {
                this.centrePos = new cc.Vec2(0, this.node.height / 2);
            }
        }
        // if (this.explosionPool == null)
        //     this.explosionPool = Helper.poolManager.eExplosion;
        this.node.emit(EnemyEvent.INIT.toString());
    }

    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        if (other.node.group == 'shipBullet') {
            if (self.node.group == "miniboss") {
                other.node.destroy();
            }
            else {
                this.onHitShipBullet(other.node);
            }
            this.spawnExplosion(this.node.x , this.node.y );
        }
        if (self.node.group == 'miniboss') {
            if (other.node.name == 'rocket') {
                this.onHitShipBullet(other.node);
            }
        }
    }

    runExplosionEffect(parent: cc.Node, x: number, y: number, scale: number, speed: number) {
        if (this.explosionPool == null)
            return;
        // var explosion = this.explosionPool.createObject(parent);
        var explosion = cc.instantiate(this.explosionPool);
        explosion.x = x;
        explosion.y = y;
        explosion.scale = scale;
        var clip = explosion.getComponent(cc.Animation).defaultClip;
        clip.speed = speed;
        var t = clip.duration / clip.speed;
        explosion.getComponent(cc.Animation).play();
        explosion.getComponent(cc.Animation).scheduleOnce(function () {
            // this.killObject(explosion);
            explosion.destroy();
        }.bind(this.explosionPool), t);
    }

    onHitShipBullet(node: cc.Node) {
        node.destroy();
        this.node.emit(EnemyEvent.HIT_BULLET.toString());
        this.onLoseHp();
    }

    runHitBulletEffect() {
        this.node.runAction(cc.sequence(
            cc.moveBy(0.15, 0, 20),
            cc.moveBy(0.15, 0, -20)
        ));
    }

    onLoseHp() {
        this.hp--;
        // spawn coin for enemy in groupE
        // console.log(this.node.x + "and" + this.node.parent.y)
        if (this.hp == 0) {
            this.posX = this.node.x;
            this.posY = this.node.y;

            //born turn 1
            if (this.node.name == 'eGold_coin' && !this.checkTurn1) {
                this.checkTurn1 = true;
                this.startSpawnTurn1();
            }

            //born turn 2
            if (this.node.name == 'eGold_1_coin' && !this.checkTurn2) {
                this.checkTurn2 = true;
                this.startSpawnTurn2();
            }

            var ran = Math.round(Math.random())
            if (ran == 0) {
                this.spawnCoin(3);
                G.score += 3;
            }
            else {
                this.spawnCoin(4);
                G.score += 4;
            }
            this._onEnemyDie();
        }
    }

    private _onEnemyDie() {
        this.node.destroy();
    }

    onDestroy() {
        // cc.audioEngine.playEffect(G.hitBullet, false);
        G.numberBossDie++;
        G.totalE++;
        G.eDie--;

        this.node.emit(EnemyEvent.DIE.toString());
        this._isDie = true;
        var pos = this.node.parent.convertToNodeSpaceAR(this.node.convertToWorldSpaceAR(this.centrePos));
        this.runExplosionEffect(this.node.parent, pos.x, pos.y + 15, this.scaleExplosion, this.speedExplosion);
        if (this.onDie)
            this.onDie(this);
    }

    isDie() {
        return this._isDie;
    }

    public spawnExplosion(x: number, y: number) {
        var newExplosion = this.hitExplosionPool.createObject(cc.Canvas.instance.node);
        newExplosion.zIndex = 1;
        // newExplosion.scale = 1.3;
        newExplosion.x = x;
        newExplosion.y = y;
    }
    // spawnCoin(k: number) {
    //     for (let i = 0; i < k; i++) {
    //         var coinPrefab = cc.instantiate(this.coinPrefab);
    //         coinPrefab.parent = cc.Canvas.instance.node;
    //         coinPrefab.x = this.node.x;
    //         coinPrefab.y = this.node.y;
    //     }
    // }
    spawnCoin(k: number) {
        for (let i = 0; i < k; i++) {
            var coinPrefab = cc.instantiate(this.coinPrefab);
            coinPrefab.parent = cc.Canvas.instance.node;

            if (this.node.name == 'z1_coin' || this.node.name == 'z33_coin' || this.node.name == 'tempMini_1' || this.node.name == 'tempMini_2') {
                coinPrefab.x = this.node.parent.x + 20 * (Math.random() * 2 - 1);
                coinPrefab.y = this.node.parent.y + 20 * (Math.random() * 2 - 1);
            }
            else if (this.node.name == 'tempE_group1' || this.node.name == 'tempE_group2' || this.node.name == 'e1_80') {
                coinPrefab.x = this.node.x;
                coinPrefab.y = this.node.parent.y;
            }
            else {
                coinPrefab.x = this.node.x + 20 * (Math.random() * 2 - 1);
                coinPrefab.y = this.node.y + 20 * (Math.random() * 2 - 1);
            }

            if (this.node.name == "eGold_coin" || this.node.name == "tempE2_2" || this.node.name == "tempE3_2" || this.node.name == "tempE4" || this.node.name == "tempE5" || this.node.name == 'tempE6' || this.node.name == 'tempE7'
                || this.node.name == 'tempMini_1' || this.node.name == 'tempMini_2' || this.node.name=="BodyDragon" || this.node.name=="HeadDragon") {
                coinPrefab.runAction(cc.sequence(
                    cc.moveTo(7, cc.v2(this.node.parent.parent.x, this.node.parent.parent.y * 3)),
                    cc.callFunc(() => {
                        coinPrefab.destroy();
                    })))
            }
            else if (this.node.name == "tempE_group2" || this.node.name == "tempE_group1" || this.node.name == "e1_80") {
                coinPrefab.runAction(cc.sequence(
                    cc.moveTo(2, cc.v2(this.node.x, 600)),
                    cc.callFunc(() => {
                        coinPrefab.destroy();
                    })))
            }
            else {
                coinPrefab.runAction(cc.sequence(
                    cc.moveTo(3, cc.v2(this.node.parent.x, this.node.parent.y * 2)),
                    cc.callFunc(() => {
                        coinPrefab.destroy();
                    })))
            }
        }
    }
    public startSpawnTurn1() {
        this.e[0] = this.spawnEnemy(0);
        this.e[1] = this.spawnEnemy(1);

        this.e[0].runAction(cc.moveTo(0.5, cc.v2(this.posX + 45, this.posY + Math.round(Math.random() * 2 - 1) * 40)).easing(cc.easeSineInOut()));
        this.e[1].runAction(cc.moveTo(0.5, cc.v2(this.posX - 45, this.posY + Math.round(Math.random() * 2 - 1) * 45)).easing(cc.easeSineInOut()));

    }
    public startSpawnTurn2() {
        this.e[0] = this.spawnEnemy(0);
        this.e[1] = this.spawnEnemy(1);
        this.e[2] = this.spawnEnemy(2);
        this.e[3] = this.spawnEnemy(3);

        this.e[0].runAction(cc.moveTo(0.5, cc.v2(this.posX + Math.random() * 60, this.posY - Math.random() * 90)).easing(cc.easeSineInOut()));
        this.e[1].runAction(cc.moveTo(0.5, cc.v2(this.posX - Math.random() * 65, this.posY - Math.random() * 105)).easing(cc.easeSineInOut()));
        this.e[2].runAction(cc.moveTo(0.5, cc.v2(this.posX + Math.random() * 55, this.posY + Math.random() * 95)).easing(cc.easeSineInOut()));
        this.e[3].runAction(cc.moveTo(0.5, cc.v2(this.posX - Math.random() * 50, this.posY + Math.random() * 80)).easing(cc.easeSineInOut()));
    }

    private spawnEnemy(n: number) {
        var newEnemy = cc.instantiate(this.enemy[n]);
        newEnemy.parent = cc.Canvas.instance.node;
        newEnemy.x = this.node.x;
        newEnemy.y = this.node.y;
        return newEnemy;
    }
    public moveChildE(eChild: cc.Node) {
        this.schedule(() => {
            let y = (Math.random() * 2 - 1) * 20;
            eChild.runAction(cc.moveTo(1, cc.v2(eChild.x, y)))
        }, 1, cc.macro.REPEAT_FOREVER, 0.5)
    }

}