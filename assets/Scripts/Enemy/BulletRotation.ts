

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {


    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        if (other.node.group == 'axis') {
            self.node.destroy();
        }
    }

}

