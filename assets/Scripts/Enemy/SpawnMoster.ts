
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Prefab)
    monster: cc.Prefab = null;
    @property()
    timeMove: number = 0;
    @property()
    interval: number = 0;
    @property()
    delay: number = 0;
    count: number = 0;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}
    start() {
        this.schedule(() => {
            if (this.count < 10) {
                var ran = Math.round(Math.random())*4;
                console.log(ran)
                if (ran == 0) {
                    this.monsterMove(4, 4);
                } else if (ran == 1) {
                    this.monsterMove(3, 3);
                }
                else {
                    this.monsterMove(5, 5);
                }
                this.count++;
            }
        }, this.interval, cc.macro.REPEAT_FOREVER, this.delay)

    }
    monsterMove(w: number, h: number) {
        // console.log(-this.node.parent.width / 3 + "   he igh" + this.node.parent.height / 3)
        var monster = cc.instantiate(this.monster);
        monster.parent = cc.Canvas.instance.node;
        monster.x = -this.node.parent.width / 3;
        monster.y = 550;
        monster.runAction(cc.sequence(cc.moveTo(this.timeMove, cc.v2(-this.node.parent.width / w, this.node.parent.height / h,)),
            cc.callFunc(() => {
                monster.getComponent(sp.Skeleton).setAnimation(1, 'attack', true);
                this.scheduleOnce(() => {
                    monster.getComponent(sp.Skeleton).setAnimation(1, 'move', true);
                }, 0.7)
                this.scheduleOnce(() => {
                    monster.runAction(cc.sequence(cc.moveTo(this.timeMove + 2, cc.v2(this.node.parent.width / w - 1, -500)),
                        cc.callFunc(() => {
                            if (monster.activeInHierarchy) {
                                monster.destroy();
                            }
                        }))
                    )
                }, 0.8)
            }))
        )
    }

    // update (dt) {}
}
