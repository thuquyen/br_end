const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    TurnPoint : cc.Node[] = [];
    min: number = 0;

    Width: number = 0;

    start() {
        for (let i = 0; i < this.TurnPoint.length; i++) {
            this.getPositionEnemy(this.TurnPoint[i]);
        }

    }
    getPositionEnemy(e: cc.Node) {
        //if pos X canvas > pos Y canvas
        if (this.node.parent.width > this.node.parent.height) {
            this.min = this.node.parent.height;
            this.Width = this.node.parent.width / 960  * 640;
        } else {
            this.min = this.node.parent.width;
            this.Width = 640;
        }
        for (let i = 0; i < e.childrenCount; i++) {
            let x = e.children[i].x / this.Width * this.min;
            let y = e.children[i].y;
            e.children[i].setPosition(cc.v2(x, y));
        }
    }
}
