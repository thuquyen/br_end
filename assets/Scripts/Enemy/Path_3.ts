import Global from "../Global";
import Jump from "./JumpToPlayer";
const { ccclass, property } = cc._decorator;

@ccclass
export default class path extends cc.Component {

    @property()
    speedPath: number = 0;

    @property()
    loop: boolean = true;

    @property(cc.Prefab)
    dragon: cc.Prefab = null;

    @property()
    timeDelay: number = 0;

    @property()
    timeEnemy: number = 0;

    @property()
    numberDragons: number = 0;

    dragons: cc.Node[] = [];

    comVec: cc.Vec2[] = [];

    array: cc.Vec2[] = [];

    public onComlete: Function = null;

    checkComplete: boolean = true;
    posX: number = 0;
    posY: number = 0;
    // @property(cc.Node)
    // rocket: cc.Node = null;

    @property()
    card: boolean = false;

    count: number = 0;
    @property(cc.Node)
    positioList: cc.Node[] = [];
    @property()
    timeMoveEnd: number = 0;
    onLoad() {
        for (let i = 0; i < this.node.childrenCount; i++) {
            this.array.push(cc.v2(this.node.children[i].x, this.node.children[i].y));
        }

        if (this.loop)
            this.array.push(cc.v2(this.node.children[0].x, this.node.children[0].y));

        this.schedule(() => {
            this.creatDragon();
        }, this.timeEnemy, this.numberDragons, this.timeDelay);
    }


    betweenDegree(comVec, dirVec) {
        let angleDeg = Math.atan2(dirVec.y - comVec.y, dirVec.x - comVec.x) * 180 / Math.PI;
        return angleDeg;
    }

    creatDragon() {
        let dragon = cc.instantiate(this.dragon);
        dragon.parent = this.node;
        dragon.x = this.node.children[0].x;
        dragon.y = this.node.children[0].y;
        this.dragons.push(dragon);
        // this.rocket.active = false;
        if (dragon.activeInHierarchy) {
            if (this.loop) {
                dragon.runAction(cc.cardinalSplineTo(this.speedPath, this.array, 0).repeatForever());
                //di chuyển nó theo list point  đã lưu trong array ở onLoad()

            } else {
                dragon.runAction(cc.sequence(cc.cardinalSplineTo(this.speedPath, this.array, 0),
                    cc.callFunc(() => {
                        dragon.runAction(cc.sequence(cc.moveTo(this.timeMoveEnd, cc.v2(this.positioList[this.count].x, this.positioList[this.count].y)),
                            cc.callFunc(() => {
                                // dragon.rotation = 179;
                                // console.log(dragon.angle + "...." + dragon.rotation)
                            })))
                        this.count++;
                    })

                ));
            }
            //vị trí đầu tiên của con rồng lúc sinh ra
            this.comVec.push(cc.v2(dragon.x, dragon.y));
        }
    }

}
