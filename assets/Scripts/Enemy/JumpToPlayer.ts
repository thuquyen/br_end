import ScheduleSkill from "./ScheduleSkill";
import G from "../Global";

const {ccclass, property, menu} = cc._decorator;

@ccclass
@menu('Skills/JumpToPlayer')
export default class JumpToPlayer extends ScheduleSkill {

    @property(Number)
    t: number = 2;

    @property(Number)
    deltaT: number = 0.3;

    @property(Number)
    deltaX: number = 0;

    @property(Number)
    deltaY: number = 0;

    @property(Number)
    adjustX: number = 0;

    @property(Number)
    adjustY: number = -100;

    private isAttack = false;

    attack(){
        if(!this.node || !this.node.isValid || this.isAttack)
            return;
        this.isAttack = true;
        let x0 = this.node.x;
        let y0 = this.node.y;

        let x1 = G.ship.node.x + this.adjustX + (Math.random()-0.5) * this.deltaX;
        let y1 = G.ship.node.y + this.adjustY + (Math.random()-0.5) * this.deltaY;

        if(y1<-cc.winSize.height/2) {
            y1 = -cc.winSize.height/2;
        }

        let t = this.t + this.deltaT * Math.random();

        this.node.runAction(cc.sequence(
            cc.jumpTo(t, x1, y1, 300, 1).easing(cc.easeSineOut()),
            cc.delayTime(0.1),
            cc.callFunc(()=>{
                this.node.stopAllActions();
                this.node.runAction(cc.sequence(
                    cc.moveTo(t/1.5, x0, y0),
                    cc.callFunc(()=>{
                        this.isAttack = false;
                    })    
                ))
            }),
        ));
    }

}