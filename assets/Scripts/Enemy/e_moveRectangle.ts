

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {


    @property()
    dau: number = 0;

    start() {
        this.schedule(() => {
            this.node.runAction(cc.sequence(
                cc.moveBy(1, cc.v2(0, -320)),
                cc.moveBy(1, cc.v2(-210 * this.dau, 0)),
                cc.moveBy(1, cc.v2(0, 320)),
                cc.moveBy(1, cc.v2(210 * this.dau, 0)),

            ))
        }, 5, cc.macro.REPEAT_FOREVER, 4)

    }

    // update (dt) {}
}
