import ScheduleSkill from "./ScheduleSkill";
import { Helper } from "../Helper";


const {ccclass, property, menu} = cc._decorator;

@ccclass
@menu('Skills/ScheduleDropBullet')
export default class ScheduleDropBullet extends ScheduleSkill {

    @property(Number)
    speed: number = 300;

    @property(Number)
    deltaSpeed: number = 100;

    attack(){
        var speed = this.speed + Math.random()*this.deltaSpeed;
        var eBullet = Helper.poolManager.eBullet.createObject(this.node.parent);
        eBullet.x = this.node.x;
        eBullet.y = this.node.y;
        eBullet.runAction(cc.sequence(
            cc.moveBy(1500/speed, 0, -1500),
            cc.callFunc(()=>{
                Helper.poolManager.eBullet.killObject(eBullet);
            })
        ));
        this.scheduleOnce(()=>{
            eBullet.zIndex = 2;
        }, 100);
    }

}