import { EnemyEvent } from "./BaseEnemy";
import G from "../Global";

const { ccclass, property } = cc._decorator;

@ccclass
export default abstract class ScheduleSkill extends cc.Component {

    @property({ type: cc.Enum(EnemyEvent) })
    startEvent: EnemyEvent = EnemyEvent.INIT;

    //@property({type: Number, visible: ()=>{return this.startEvent===EnemyEvent.INIT||this.startEvent==EnemyEvent.APPEARED;}})
    @property(Number)
    delay: number = 0;

    @property(Number)
    deltaDelay: number = 0;

    //@property({type: Number, visible: ()=>{return this.startEvent===EnemyEvent.INIT||this.startEvent==EnemyEvent.APPEARED;}})
    @property(Number)
    interval: number = 3;

    //@property({type: Number, visible: ()=>{return this.startEvent===EnemyEvent.INIT||this.startEvent==EnemyEvent.APPEARED;}})
    @property(Number)
    deltaInterval: number = 1;

    private i = 0;

    start() {
        var delay = this.delay + this.deltaDelay * Math.random();
        if (this.startEvent === EnemyEvent.INIT) {
            this.scheduleOnce(() => {
                this.scheduleTick();
                this.schedule(() => {
                    this.scheduleTick();
                }, this.interval, cc.macro.REPEAT_FOREVER);
            }, this.delay);
        } else if (this.startEvent === EnemyEvent.APPEARED) {
            this.node.on(this.startEvent.toString(), () => {
                this.scheduleOnce(() => {
                    this.scheduleTick();
                    this.schedule(() => {
                        this.scheduleTick();
                    }, this.interval, cc.macro.REPEAT_FOREVER);
                }, this.delay);
            });
        } else if (this.startEvent === EnemyEvent.HIT_BULLET) {
            this.node.on(this.startEvent.toString(), () => {
                if ((<any>this.node)._appear === false)
                    return;
                this.attack();
            });
        } else if (this.startEvent === EnemyEvent.DIE) {
            this.node.on(this.startEvent.toString(), () => {
                this.attack();
            });
        }
    }

    private scheduleTick() {
        var time = Math.random() * this.deltaInterval;
        this.scheduleOnce(() => {
            if (G.e == false) {
                this.i++;
                this.attack();
            } else {
                return;
            }

        }, time);
    }

    public abstract attack();

}