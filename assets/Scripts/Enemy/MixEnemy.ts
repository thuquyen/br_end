const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {


    @property([cc.Node])
    arrE: cc.Node[] = [];
    @property()
    timeActive: number = 0;
    @property(cc.Vec2)
    public SaveArr: cc.Vec2[] = [];
    @property()
    speed: number = 1;
    @property()
    deltaSpeed : number = 1;
    // onLoad () {}

    start() {
        for (let i = 0; i < this.arrE.length; i++) {
            this.SaveArr.push(this.arrE[i].position);
        }
        this.schedule(()=>{
            this.mix1();
        },16,cc.macro.REPEAT_FOREVER,this.timeActive);
        
    }
    mix1() {
        this.scheduleOnce(() => {
            this.scheduleOnce(() => {
                this.MixColum1();
                this.MixColum2();
            }, 1.5)
            this.MixColum3();
            this.MixColum4();
        }, 2);

        this.scheduleOnce(() => {
            this.MixRow1();
            this.MixRow2();
            this.MixColum3();
            this.MixColum4();
            this.scheduleOnce(()=>{
                this.MixRow3();
                this.MixRow4();
            },1.5);
        }, 6);

        this.scheduleOnce(()=>{
            this.mix2();
        },12)
    }
    mix2(){
        this.MixColum3();
        this.scheduleOnce(()=>{
            this.MixColum1();
            this.scheduleOnce(()=>{
                this.MixColum2();
                this.scheduleOnce(()=>{
                    this.MixColum4();
                },1)
            },1);
        },1);
    }


    public Mix(timeSpeed:number,enemy: cc.Node, posX1: number, posY1: number, posX2: number, posY2) {
        enemy.runAction(cc.sequence(
            cc.moveTo(timeSpeed, posX1, posY1),
            cc.moveTo(timeSpeed, posX2, posY2),

        ));
    }
    public MixColum1() {
        this.Mix(this.speed,this.arrE[3], this.SaveArr[0].x, this.SaveArr[0].y, this.SaveArr[3].x, this.SaveArr[3].y);
        this.Mix(this.speed,this.arrE[2], this.SaveArr[1].x, this.SaveArr[1].y, this.SaveArr[2].x, this.SaveArr[2].y);
        this.Mix(this.speed,this.arrE[1], this.SaveArr[2].x, this.SaveArr[2].y, this.SaveArr[1].x, this.SaveArr[1].y);
        this.Mix(this.speed,this.arrE[0], this.SaveArr[3].x, this.SaveArr[3].y, this.SaveArr[0].x, this.SaveArr[0].y);
    }
    public MixColum2() {
        this.Mix(this.speed,this.arrE[4], this.SaveArr[7].x, this.SaveArr[7].y, this.SaveArr[4].x, this.SaveArr[4].y);
        this.Mix(this.speed,this.arrE[5], this.SaveArr[6].x, this.SaveArr[6].y, this.SaveArr[5].x, this.SaveArr[5].y);
        this.Mix(this.speed,this.arrE[6], this.SaveArr[5].x, this.SaveArr[5].y, this.SaveArr[6].x, this.SaveArr[6].y);
        this.Mix(this.speed,this.arrE[7], this.SaveArr[4].x, this.SaveArr[4].y, this.SaveArr[7].x, this.SaveArr[7].y);
    }
    public MixColum3() {
        this.Mix(this.speed,this.arrE[8], this.SaveArr[11].x, this.SaveArr[11].y, this.SaveArr[8].x, this.SaveArr[8].y);
        this.Mix(this.speed,this.arrE[9], this.SaveArr[10].x, this.SaveArr[10].y, this.SaveArr[9].x, this.SaveArr[9].y);
        this.Mix(this.speed,this.arrE[10], this.SaveArr[9].x, this.SaveArr[9].y, this.SaveArr[10].x, this.SaveArr[10].y);
        // this.Mix(this.speed,this.arrE[11], this.SaveArr[8].x, this.SaveArr[8].y, this.SaveArr[11].x, this.SaveArr[11].y);
    }
    public MixColum4() {
        // this.Mix(this.speed,this.arrE[12], this.SaveArr[15].x, this.SaveArr[15].y, this.SaveArr[12].x, this.SaveArr[12].y);
        this.Mix(this.speed,this.arrE[13], this.SaveArr[14].x, this.SaveArr[14].y, this.SaveArr[13].x, this.SaveArr[13].y);
        this.Mix(this.speed,this.arrE[14], this.SaveArr[13].x, this.SaveArr[13].y, this.SaveArr[14].x, this.SaveArr[14].y);
        // this.Mix(this.speed,this.arrE[15], this.SaveArr[12].x, this.SaveArr[12].y, this.SaveArr[15].x, this.SaveArr[15].y);
    }
    public MixRow1() {
        this.Mix(this.speed,this.arrE[0], this.SaveArr[4].x, this.SaveArr[4].y, this.SaveArr[0].x, this.SaveArr[0].y);
        this.Mix(this.speed,this.arrE[1], this.SaveArr[5].x, this.SaveArr[5].y, this.SaveArr[1].x, this.SaveArr[1].y);
        this.Mix(this.speed,this.arrE[2], this.SaveArr[6].x, this.SaveArr[6].y, this.SaveArr[2].x, this.SaveArr[2].y);
        this.Mix(this.speed,this.arrE[3], this.SaveArr[7].x, this.SaveArr[7].y, this.SaveArr[3].x, this.SaveArr[3].y);
    }
    public MixRow2() {
        this.Mix(this.speed,this.arrE[4], this.SaveArr[0].x, this.SaveArr[0].y, this.SaveArr[4].x, this.SaveArr[4].y);
        this.Mix(this.speed,this.arrE[5], this.SaveArr[1].x, this.SaveArr[1].y, this.SaveArr[5].x, this.SaveArr[5].y);
        this.Mix(this.speed,this.arrE[6], this.SaveArr[2].x, this.SaveArr[2].y, this.SaveArr[6].x, this.SaveArr[6].y);
        this.Mix(this.speed,this.arrE[7], this.SaveArr[3].x, this.SaveArr[3].y, this.SaveArr[7].x, this.SaveArr[7].y);
    }
    public MixRow3() {
        // this.Mix(this.speed+this.deltaSpeed,this.arrE[8], this.SaveArr[12].x, this.SaveArr[12].y, this.SaveArr[8].x, this.SaveArr[8].y);
        this.Mix(this.speed+this.deltaSpeed,this.arrE[9], this.SaveArr[13].x, this.SaveArr[13].y, this.SaveArr[9].x, this.SaveArr[9].y);
        this.Mix(this.speed+this.deltaSpeed,this.arrE[10], this.SaveArr[14].x, this.SaveArr[14].y, this.SaveArr[10].x, this.SaveArr[10].y);
        // this.Mix(this.speed+this.deltaSpeed,this.arrE[11], this.SaveArr[15].x, this.SaveArr[15].y, this.SaveArr[11].x, this.SaveArr[11].y);
    }
    public MixRow4() {
        // this.Mix(this.speed+this.deltaSpeed,this.arrE[12], this.SaveArr[8].x, this.SaveArr[8].y, this.SaveArr[12].x, this.SaveArr[12].y);
        this.Mix(this.speed+this.deltaSpeed,this.arrE[13], this.SaveArr[9].x, this.SaveArr[9].y, this.SaveArr[13].x, this.SaveArr[13].y);
        this.Mix(this.speed+this.deltaSpeed,this.arrE[14], this.SaveArr[10].x, this.SaveArr[10].y, this.SaveArr[14].x, this.SaveArr[14].y);
        // this.Mix(this.speed+this.deltaSpeed,this.arrE[15], this.SaveArr[11].x, this.SaveArr[11].y, this.SaveArr[15].x, this.SaveArr[15].y);
    }
}


