import BasePool from "../common/BasePool";
import { Helper } from "../Helper";
import IEnemy from "./IEnemy";
import G from "../Global";
// import CoinContainScripts from "../CoinContainScripst";
const { ccclass, property } = cc._decorator;

export enum EnemyEvent {
    INIT,
    APPEARED,
    HIT_BULLET,
    DIE
};

@ccclass
export default class BaseEnemy extends IEnemy {

    _isDie: boolean = false;

    public score: number = 0;
    // @property(cc.Prefab)
    // explosionPrefab: cc.Prefab = null;

    @property(BasePool)
    explosionPool: BasePool = null;

    @property(BasePool)
    hitExplosionPool: BasePool = null;
    // @property(BasePool)
    // protectedExplosion: BasePool = null;

    centrePos: cc.Vec2 = null;

    @property(cc.Vec2)
    adjustExplosion: cc.Vec2 = null;

    @property(Number)
    scaleExplosion: number = 1.1;

    @property(Number)
    speedExplosion: number = 2;

    @property(Number)
    public hp: number = 1;

    @property(Number)
    dmg: number = 0;

    @property()
    check: number = 0;

    @property()
    checkExplosion = 0;

    @property(cc.Node)
    bar: cc.Node = null;

    hpTotal: number = 0;

    widthBar: number = 0;

    setOnDie: Function = null;
    // @property(cc.Node)
    // shield: cc.Node = null;
    @property()
    timeSpeedCrop: number = 1;
    @property()
    distance: number = 12;
    @property()
    disY: number = 7;

    @property()
    e_3circle: boolean = false;
    @property()
    e_triangle: boolean = false;
    @property()
    e_flower: boolean = false;
    @property()
    e_jump: boolean = false;
    @property()
    e_axis: boolean = false;
    @property()
    e_circle_exchange: boolean = false;

    @property(cc.Prefab)
    Coin: cc.Prefab = null;

    start() {
        //enemy move horizontal
        // if(this.node.name == "e_1_pixel"){
        //         this.schedule(()=>{
        //             this.node.runAction(cc.sequence(
        //                 cc.moveTo(1,cc.v2(cc.Canvas.instance.node.width / 2, this.node.y)),
        //                 cc.moveTo(1,cc.v2(0, this.node.y)),
        //                 cc.moveTo(1,cc.v2(-cc.Canvas.instance.node.width / 2, this.node.y)),
        //                 cc.moveTo(1,cc.v2(0, this.node.y)),
        //             ))
        //         },4.5,cc.macro.REPEAT_FOREVER,3)
        
        // }
        //enemy scaleUp
        if (this.node.name == "enemy4_t12" || this.node.name == "e_4new") {
            this.schedule(() => {
                this.node.runAction(cc.sequence(
                    cc.scaleTo(1, 1, 1).easing(cc.easeSineOut()),
                    cc.scaleTo(1, 0.5, 0.5).easing(cc.easeSineOut()),
                ))
            }, 4, cc.macro.REPEAT_FOREVER, 1)
        }
        if (this.node.name == "enemyz30") {
            this.schedule(() => {
                this.node.runAction(cc.sequence(
                    cc.scaleTo(1, 2, 2).easing(cc.easeSineOut()),
                    cc.scaleTo(1, 1, 1).easing(cc.easeSineOut()),
                ))
            }, 8, cc.macro.REPEAT_FOREVER, 2.5)
        }
        if (this.node.name == "enemy_1") {
            this.schedule(() => {
                this.node.runAction(cc.sequence(
                    cc.scaleTo(1, 1.3, 1.3).easing(cc.easeSineOut()),
                    cc.scaleTo(1, 0.5, 0.5).easing(cc.easeSineOut()),
                ))
            }, 11, cc.macro.REPEAT_FOREVER, 4.5)
        }

        if (this.node.name == 'enemy_monster') {
            this.scheduleOnce(() => {
                this.node.getComponent(sp.Skeleton).setAnimation(1, 'attack', true);
                // this.scheduleOnce(() => {
                //     this.node.getComponent(sp.Skeleton).setAnimation(1, 'move', true);
                // }, 1.65)
            }, 5)
        }
        if (this.node.name == "enemy_26") {
            this.schedule(() => {
                this.node.runAction(cc.sequence(
                    cc.moveBy(2.5, cc.v2(0, -300)).easing(cc.easeSineOut()),
                    cc.moveBy(2.5, cc.v2(0, 300)).easing(cc.easeSineOut()),
                ))
            }, 5.1, cc.macro.REPEAT_FOREVER, 3)
        }

        if (this.node.name == "eSwinging") {
            this.schedule(() => {
                this.node.runAction(cc.sequence(
                    cc.moveBy(0.8, 30, 0).easing(cc.easeSineOut()),
                    cc.moveBy(0.8, -30, 0).easing(cc.easeSineIn()),
                    cc.moveBy(0.8, -30, 0).easing(cc.easeSineOut()),
                    cc.moveBy(0.8, 30, 0).easing(cc.easeSineIn())
                ))
            }, 3.2, cc.macro.REPEAT_FOREVER, 1)
        }
        this.scheduleOnce(() => {
            if (this.node.name == "crop1") {
                this.node.runAction(cc.sequence(
                    cc.moveTo(this.timeSpeedCrop, this.node.position.x - this.distance, this.node.position.y - this.distance - this.disY),
                    cc.moveTo(this.timeSpeedCrop, this.node.position.x + this.distance, this.node.position.y + this.distance + this.disY),
                ).repeatForever());
            }
            else if (this.node.name == "crop2") {
                this.node.runAction(cc.sequence(
                    cc.moveTo(this.timeSpeedCrop, this.node.position.x + this.distance, this.node.position.y - this.distance - this.disY),
                    cc.moveTo(this.timeSpeedCrop, this.node.position.x - this.distance, this.node.position.y + this.distance + this.disY),
                ).repeatForever());
            }
            else if (this.node.name == "crop3") {
                this.node.runAction(cc.sequence(
                    cc.moveTo(this.timeSpeedCrop, this.node.position.x, this.node.position.y - this.distance - this.disY),
                    cc.moveTo(this.timeSpeedCrop, this.node.position.x, this.node.position.y + this.distance + this.disY),
                ).repeatForever());
            }
        }, 1.8)

        this.hpTotal = this.hp;
        if (this.bar)
            this.widthBar = this.bar.children[0].scaleX;
        var centrePos = this.node.getChildByName('centrePos');
        if (centrePos != null) {
            this.centrePos = centrePos.getPosition();
        } else {
            if (this.adjustExplosion != null) {
                this.centrePos = this.adjustExplosion;
            } else {
                this.centrePos = new cc.Vec2(0, this.node.height / 2);
            }
        }
        if (this.explosionPool == null)
            this.explosionPool = Helper.poolManager.eExplosion;
        this.node.emit(EnemyEvent.INIT.toString());
    }

    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        if (other.node.group == 'shipBullet') {
            // if (this.checkExplosion >= 2) {
            // if (G.hitBullet != null)
            //     cc.audioEngine.playEffect(G.hitBullet, false);
            // this.checkExplosion = 0;
            // }
            if (this.node.name == 'Axis') {
                this.spawnExplosion(this.node.x + 20 * (Math.random() * 2 - 1), this.node.y + 65);
            }
            else {
                // this.spawnExplosion(this.node.x + (Math.random() * 2 - 1) * 35, this.node.y + (Math.random() * 2 - 1) * 35);
            }
            if (!G.startSpawnItem) {
                G.startSpawnItem = true;
            }
            if (self.node.group == "miniboss") {
                other.node.destroy();
            }
            else {
                // if (self.node.name == 'boss_S' || self.node.name == 'boss_S_4') {
                //     this.spawnExplosion(this.node.x + (Math.random() * 2 - 1) * 80, this.node.y + 10 + (Math.random() * 2 - 1) * 15);
                // this.checkExplosion++;
                // }
                // else {
                //     if (G.hitBullet != null)
                //         cc.audioEngine.playEffect(G.hitBullet, false);
                // }
                this.onHitShipBullet(other.node);

                if (this.bar)
                    this.BarHealthy();
                // G.score += 10;
            }
        }
        if (self.node.group == 'miniboss') {
            if (other.node.name == 'rocket') {
                this.onHitShipBullet(other.node);

                if (this.bar)
                    this.BarHealthy();
                // G.score += 10;
            }
        }
    }

    BarHealthy() {
        this.bar.active = true;
        this.bar.children[0].scaleX = this.widthBar * this.hp / this.hpTotal;
    }

    runExplosionEffect(parent: cc.Node, x: number, y: number, scale: number, speed: number) {
        if (this.explosionPool == null)
            return;
        var explosion = this.explosionPool.createObject(parent);
        explosion.x = x;
        explosion.y = y;
        explosion.scale = scale;
        var clip = explosion.getComponent(cc.Animation).defaultClip;
        clip.speed = speed;
        var t = clip.duration / clip.speed;
        explosion.getComponent(cc.Animation).play();
        explosion.getComponent(cc.Animation).scheduleOnce(function () {
            this.killObject(explosion);
        }.bind(this.explosionPool), t);
    }

    onHitShipBullet(node: cc.Node) {
        node.destroy();
        this.node.emit(EnemyEvent.HIT_BULLET.toString());
        this.onLoseHp();
    }

    runHitBulletEffect() {
        this.node.runAction(cc.sequence(
            cc.moveBy(0.15, 0, 20),
            cc.moveBy(0.15, 0, -20)
        ));
    }

    onLoseHp() {

        // if (G.hpBoss) {
        this.hp--;
        // if (this.node.group == 'boss') {
        //     if (this.hp <= 50) {
        //         G.endG = true;
        //     }
        // }
        // }
        //br18
        // if (this.hp <= 2) {
        //     this.shield.active = false;
        //     this.spawnExplosion2(this.node.x, this.node.y + 25);
        // }
        if (this.hp <= 0) {
            // if (G.enemyDie != null)
            //     cc.audioEngine.playEffect(G.enemyDie, false);
            this.SpawnerCoin();
            // this.node.getComponent(cc.CircleCollider).enabled = false;
            // CoinContainScripts.Instance(CoinContainScripts).xenemy = this.node.x;
            // CoinContainScripts.Instance(CoinContainScripts).yenemy = this.node.y
            // CoinContainScripts.Instance(CoinContainScripts).SpawnerCoin();
            this._onEnemyDie();
        }
    }

    private _onEnemyDie() {
        if (this.node.group == 'boss') {
            G.bossDie = true;
        }
        this.node.destroy();
    }

    onDestroy() {
        // cc.audioEngine.playEffect(G.enemyDie, false);
        G.numberBossDie++;
        G.totalE++;
        if (this.e_3circle) {
            G.e_3circleAll++;
        }
        else if (this.e_triangle) {
            G.e_triangle++;
        }
        else if (this.e_flower) {
            G.e_flower++;
        }
        else if (this.e_jump) {
            G.e_jump++;
        }
        else if (this.e_axis) {
            G.e_groupBoss++;
        }
        else if (this.e_circle_exchange) {
            G.e_cirlce_exchange++;
        }
        this.node.emit(EnemyEvent.DIE.toString());
        this._isDie = true;
        var pos = this.node.parent.convertToNodeSpaceAR(this.node.convertToWorldSpaceAR(this.centrePos));
        this.runExplosionEffect(this.node.parent, pos.x, pos.y + 15, this.scaleExplosion * this.node.scale, this.speedExplosion);
        if (this.onDie)
            this.onDie(this);
    }

    isDie() {
        return this._isDie;
    }

    public spawnExplosion(x: number, y: number) {
        var newExplosion = this.hitExplosionPool.createObject(cc.Canvas.instance.node);
        newExplosion.zIndex = 100;
        // newExplosion.scale = 1.1;
        newExplosion.x = x;
        newExplosion.y = y;
    }

    SpawnerCoin() {
        var arraycoin = [];
        for (let i = 0; i < 3; i++) {
            var newCoin = cc.instantiate(this.Coin);
            newCoin.parent = cc.Canvas.instance.node;
            newCoin.x = this.node.x;
            newCoin.y = this.node.y;
            arraycoin.push(newCoin);
        }

        var RandomDelay = 0;
        for (let i = 0; i < arraycoin.length; i++) {
            let Ran = Math.round(Math.random());
            if (Ran > 0.5) {
                do {
                    var RandomPosx2 = -150 * Math.random();
                } while (RandomPosx2 < -40);
                do {
                    var RandomPosy2 = -150 * Math.random();
                } while (RandomPosy2 < -40);
                arraycoin[i].runAction(cc.sequence(cc.moveTo(0.5, cc.v2(arraycoin[i].x + RandomPosx2, arraycoin[i].y + RandomPosy2)), cc.delayTime(RandomDelay), cc.moveTo(0.5, cc.v2(-cc.Canvas.instance.node.width / 2 + 30, cc.Canvas.instance.node.height / 2 - 27)), cc.callFunc(() => {
                    //cc.audioEngine.playEffect(Global.Coin, false);

                    arraycoin[i].destroy();

                })));
                arraycoin[i].runAction(cc.moveTo(0.5, cc.v2(arraycoin[i].x + RandomPosx2, arraycoin[i].y + RandomPosy2)));
            }
            else {
                do {
                    var RandomPosx = 150 * Math.random();
                } while (RandomPosx > 40);
                do {
                    var RandomPosy = 150 * Math.random();
                } while (RandomPosy > 40);
                arraycoin[i].runAction(cc.sequence(cc.moveTo(0.5, cc.v2(arraycoin[i].x + RandomPosx, arraycoin[i].y + RandomPosy)), cc.delayTime(RandomDelay), cc.moveTo(0.5, cc.v2(-cc.Canvas.instance.node.width / 2 + 30, cc.Canvas.instance.node.height / 2 - 27)), cc.callFunc(() => {

                    arraycoin[i].destroy();
                })));
                arraycoin[i].runAction(cc.moveTo(0.5, cc.v2(arraycoin[i].x + RandomPosx, arraycoin[i].y + RandomPosy)));
            }
            RandomDelay = RandomDelay + 0.01;
        }
    }
}