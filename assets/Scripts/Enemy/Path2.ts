import Global from "../Global";
import Jump from "./JumpToPlayer";
const { ccclass, property } = cc._decorator;

@ccclass
export default class path extends cc.Component {

    @property()
    speedPath: number = 0;

    @property()
    loop: boolean = true;

    @property(cc.Prefab)
    dragon: cc.Prefab = null;

    @property()
    timeDelay: number = 0;

    @property()
    timeEnemy: number = 0;

    @property()
    numberDragons: number = 0;

    dragons: cc.Node[] = [];

    comVec: cc.Vec2[] = [];

    array: cc.Vec2[] = [];

    public onComlete: Function = null;

    checkComplete: boolean = true;
    posX: number = 0;
    posY: number = 0;
    // @property(cc.Node)
    // rocket: cc.Node = null;

    @property()
    card: boolean = false;

    count: number = 0;
    @property(cc.Node)
    positioList: cc.Node[] = [];
    @property()
    timeMoveEnd: number = 0;
    onLoad() {
        for (let i = 0; i < this.node.childrenCount; i++) {
            this.array.push(cc.v2(this.node.children[i].x, this.node.children[i].y));
        }

        if (this.loop)
            this.array.push(cc.v2(this.node.children[0].x, this.node.children[0].y));

        this.schedule(() => {
            this.creatDragon();
        }, this.timeEnemy, this.numberDragons, this.timeDelay);
    }

    start() {
        this.schedule(() => {
            for (let i = 0; i < this.dragons.length; i++) {
                if (this.dragons[i].activeInHierarchy) {
                    if (this.dragons[i].y != this.comVec[i].y) {
                        //khi con rồng thì vị trí của nó sẽ # vs vị trí ban đầu đc lưu, tính vector và góc lệch giữa 2 vị trí để quay góc rồng
                        let degree = this.betweenDegree(this.comVec[i], cc.v2(this.dragons[i].x, this.dragons[i].y)) - 90;
                        this.dragons[i].angle = cc.misc.lerp(this.dragons[i].angle, degree, 0.1);
                        this.comVec[i] = cc.v2(this.dragons[i].x, this.dragons[i].y);
                    }
                }
            }
        }, 0.001, cc.macro.REPEAT_FOREVER, 0)
    }

    betweenDegree(comVec, dirVec) {
        let angleDeg = Math.atan2(dirVec.y - comVec.y, dirVec.x - comVec.x) * 180 / Math.PI;
        return angleDeg;
    }

    // update(dt) {
    //     // if (this.node.name == "rocket1" || this.node.name == "rocket2") {
    //         for (let i = 0; i < this.dragons.length; i++) {
    //             if (this.dragons[i].activeInHierarchy) {
    //                 if (this.dragons[i].y != this.comVec[i].y) {
    //                     //khi con rồng thì vị trí của nó sẽ # vs vị trí ban đầu đc lưu, tính vector và góc lệch giữa 2 vị trí để quay góc rồng
    //                     let degree = this.betweenDegree(this.comVec[i], cc.v2(this.dragons[i].x, this.dragons[i].y)) - 90;
    //                     this.dragons[i].angle = cc.misc.lerp(this.dragons[i].angle, degree, 0.1);
    //                     this.comVec[i] = cc.v2(this.dragons[i].x, this.dragons[i].y);
    //                 }
    //             }
    //         }
    //     }

    //     // if (this.checkComplete) {
    //     //     if (Global.numberEnemy <= 0) {
    //     //         this.checkComplete = false;
    //     //         if (this.onComlete)
    //     //             this.onComlete();
    //     //     }
    //     // }

    // }

    creatDragon() {
        let dragon = cc.instantiate(this.dragon);
        dragon.parent = this.node;
        dragon.x = this.node.children[0].x;
        dragon.y = this.node.children[0].y;
        this.dragons.push(dragon);
        // this.rocket.active = false;
        if (dragon.activeInHierarchy) {
            if (this.loop) {
                dragon.runAction(cc.cardinalSplineTo(this.speedPath, this.array, 0).repeatForever());
                //di chuyển nó theo list point  đã lưu trong array ở onLoad()

            } else {
                dragon.runAction(cc.sequence(cc.cardinalSplineTo(this.speedPath, this.array, 0),
                    cc.callFunc(() => {
                        dragon.runAction(cc.sequence(cc.moveTo(this.timeMoveEnd, cc.v2(this.positioList[this.count].x, this.positioList[this.count].y)),
                            cc.callFunc(() => {
                                dragon.rotation = 179;
                                // console.log(dragon.angle + "...." + dragon.rotation)
                            })))
                        this.count++;
                    })

                ));
            }
            //vị trí đầu tiên của con rồng lúc sinh ra
            this.comVec.push(cc.v2(dragon.x, dragon.y));
        }
    }

}
