import G from "../Global";
const { ccclass, property } = cc._decorator;

@ccclass
export default class RotateCircle extends cc.Component {
    @property
    r: number = 0;
    @property
    timeStep: number = 1
    // @property()
    alpha: number = 0;
    // @property()
    // posX: number = 0;
    // @property()
    // posY: number = 0;
    // @property()
    time: number = 1;
    @property()
    changeNumber: number = 1;
    @property()
    looptime: number = 1000000;
    @property(cc.Node)
    centrePoint: cc.Node = null;
    // LIFE-CYCLE CALLBACKS:
    x: number = 0;
    y: number = 0;

    onLoad() {
        // this.node.runAction(cc.moveTo(0.001,this.node.convertToWorldSpaceAR(this.node.getPosition())));
        this.x = this.centrePoint.x;
        this.y = this.centrePoint.y;
    }
    start() {
        //get angles
        var dis = {
            'x': this.x - this.node.x,
            'y': this.y - this.node.y
        };
        var angle = Math.atan2(dis.x, dis.y);
        this.alpha = angle;
        if (this.node.activeInHierarchy) {
            this.schedule(() => {
                this.alpha = this.alpha - this.changeNumber * cc.misc.degreesToRadians(12);
                let i = this.x + (this.r * Math.cos(this.alpha));
                let j = this.y + (this.r * Math.sin(this.alpha));
                this.node.runAction(cc.sequence(
                    cc.moveTo(this.time, cc.v2(i, j)),
                    cc.callFunc(() => {

                    })
                ))
            }, this.timeStep)
        }
    }
}
