import WaveQuangCao from "./Wave/WaveQuangCao";
import ShipController from "./Ship46";
import HandController from "./HandController";
import { Helper } from "./Helper";
import G from "./Global";

const { ccclass, property } = cc._decorator;

let GameState = {
    INIT: 0,

    READY: 1,
    PLAYING: 2,
    ENDGAME: 3
}

declare const window: any;

@ccclass
export default class SpaceShooterAd2 extends cc.Component {

    gameState: number = GameState.INIT;
    @property(cc.Node)
    logo: cc.Node = null;

    @property(cc.Node)
    txtMoveShip: cc.Node = null;

    @property(HandController)
    hand: HandController = null;

    @property(cc.Layout)
    bgLayout: cc.Layout = null;

    // @property(cc.Node)
    // buttonInstall: cc.Node = null;

    index: number = 1;

    nShip: number = 0;

    @property(ShipController)
    ship: ShipController = null;

    istouch: boolean = true;

    ironsource: boolean = false;

    // @property({ type: cc.AudioClip })
    // bgSound: cc.AudioClip = null;
    // temp: number = 0;

    check1st: boolean = false;

    firstTouch: boolean = false;
    posX: number;
    posY: number;
    checkEnd: boolean = false;

    @property(cc.Node)
    tempBoss: cc.Node = null;

    isItemVip: boolean = false;
    ready: boolean = false;

    checkCard_1: boolean = false;
    checkCard_2: boolean = false;
    checkCard_3: boolean = false;
    checkCard_4: boolean = false;

    countChoose: number = 0;
    chooseDone: boolean = false;

    checkMerge: boolean = false;

    checkInteraction: boolean = false;


    choosebtn1: boolean = false;

    @property(cc.Node)
    menu_start: cc.Node = null;

    @property(cc.Node)
    menu_end: cc.Node = null;

    @property(cc.Node)
    bullet_rotation: cc.Node = null;

    level: number = 0;

    onLoad() {
        cc.view.enableAutoFullScreen(false);
        cc.director.getCollisionManager().enabled = false;

        this.txtMoveShip.zIndex = 100;
        this.ship.node.zIndex = 1;
    }

    btnPlay() {
        this.menu_start.active = false;
        this.scheduleOnce(() => {
            // this.buttonInstall.active = true;
            this.appearShip();
        }, 0.5)
    }

    appearShip() {
        this.checkMerge = true;
        G.startSpawnItem = true;
        this.ship.node.active = true;
        this.ship.node.runAction(cc.sequence(cc.moveTo(0.8, cc.v2(0, -280)).easing(cc.easeBackOut()), cc.callFunc(() => {
            this.node.getComponent("CanvasTouchManager").enabled = true;

            this.txtMoveShip.active = true;
            this.hand.node.opacity = 255;
            this.hand.node.zIndex = 12;
            this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 50);
            this.posX = this.ship.node.x;
            this.posY = this.ship.node.y;
            this.gameState = GameState.READY;
            this.startGame();
            this.scheduleOnce(() => {
                // this.ship.setEnableBullet(true);
                cc.director.getCollisionManager().enabled = true;
            }, 0.2)
            G.startPlay = true;
        })))
    }

    start() {
        // this.temp = cc.audioEngine.play(this.bgSound, true, 1);
        this.gameState = GameState.INIT;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
        if (this.ironsource) {
            window.NUC.trigger.ready("ready")
        }

        // auto play
        // this.scheduleOnce(() => {
        //     this.btnBossFight();
        // }, 6)
    }

    onTouchStart(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                if (!G.endG && !G.enableMouse) {
                    this.startGame();
                    this.Interaction();
                }
                break;

            case GameState.PLAYING:
                if (!G.endG && !G.enableMouse) {
                    this.txtMoveShip.active = false;
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                    this.hand.stopSwipeAnim();
                    // this.unscheduleAllCallbacks();
                }
                break;
        }
    }

    onTouchMove(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                if (!G.endG && !G.enableMouse) {
                    this.Interaction();
                    this.startGame();
                }
                break;
            case GameState.PLAYING:
                if (!G.endG && !G.enableMouse) {
                    this.txtMoveShip.active = false;
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                    this.hand.stopSwipeAnim();
                    // this.unscheduleAllCallbacks();
                }
                break;
        }

    }

    onTouchEnd(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!G.endG) {
                    this.ship.setEnableBullet(false);
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 50);
                }
                break;
        }
    }

    onTouchCancel(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!G.endG) {
                    this.ship.setEnableBullet(false);
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 50);
                    // console.log('cancel')
                }
                break;
        }
    }

    Interaction() {
        this.bgLayout.node.active = false;;
        if (this.ironsource && !this.checkInteraction) {
            this.checkInteraction = true
            window.NUC.trigger.interaction();
        }
    }

    startGame() {
        if (this.checkMerge) {
            this.gameState = GameState.PLAYING;
            // this.hand.stopSwipeAnim();
            this.ship.setEnableMove(true);
        }
    }

    update(dt) {
        if (!this.check1st && G.totalE >= 1 && !G.endG) {
            this.bullet_rotation.active = false;
            this.check1st = true;
            this.endGame()
        }
    }

    endGame() {
        this.logo.destroy();
        this.hand.node.active = false;
        this.txtMoveShip.active = false;
        G.endG = true;
        this.ship.setEnableBullet(false);
        this.ship.node.active = false;
        this.ship.setEnableMove(false);
        cc.director.getCollisionManager().enabled = false;
        this.scheduleOnce(() => {
            this.menu_end.active = true;
        },0.5)
        // cc.audioEngine.stop(this.temp);

        G.text = false;
        this.checkEnd = true;

        if (this.ironsource) {
            window.NUC.trigger.endGame('win')
        }

        // if (this.buttonInstall)
        //     this.buttonInstall.active = false;


        this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    }
}