import IWave from "../Wave/IWave";
import BaseWave from "../Wave/BaseWave";

const { ccclass, property } = cc._decorator;

@ccclass
export default class LevelManager extends cc.Component {

    public onComplete: Function = null;
    @property([cc.Prefab])
    wavePrefabs: cc.Prefab[] = [];
    @property([cc.Vec2])
    startPath: cc.Vec2[] = [];
    @property(cc.Color)
    color: cc.Color = cc.color(255, 255, 255, 255);
    startLevel() {
        this.initWave(0);
    }

  
    initWave(i: number) {
        let waveNode = cc.instantiate(this.wavePrefabs[i]);
        waveNode.parent = cc.Canvas.instance.node;
        let wave = waveNode.getComponent(IWave);
        wave.startWave();
        if (i === this.wavePrefabs.length - 1) {
            wave.onComplete = () => {
                if (this.onComplete)
                    this.onComplete();
            }
        } else {
            wave.onComplete = () => {
                setTimeout(() => {
                    this.initWave(i + 1);
                }, 1000);
            }
        }
    }
}
