import G from './Global'

const { ccclass, property } = cc._decorator;

@ccclass
export default class HpPercent extends cc.Component {
    @property(cc.Node)
    bar: cc.Node = null;

    hpTotal: number = 0;

    widthBar: number = 0;

    percent: number = 0;

    @property(cc.Node)
    percentTxt: cc.Node = null;
    @property(cc.Node)
    coinTxt: cc.Node = null;

    start() {
        this.hpTotal = G.eDie;
        if (this.bar)
            this.widthBar = this.bar.children[0].scaleX;
    }
    BarHealthy() {
        this.bar.active = true;
        this.bar.children[0].scaleX = this.widthBar * G.eDie / this.hpTotal;
        this.percent = Math.round((G.eDie / this.hpTotal) * 100)
        this.percentTxt.getComponent(cc.Label).string = this.percent.toString();
        this.coinTxt.getComponent(cc.Label).string = G.score.toString();
    }
    update(dt) {
        if (this.bar) {
            this.BarHealthy();
        }
    }
}
