import G from "./Global";

const {ccclass, property} = cc._decorator;

@ccclass
export default class CanvasTouchManager extends cc.Component {

    start () {
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    }

    onTouchStart(event: cc.Touch){
        G.touchPos = this.node.convertToNodeSpaceAR(event.getLocation());
    }

    onTouchMove(event: cc.Touch) {
        G.touchPos = this.node.convertToNodeSpaceAR(event.getLocation());
    }

    onTouchEnd(event: cc.Touch) {
        G.touchPos = this.node.convertToNodeSpaceAR(event.getLocation());
    }

    onTouchCancel(event: cc.Touch) {
        G.touchPos = this.node.convertToNodeSpaceAR(event.getLocation());
    }

}
