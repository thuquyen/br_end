import G from "./Global";
const { ccclass, property } = cc._decorator;

@ccclass
export default class SpawnItem extends cc.Component {


    @property(cc.Prefab)
    item: cc.Prefab[] = [];

    start() {
        this.updateItem();
    }

    spawnItem() {
            var newItem = cc.instantiate(this.item[0]);
            newItem.parent = this.node;
            newItem.zIndex = 10;
            newItem.x = (this.node.x - 100) * (2 * Math.random() - 1);
            newItem.y = (this.node.y - 20) * Math.random();

            var newItem = cc.instantiate(this.item[1]);
            newItem.parent = this.node;
            newItem.zIndex = 10;
            newItem.x = (this.node.x + 80) * (2 * Math.random() - 1);
            newItem.y = (this.node.y - 40) * Math.random();

            this.scheduleOnce(() => {
                var newItem = cc.instantiate(this.item[2]);
                newItem.parent = this.node;
                newItem.zIndex = 10;
                newItem.x = (this.node.x + 100) * (2 * Math.random() - 1);
                newItem.y = (this.node.y ) * Math.random();

                var newItem = cc.instantiate(this.item[3]);
                newItem.parent = this.node;
                newItem.zIndex = 10;
                newItem.x = (this.node.x - 90) * (2 * Math.random() - 1);
                newItem.y = (this.node.y - 40) * Math.random();
            }, 1)
    }

    updateItem() {
        this.schedule(() => {
            if (!G.endG && G.startSpawnItem) {
                this.spawnItem();
            }
        }, 7, cc.macro.REPEAT_FOREVER, 0.01);
    }
}
