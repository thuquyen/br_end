import WaveQuangCao from "./Wave/WaveQuangCao";
import ShipController from "./Ship34";
import HandController from "./HandController";
import { Helper } from "./Helper";
import G from "./Global";

const { ccclass, property } = cc._decorator;

let GameState = {
    INIT: 0,
    READY: 1,
    PLAYING: 2,
    ENDGAME: 3
}

declare const window: any;

@ccclass
export default class SpaceShooterAd2 extends cc.Component {

    gameState: number = GameState.INIT;
    @property(cc.Node)
    logo: cc.Node = null;

    @property(cc.Node)
    endG: cc.Node = null;

    @property(cc.Node)
    txtMoveShip: cc.Node = null;

    @property(WaveQuangCao)
    wave1: WaveQuangCao = null;

    @property(HandController)
    hand: HandController = null;

    @property(cc.Layout)
    bgLayout: cc.Layout = null;

    @property(cc.Node)
    buttonInstall: cc.Node = null;

    @property(cc.Node)
    titleMini: cc.Node = null;

    index: number = 1;

    nShip: number = 0;

    @property(ShipController)
    ship: ShipController = null;
    istouch: boolean = true;

    ironsource: boolean = false;

    @property({ type: cc.AudioClip })
    bgSound: cc.AudioClip = null;
    temp: number = 0;

    check1st: boolean = false;
    check2st: boolean = false;
    check3st: boolean = false;

    nextBoss: number = 0;
    doneChoose: boolean = false;
    // @property(cc.Node)
    textFeatured: cc.Node = null;

    firstTouch: boolean = false;
    posX: number;
    posY: number;
    checkEnd: boolean = false;

    @property(cc.Node)
    effectShip: cc.Node = null;

    @property(cc.Node)
    tempBoss: cc.Node = null;
    isItemVip: boolean = false;
    ready: boolean = false;

    @property(cc.Node)
    cardEvo: cc.Node = null
    @property(cc.Node)
    evo_bg: cc.Node = null;

    mergeDone: boolean = false;

    @property(cc.Node)
    heart: cc.Node = null;

    onLoad() {
        this.txtMoveShip.zIndex = 100;
        cc.view.enableAutoFullScreen(false);
        cc.director.getCollisionManager().enabled = false;
        this.ship.node.zIndex = 1;
        this.bgLayout.node.zIndex = 2;
        this.endG.zIndex = 3;
    }

    merger() {
        this.mergeDone = true;
        this.evo_bg.active = false;
        this.cardEvo.active = true;
        this.scheduleOnce(() => {
            this.wave1.appear(() => { })
            this.ship.node.runAction(cc.sequence(cc.moveTo(0.8, cc.v2(0, -280)).easing(cc.easeBackOut()), cc.callFunc(() => {
                this.txtMoveShip.active = true;
                this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
                this.hand.node.opacity = 255;
                this.hand.node.zIndex = 12;
            
                this.heart.active = true;
                // this.ship.setEnableBullet(true);
                // this.ship.tranform1();
                this.effectShip.active = false;
                this.posX = this.ship.node.x;
                this.posY = this.ship.node.y;
                this.node.getComponent("CanvasTouchManager").enabled = true;
                this.gameState = GameState.READY;
            })))
            // this.turnOnGuide();
        }, 3.5)
        this.buttonInstall.active = true;
    }

    start() {
        this.temp = cc.audioEngine.play(this.bgSound, true, 1);
        this.gameState = GameState.INIT;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);

        if (this.ironsource) {
            window.NUC.trigger.ready("ready")
        }

        //auto after 5s
        // this.scheduleOnce(() => {
        //     if (!this.mergeDone) {
        //         this.merger();
        //     }
        // }, 5.5)
    }

    turnOnGuide() {
        this.txtMoveShip.active = true;
        this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
        this.hand.node.opacity = 255;
        this.hand.node.zIndex = 12;
    }

    endGame() {
        this.heart.active = false;
        cc.audioEngine.stop(this.temp);
        var findDeep = function (node, nodeName) {
            if (node.name == nodeName) return node;
            for (var i = 0; i < node.children.length; i++) {
                var res = findDeep(node.children[i], nodeName);
                if (res) {
                    res.destroy();
                }

                // return res;
            }
        }
        findDeep(cc.director.getScene(), "item_Upgrade");
        findDeep(cc.director.getScene(), "Bullet_medusa_3");
        findDeep(cc.director.getScene(), "Bullet_medusa_4");

        this.ship.node.runAction(cc.sequence(
            cc.moveBy(1.1, 0, 2000).easing(cc.easeBackIn()),
            cc.callFunc(() => {
                findDeep(cc.director.getScene(), "ship");
            }))
        )
        G.text = false;
        this.checkEnd = true;

        if (this.ironsource) {
            window.NUC.trigger.endGame('win')
        }
        this.scheduleOnce(() => {
            this.endG.active = true;
        }, 1)

        if (this.buttonInstall)
            this.buttonInstall.active = false;
        this.titleMini.active = false;

        this.hand.node.active = false;
        this.txtMoveShip.active = false;
        this.ship.setEnableMove(false);
        this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    }

    onTouchStart(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                cc.director.getCollisionManager().enabled = true;
                G.clickStart = true;
                break;
            case GameState.PLAYING:
                this.txtMoveShip.active = false;
                this.hand.stopSwipeAnim();
                if (this.istouch) {
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                    this.istouch = false;
                    this.unscheduleAllCallbacks();
                }
                break;
        }
    }

    onTouchMove(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                cc.director.getCollisionManager().enabled = true;
                G.clickStart = true;
                break;
            case GameState.PLAYING:
                this.txtMoveShip.active = false;
                this.hand.stopSwipeAnim();
                if (this.istouch) {
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                    this.istouch = false;
                    this.unscheduleAllCallbacks();
                }
                break;
        }
    }

    onTouchEnd(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!G.endG && !G.isBoss) {
                    // this.ship.setEnableBullet(false);
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
                }
                break;
        }
    }

    onTouchCancel(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!G.endG && !G.isBoss) {
                    // this.ship.setEnableBullet(false);
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
                }
                break;
        }
    }

    startGame() {
        if (this.ironsource)
            window.NUC.trigger.interaction();
        this.gameState = GameState.PLAYING;
        // this.hand.stopSwipeAnim();
    }

    update(dt) {
        if (G.clickStart && !this.firstTouch) {
            G.startSpawnItem = true;
            this.firstTouch = true;
            this.startGame();
            this.bgLayout.node.opacity = 0;
        }

        if (!this.check1st && G.totalE >= 18) {
            this.check1st = true;
            G.speedBg = -550;
            G.isBoss = true;
            this.ship.setEnableBullet(false);
            this.scheduleOnce(() => {
                this.tempBoss.active = true;
                G.speedBg = -100;
                this.tempBoss.runAction(cc.sequence(cc.moveTo(1, cc.v2(0, 240)),
                    cc.callFunc(() => {
                        G.hpBoss = true;
                        G.isBoss = false;
                        this.txtMoveShip.active = true;
                        this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
                    })
                ));
                this.ship.setEnableBullet(true);
            }, 1.2)
        }

        if (!G.endG && G.bossDie) {
            G.endG = true
            this.scheduleOnce(() => {
                this.endGame();
            }, 0.5)
        }
    }
}