import G from "./Global";
import HPBar from "./HpBar";
import BasePool from "./common/BasePool";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ShipController extends cc.Component {

    @property()
    private enableMove: boolean = true;
    // @property()
    private enableBullet: boolean = false;
    private enableProtect: boolean = false;

    @property(BasePool)
    private bulletPool: BasePool = null;

    @property()
    private fireRate = 150;

    private fireTime = 0;
    private fireTimeR = 0;
    private fireTimeL = 0;
    private fireTimeE = 0;
    private fireTimeC = 0;

    bulletShip: cc.Prefab = null;

    @property(cc.Prefab)
    private efxPrefab: cc.Prefab = null;

    @property(cc.Vec2)
    private startPoint: cc.Vec2 = null;

    @property()
    private minY = -500;

    public onDie: Function = null;
    public onRevive: Function = null;
    public isDie: boolean = false;

    @property()
    private HPMAX = 1;
    private hp = 0;
    // @property(HPBar)
    public hpBar: HPBar = null;

    @property(Boolean)
    hackNoDie: boolean = false;

    @property(cc.Prefab)
    bullet16: cc.Prefab[] = [];
    @property(cc.Prefab)
    bullet17: cc.Prefab[] = [];
    bullet: cc.Prefab[] = [];
    @property(cc.Prefab)
    bulletCircle: cc.Prefab = null;
    @property(cc.Node)
    shieldEffect: cc.Node = null;
    startFire1: boolean = false;
    startFire2: boolean = false;
    startFire3: boolean = false;
    startFire4: boolean = false;
    time2S: boolean = false;
    strAnimCurrent: string = '';

    @property(cc.Node)
    luckyWheel: cc.Node = null;
    @property(sp.SkeletonData)
    ship22: sp.SkeletonData = null;
    isTranform: boolean = false;

    @property(cc.Node)
    heart: cc.Node[] = []

    public isSpin: boolean = false;
    @property(cc.Node)
    txtGuide: cc.Node = null;

    onLoad() {
        this.bulletPool.prefab = this.bullet16[0];
        this.bullet = this.bullet16;
        this.node.zIndex = 1;
        this.hp = this.HPMAX;
        G.ship = this;
    }

    setEnableMove(enable: boolean) {
        this.enableMove = enable;
    }

    setEnableBullet(enable: boolean) {
        this.enableBullet = enable;
    }

    setEnableProtect(enable: boolean) {
        this.enableProtect = enable;
    }

    changeAnimation(track: number, str: string, bool: boolean) {
        if (str === this.strAnimCurrent) {
            return;
        }
        this.getComponent(sp.Skeleton).setAnimation(track, str, bool);
        this.strAnimCurrent = str;
    }

    public tranform1() {
        this.changeAnimation(1, 'transform_1_2', false);
        this.scheduleOnce(() => {
            this.changeAnimation(1, '2', true);
            this.getComponent(cc.CircleCollider).enabled = true;
            G.isWheel = false;
            G.trans1 = false;
            this.scheduleOnce(() => {
                this.txtGuide.active = true;
                cc.director.pause();
            }, 0.2)
        }, 1.5)
    }
    public tranform2() {
        this.changeAnimation(1, 'transform_2_3', true);
        this.scheduleOnce(() => {
            this.changeAnimation(1, '3', true);
            G.isTransform = false;
            this.startFire4 = true;
        }, 1.2)
    }
    update(dt) {
        if (this.enableBullet) {
            this.updateBullet();
            if (!this.time2S) {
                this.time2S = true;
                this.scheduleOnce(() => {
                    this.startFire1 = true;
                    this.fireRate = 160;
                }, 1)
            }
            if (this.startFire1) {
                this.updateBullet1();
            }
            if (this.startFire2) {
                this.updateBullet2();
            }
            if (this.startFire3) {
                this.updateBullet3();
            }
        }

        if (this.enableMove) {
            this.updateMove();
        }
    }

    lerp(a: number, b: number, r: number) {
        return a + (b - a) * r;
    }

    private updateMove() {
        if (G.touchPos != null) {
            this.node.x = this.lerp(this.node.x, G.touchPos.x, 0.3);
            this.node.y = this.lerp(this.node.y, G.touchPos.y + 20, 0.3);
            if (this.node.y < this.minY) this.node.y = this.minY;
        }
    }

    private updateBullet() {
        if (Date.now() > this.fireTime + this.fireRate) {
            this.fireTime = Date.now();
            this.fireBullet();
        }
    }
    private updateBullet1() {
        if (Date.now() > this.fireTimeR + this.fireRate) {
            this.fireTimeR = Date.now();
            this.fireBulletRight1();
            if (this.startFire4) {
                if (Date.now() > this.fireTimeC + this.fireRate + 335) {
                    this.fireTimeC = Date.now();
                    this.fireBulletCircle();
                }
            }
        }
    }
    private updateBullet2() {
        if (Date.now() > this.fireTimeL + this.fireRate) {
            this.fireTimeL = Date.now();
            this.fireBulletRight2();
            if (this.startFire4) {
                if (Date.now() > this.fireTimeC + this.fireRate + 335) {
                    this.fireTimeC = Date.now();
                    this.fireBulletCircle();
                }
            }
        }
    }
    private updateBullet3() {
        if (Date.now() > this.fireTimeE + this.fireRate) {
            this.fireTimeE = Date.now();
            this.fireBulletRight3();
            if (this.startFire4) {
                if (Date.now() > this.fireTimeC + this.fireRate + 335) {
                    this.fireTimeC = Date.now();
                    this.fireBulletCircle();
                }
            }
        }
    }

    private fireBullet() {
        // cc.audioEngine.playEffect(G.enemyShoot, false);
        var newBulletShip = this.bulletPool.createObject(cc.Canvas.instance.node);
        newBulletShip.x = this.node.x;
        newBulletShip.y = this.node.y + 50;//50;
    }
    private fireBulletRight1() {
        var newBulletShip = this.bulletPool.createObject(cc.Canvas.instance.node);
        newBulletShip.x = this.node.x + 25;
        newBulletShip.y = this.node.y + 80;

        var newBulletShip = this.bulletPool.createObject(cc.Canvas.instance.node);
        newBulletShip.x = this.node.x - 25;
        newBulletShip.y = this.node.y + 80;

    }
    private fireBulletRight2() {
        var newBulletShip = this.bulletPool.createObject(cc.Canvas.instance.node);
        newBulletShip.x = this.node.x + 55;
        newBulletShip.y = this.node.y + 80;

        var newBulletShip = this.bulletPool.createObject(cc.Canvas.instance.node);
        newBulletShip.x = this.node.x - 55;
        newBulletShip.y = this.node.y + 80;
    }
    private fireBulletRight3() {

        var newBulletShip = this.bulletPool.createObject(cc.Canvas.instance.node);
        newBulletShip.x = this.node.x + 45;
        newBulletShip.y = this.node.y + 80 + 35;

        var newBulletShip = this.bulletPool.createObject(cc.Canvas.instance.node);
        newBulletShip.x = this.node.x - 45;
        newBulletShip.y = this.node.y + 80 + 35;
    }
    private fireBulletCircle() {
        var newBullet = cc.instantiate(this.bulletCircle);
        newBullet.parent = cc.Canvas.instance.node;
        newBullet.x = this.node.x;
        newBullet.y = this.node.y;
    }
    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        if (other.node.group == "item") {
            if (other.node.name == 'item_Upgrade') {
                this.itemUpgrade(other);
            }
            if (other.node.name == 'item_Vip') {
                G.isTransform = true;
                this.tranform2();
                other.node.destroy();
            }
        }

        if (this.enableProtect)
            return;
        if (other.node.group == 'boss') {
            // self.destroy();
            this.onHitBoss(other.node);
        }
        else if (other.node.group == 'bossBullet') {
            if (G.hitBullet != null)
            this.onHitBossBullet(other.node);
        }
        else if (other.node.group == 'e' || other.node.group == 'eSpecial') {
            // if (G.hitBullet != null)
            this.onHitEnemy(other.node);
        }
        else if (other.node.group == 'eBullet') {
            this.onHitEnemyBullet(other.node);
        }
    }

    itemUpgrade(other: cc.Collider) {
        G.bulletUpgrade++;
        this.changeBulletShip(this.bullet);
        other.node.destroy();
    }

    private onHitBoss(node: cc.Node) {
        this.loseHp(1);
    }

    private onHitBossBullet(node: cc.Node) {
        node.destroy();
        this.loseHp(1);
    }

    private onHitEnemy(node: cc.Node) {
        this.loseHp(1);
    }

    private onHitEnemyBullet(node: cc.Node) {
        node.destroy();
        this.loseHp(1);
    }

    public spin() {
        this.isSpin = true;
        this.luckyWheel.getComponent(cc.Animation).play();
        this.node.getComponent(sp.Skeleton).skeletonData = this.ship22;
        this.node.scale = 0.75;
        this.node.getChildByName("shield").scale = 1.6;
        this.node.getChildByName("levelup_effect").scale = 2.5;
        this.changeAnimation(1, '1', true);

        this.bullet = this.bullet17;
        this.bulletPool.prefab = this.bullet17[0];
        this.changeBulletShip(this.bullet);
        this.scheduleOnce(() => {
            this.luckyWheel.active = false;
            this.showLife(this.hp);
            G.startSpawnItem = true;
            this.scheduleOnce(() => {
                this.tranform1();
            }, 0.8)
        }, 2.4)
    }
    private loseHp(n: number) {
        this.hp -= n;
        if (this.heart[0].active) {
            this.heart[0].active = false;
        }

        if (this.hp <= 0) {
            // cc.audioEngine.playEffect(G.flyDie, false);
            this.node.opacity = 0;
            this.isDie = true;
            if (this.heart[1].active) {
                this.heart[1].active = false;
            }

            if (!G.enabledWheel) {
                this.explosion();
                G.trans1 = true;
                G.enabledWheel = true;
                this.luckyWheel.active = true;
                this.luckyWheel.zIndex = 20;
                G.isWheel = true;
                this.scheduleOnce(() => {
                    if (!this.isSpin) {
                        this.spin();
                    }
                }, 6)
            }
            else {
                this.explosion();
                this.showLife(this.hp);
                this.scheduleOnce(() => {
                    this.getComponent(cc.CircleCollider).enabled = true;
                }, 1)
            }

            G.bulletDie = true;
            G.lazeShip = false;
            if (this.hpBar) {
                this.hpBar.setPercent(this.hp / this.HPMAX);
            }
        }
    }

    private onShipDie() {
        this.isDie = true;
        this.explosion();
        if (this.onDie) {
            this.onDie();
        }
    }

    showLife(n: number) {

        this.node.opacity = 255;
        // if (this.tim.children[n])
        //     this.tim.children[n].active = false;
        this.node.setPosition(cc.v2(0, -598));
        this.shieldEffect.opacity = 255;
        this.enableProtect = true;
        G.touchPos = null;
        this.node.runAction(cc.sequence(cc.moveTo(0.3, cc.v2(0, -280)).easing(cc.easeQuarticActionIn()), cc.callFunc(() => {
            this.scheduleOnce(() => {
                this.enableProtect = false;
                this.shieldEffect.opacity = 0;
                // this.shieldEffect.runAction(cc.spawn(cc.fadeOut(0.3), cc.scaleTo(0.3, 0)));
            }, 0.5)
            this.enableBullet = true;
            G.touchPos = null;
            this.enableMove = true;
            G.bulletDie = false;
        })));
    }

    private explosion() {
        var efx = cc.instantiate(this.efxPrefab);
        efx.parent = this.node.parent;
        efx.x = this.node.x;
        efx.y = this.node.y;
        this.node.opacity = 0;
        this.enableBullet = false;
        this.enableMove = false;
        this.getComponent(cc.CircleCollider).enabled = false;
    }

    public revive() {
        this.enableProtect = true;
        G.touchPos = null;
        this.node.setPosition(this.startPoint);
        this.node.opacity = 255;
        // this.enableBullet = true;
        this.enableMove = true;
        this.getComponent(cc.CircleCollider).enabled = true;
        this.isDie = false;
        this.scheduleOnce(() => {
            this.enableProtect = false;
        }, 1);
        this.hp = this.HPMAX;
        if (this.onRevive) {
            this.onRevive();
        }
    }

    flyOut(done: Function = null) {
        G.checkWing = false;
        G.touchPos = null;
        this.enableMove = false;
        this.enableProtect = true;
        this.enableBullet = false;
        this.node.runAction(cc.sequence(cc.spawn(
            cc.moveBy(1.1, 0, 2000).easing(cc.easeBackIn()), cc.fadeOut(1.1)),
            cc.callFunc(() => {
                this.enableMove = false;
                G.touchPos = null;
                if (done)
                    done();
            })
        ));
    }

    changeBulletShip(prefabs) {
        switch (G.bulletUpgrade) {
            case 2: {
                this.startFire2 = true;
                break;
            }

            case 3: {
                this.startFire3 = true;
                break;
            }

            default: {
                break;
            }
        }
    }
}