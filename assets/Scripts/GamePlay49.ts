import WaveQuangCao from "./Wave/WaveQuangCao";
import ShipController from "./Ship48";
import HandController from "./HandController";
import { Helper } from "./Helper";
import G from "./Global";


const { ccclass, property } = cc._decorator;

let GameState = {
    INIT: 0,
    READY: 1,
    PLAYING: 2,
    ENDGAME: 3
}

declare const window: any;

@ccclass
export default class SpaceShooterAd2 extends cc.Component {

    gameState: number = GameState.INIT;

    @property(WaveQuangCao)
    wave: WaveQuangCao[] = [];

    @property(cc.Node)
    wave0: cc.Node = null;

    @property(cc.Node)
    endG: cc.Node = null;

    @property(cc.Node)
    txtMoveShip: cc.Node = null;

    @property(HandController)
    hand: HandController = null;

    // @property(cc.Node)
    // buttonInstall: cc.Node = null;

    @property(cc.Node)
    titleMini: cc.Node = null;

    index: number = 1;

    @property(ShipController)
    ship: ShipController = null;

    istouch: boolean = true;

    ironsource: boolean = false;

    mindworks: boolean = true;

    // @property({ type: cc.AudioClip })
    // bgSound: cc.AudioClip = null;
    // temp: number = 0;

    check1st: boolean = false;
    check2st: boolean = false;

    firstTouch: boolean = false;
    posX: number;
    posY: number;
    checkEnd: boolean = false;

    @property(cc.Node)
    menu: cc.Node[] = [];

    // check_play: boolean = false;

    @property(cc.Node)
    coinTxt: cc.Node = null;

    point: number = 0;

    // @property(cc.Node)
    // wall: cc.Node = null;

    @property(cc.Node)
    coin: cc.Node = null;

    @property(cc.Node)
    door: cc.Node = null;

    @property(cc.Prefab)
    item: cc.Prefab[] = [];

    check_item: boolean = false;

    onLoad() {
        cc.view.enableAutoFullScreen(false);
    }

    start() {
        this.scheduleOnce(() => {
            this.menu[0].runAction(cc.scaleTo(0.5, 1, 1).easing(cc.easeSineOut()));
        }, 0.5)
        // this.temp = cc.audioEngine.play(this.bgSound, true, 1);
        //auto play
        this.scheduleOnce(() => {
            if (!G.check_play) {
                G.check_play = true;
                this.btnPlay();
            }
        }, 8)

        this.txtMoveShip.zIndex = 100;
        cc.director.getCollisionManager().enabled = true;
        this.ship.node.zIndex = 1;
        // this.endG.zIndex = 3;

        this.gameState = GameState.INIT;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
        if (this.ironsource) {
            window.NUC.trigger.ready("ready")
        }

      
    }

    btnPlay() {
        this.wave0.active = true;
        this.wave[0].appear(() => {
            // this.scheduleOnce(() => {
              
                // this.scheduleOnce(() => {
                
                // }, 2)
            // }, 0.5)
        })
        this.wave[1].appear(() => {
            this.wave[2].appear(() => { })
         })
        this.menu[0].destroy();
        this.menu[1].destroy();
        // this.wall.getComponent(cc.Animation).play();
        this.scheduleOnce(() => {
            this.appearShip();
            this.coin.active = true;
        }, 0.5)
        G.allow_fire = false;
        G.check_play = true;

    }

    appearShip() {
        // this.buttonInstall.active = true;
        this.ship.node.runAction(cc.sequence(cc.moveTo(0.8, cc.v2(0, -280)).easing(cc.easeBackOut()), cc.callFunc(() => {
            this.turnOnGuide();
            this.posX = this.ship.node.x;
            this.posY = this.ship.node.y;
            this.node.getComponent("CanvasTouchManager").enabled = true;
            this.gameState = GameState.READY;
        })))
    }

    turnOnGuide() {
        this.txtMoveShip.active = true;
        this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 50);
        this.hand.node.zIndex = 12;
    }
    update(dt) {
        if (G.totalE > this.point) {
            this.coinTxt.getComponent(cc.Label).string = (this.point + 5).toString() + "K";
            this.point = G.totalE;
        }
        if (!G.endG) {
            if (!this.check_item) {
                if (G.totalE == 10) {
                    this.check_item = true;
                    this.spawnItem();
                }
            }

            if (G.totalE >= 36) {
                G.endG = true;
                this.endGame();
            }
        }
    }

    endGame() {
        // this.buttonInstall.active = false;
        this.coin.destroy();
        // cc.audioEngine.stop(this.temp);
        // cc.audioEngine.playEffect(G.finalWin, false);

        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        this.ship.flyOut();

        this.door.active = true;

        G.text = false;
        this.checkEnd = true;

        if (this.ironsource) {
            window.NUC.trigger.endGame('win')
        }
        this.scheduleOnce(() => {
            this.endG.active = true;
        }, 1.8)

        this.hand.node.active = false;
        this.txtMoveShip.active = false;
        this.ship.setEnableMove(false);

        this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    }

    onTouchStart(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                G.clickStart = true;
                this.startGame();
                break;
            case GameState.PLAYING:
                this.txtMoveShip.active = false;
                this.hand.stopSwipeAnim();
                if (G.check_play) {
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                }
                break;
        }
    }

    onTouchMove(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                G.clickStart = true;
                this.startGame();
                break;
            case GameState.PLAYING:
                this.txtMoveShip.active = false;
                this.hand.stopSwipeAnim();
                if (G.check_play) {
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                }
                break;
        }
    }

    onTouchEnd(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!this.ship.isDie && G.check_play) {
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 50);
                    this.ship.setEnableBullet(false);
                }
                break;
        }
    }

    onTouchCancel(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!this.ship.isDie && G.check_play) {
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 50);
                    this.ship.setEnableBullet(false);
                }
                break;
        }
    }

    startGame() {
        if (this.ironsource)
            window.NUC.trigger.interaction();
        this.gameState = GameState.PLAYING;
        this.hand.stopSwipeAnim();
        this.ship.setEnableMove(true);
    }

    spawnItem() {
        var newItem = cc.instantiate(this.item[0]);
        newItem.parent = this.node;
        newItem.zIndex = 10;
        newItem.x = 100
        newItem.y = 20

        var newItem = cc.instantiate(this.item[1]);
        newItem.parent = this.node;
        newItem.zIndex = 10;
        newItem.x = -80
        newItem.y = 30

        this.scheduleOnce(() => {
            var newItem = cc.instantiate(this.item[2]);
            newItem.parent = this.node;
            newItem.zIndex = 10;
            newItem.x = 70
            newItem.y = 30

            this.scheduleOnce(() => {
                var newItem = cc.instantiate(this.item[3]);
                newItem.parent = this.node;
                newItem.zIndex = 10;
                newItem.x = 0
                newItem.y = 60
            }, 1)
        }, 1)
    }
}