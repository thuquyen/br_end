import WaveQuangCao from "./Wave/WaveQuangCao";
import ShipController from "./Ship39";
import HandController from "./HandController";
import { Helper } from "./Helper";
import G from "./Global";
import LevelManager from "./Level/LevelManager";
const { ccclass, property } = cc._decorator;

let GameState = {
    INIT: 0,

    READY: 1,
    PLAYING: 2,
    ENDGAME: 3
}

declare const window: any;

@ccclass
export default class SpaceShooterAd2 extends cc.Component {

    gameState: number = GameState.INIT;
    @property(cc.Node)
    logo: cc.Node = null;

    @property(cc.Node)
    txtMoveShip: cc.Node = null;

    @property(HandController)
    hand: HandController = null;

    @property(cc.Layout)
    bgLayout: cc.Layout = null;

    @property(cc.Node)
    buttonInstall: cc.Node = null;

    index: number = 1;

    nShip: number = 0;

    @property(ShipController)
    ship: ShipController = null;

    istouch: boolean = true;

    ironsource: boolean = false;

    mindworks: boolean = false;

    // @property({ type: cc.AudioClip })
    // bgSound: cc.AudioClip = null;
    // temp: number = 0;

    check1st: boolean = false;
    check2st: boolean = false;
    check3st: boolean = false;

    nextBoss: number = 0;
    doneChoose: boolean = false;

    firstTouch: boolean = false;
    posX: number;
    posY: number;
    checkEnd: boolean = false;

    @property(cc.Node)
    tempBoss: cc.Node = null;

    isItemVip: boolean = false;
    ready: boolean = false;

    checkCard_1: boolean = false;
    checkCard_2: boolean = false;
    checkCard_3: boolean = false;
    checkCard_4: boolean = false;

    countChoose: number = 0;
    chooseDone: boolean = false;

    checkMerge: boolean = false;

    checkInteraction: boolean = false;

    @property(cc.Node)
    btn_Con: cc.Node = null;

    @property(cc.Node)
    planetList: cc.Node[] = [];

    @property(cc.Node)
    arrow: cc.Node = null;

    choosebtn1: boolean = false;

    @property(cc.Node)
    handChoose: cc.Node = null;

    @property(cc.Node)
    btnFly: cc.Node = null;

    @property(cc.Node)
    menu: cc.Node = null;

    @property(cc.Node)
    levelCircle: cc.Node = null;

    @property(LevelManager)
    level: LevelManager = null;

    @property(cc.Node)
    txt: cc.Node = null;

    onLoad() {
        cc.view.enableAutoFullScreen(false);
        cc.director.getCollisionManager().enabled = false;
        for (var i = 0; i < 4; i++) {
            this.planetList[i].getComponent(cc.Animation).play("planetRound");
        }
        this.btn_Con.zIndex = 200;
        this.txtMoveShip.zIndex = 100;
        this.ship.node.zIndex = 1;
        if (this.ironsource) {
            window.NUC.trigger.ready("ready")
        }
        // this.bgLayout.node.zIndex = 2;
    }

    doneClickBtn1() {
        this.btnFly.opacity = 255;
        this.arrow.opacity = 100;
        this.handChoose.getComponent(cc.Animation).stop();
        this.handChoose.active = false;
        this.btnPlay();
    }

    btnPlanet1() {
        if (!this.choosebtn1) {
            this.choosebtn1 = true;
            this.planetList[0].getComponent(cc.Animation).play("scalePlanet");
            for (var i = 1; i < 4; i++) {
                this.planetList[i].opacity = 150;
            }
            this.doneClickBtn1();
        }
    }

    btnPlanet2() {
        if (!this.choosebtn1) {
            this.choosebtn1 = true;
            this.planetList[1].getComponent(cc.Animation).play("scalePlanet");
            this.planetList[0].opacity = 150;
            for (var i = 2; i < 4; i++) {
                this.planetList[i].opacity = 150;
            }
            this.doneClickBtn1();
        }
    }

    btnPlanet3() {
        if (!this.choosebtn1) {
            this.choosebtn1 = true;
            this.planetList[2].getComponent(cc.Animation).play("scalePlanet");
            this.planetList[3].opacity = 150;
            for (var i = 0; i < 2; i++) {
                this.planetList[i].opacity = 150;
            }
            this.doneClickBtn1();
        }
    }
    btnPlanet4() {
        if (!this.choosebtn1) {
            this.choosebtn1 = true;
            this.planetList[3].getComponent(cc.Animation).play("scalePlanet");
            for (var i = 0; i < 3; i++) {
                this.planetList[i].opacity = 150;
            }
            this.doneClickBtn1();
        }
    }

    btnPlay() {
        this.txt.destroy();
        this.scheduleOnce(() => {
            this.btnFly.getComponent(cc.Animation).play();
            this.scheduleOnce(() => {
                this.buttonInstall.active = true;
                this.menu.active = false;
                this.appearShip();
                this.level.startLevel();
                this.scheduleOnce(() => {
                    this.levelCircle.active = true;
                }, 1.2)
                this.scheduleOnce(() => {
                    cc.director.getCollisionManager().enabled = true;
                }, 1)
            }, 0.6)
        }, 0.5)
    }

    appearShip() {
        this.checkMerge = true;
        this.ship.node.runAction(cc.sequence(cc.moveTo(0.8, cc.v2(0, -280)).easing(cc.easeBackOut()), cc.callFunc(() => {
            this.node.getComponent("CanvasTouchManager").enabled = true;

            this.txtMoveShip.active = true;
            this.hand.node.opacity = 255;
            this.hand.node.zIndex = 12;
            this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 50);
            this.posX = this.ship.node.x;
            this.posY = this.ship.node.y;
            this.gameState = GameState.READY;
            this.startGame();

            G.startPlay = true;
        })))
    }

    appearBossEnd() {
        // this.buttonInstall.destroy();
        this.tempBoss.runAction((cc.scaleTo(1, 0.85, 0.85)))
        this.tempBoss.runAction(cc.sequence(cc.moveBy(1, cc.v2(0, 135)), cc.callFunc(() => {
            this.tempBoss.opacity = 255;
            G.boss1 = true;
            this.tempBoss.getComponent(sp.Skeleton).setAnimation(1, 'tranform_1', true);
            this.scheduleOnce(() => {
                this.tempBoss.getComponent(sp.Skeleton).setAnimation(1, 'idle2', true);
            }, 0.95)
        }))
        )
    }


    start() {
        // this.temp = cc.audioEngine.play(this.bgSound, true, 1);
        this.gameState = GameState.INIT;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);


        // auto play
        this.scheduleOnce(() => {
            this.btnPlanet1();
        }, 6.2)
    }

    onTouchStart(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                if (!G.endG && !G.enableMouse) {
                    this.startGame();
                    this.Interaction();
                }
                break;

            case GameState.PLAYING:
                if (!G.endG && !G.enableMouse) {
                    this.txtMoveShip.active = false;
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                    this.hand.stopSwipeAnim();
                    // this.unscheduleAllCallbacks();
                }
                break;
        }
    }

    onTouchMove(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                if (!G.endG && !G.enableMouse) {
                    this.Interaction();
                    this.startGame();
                }
                break;
            case GameState.PLAYING:
                if (!G.endG && !G.enableMouse) {
                    this.txtMoveShip.active = false;
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                    this.hand.stopSwipeAnim();
                    // this.unscheduleAllCallbacks();
                }
                break;
        }

    }

    onTouchEnd(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!G.endG) {
                    this.ship.setEnableBullet(false);
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 50);
                }
                break;
        }
    }

    onTouchCancel(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!G.endG) {
                    this.ship.setEnableBullet(false);
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 50);
                    // console.log('cancel')
                }
                break;
        }
    }

    Interaction() {
        this.bgLayout.node.active = false;;
        if (this.ironsource && !this.checkInteraction) {
            this.checkInteraction = true
            window.NUC.trigger.interaction();
        }
    }

    startGame() {
        G.startSpawnItem = true;
        if (this.checkMerge) {
            this.gameState = GameState.PLAYING;
            // this.hand.stopSwipeAnim();
            this.ship.setEnableMove(true);
        }
    }

    update(dt) {
        if (G.totalE >= 26 && !this.check1st && !G.endG) {
            this.check1st = true;
            this.activeBossEnd();
        }
    }

    activeBossEnd() {
        this.ship.setEnableBullet(false);
        cc.director.getCollisionManager().enabled = false;
        G.speedBg = -550;
        this.scheduleOnce(() => {
            cc.director.getCollisionManager().enabled = true;
            G.speedBg = -80;
            this.appearBossEnd()
            this.endGame()
        }, 0.5)

    }

    endGame() {
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        G.endG = true;
        this.ship.setEnableBullet(false);
        this.ship.node.runAction(cc.moveTo(0.5, cc.v2(0, -290)))
        this.ship.setEnableMove(false);
        cc.director.getCollisionManager().enabled = false;

        // cc.audioEngine.stop(this.temp);
        this.scheduleOnce(() => {
            this.btn_Con.active = true;
        }, 1)

        var findDeep = function (node, nodeName) {
            if (node.name == nodeName) return node;
            for (var i = 0; i < node.children.length; i++) {
                var res = findDeep(node.children[i], nodeName);
                if (res) {
                    res.destroy();
                }
                // return res;
            }
        }
        findDeep(cc.director.getScene(), "item_Upgrade");
        findDeep(cc.director.getScene(), "Bullet_41_4");
        findDeep(cc.director.getScene(), "item_Vip");

        G.text = false;
        this.checkEnd = true;

        if (this.ironsource) {
            window.NUC.trigger.endGame('win')
        }


        if (this.buttonInstall)
            this.buttonInstall.active = false;

        this.hand.node.active = false;
        this.txtMoveShip.active = false;
        this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    }
}