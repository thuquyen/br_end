import WaveQuangCao from "./Wave/WaveQuangCao";
import LevelManager from "./level/LevelManager";
import ShipController from "./ShipInstant";
import HandController from "./HandController";
import { Helper } from "./Helper";
import G from "./Global";

const { ccclass, property } = cc._decorator;

let GameState = {
    INIT: 0,
    READY: 1,
    PLAYING: 2,
    ENDGAME: 3
}

declare const window: any;

@ccclass
export default class SpaceShooterAd2 extends cc.Component {

    gameState: number = GameState.INIT;
    @property(cc.Node)
    logo: cc.Node = null;

    @property(cc.Node)
    txtMoveShip: cc.Node = null;

    @property(cc.Prefab)
    wave0_Prefab: cc.Prefab = null

    @property(WaveQuangCao)
    w0_heart_1: WaveQuangCao = null

    @property(WaveQuangCao)
    w1_hexagon: WaveQuangCao = null

    @property(cc.Node)
    w2_groupBoss: cc.Node = null;

    @property(cc.Node)
    w3_Circle: cc.Node = null;

    @property(LevelManager)
    w3_exchange: LevelManager = null;

    @property(WaveQuangCao)
    w4_jump: WaveQuangCao = null

    @property(LevelManager)
    w5_2heart: LevelManager = null;

    @property(cc.Node)
    waveBoss: cc.Node = null;

    @property(HandController)
    hand: HandController = null;

    @property(cc.Layout)
    bgLayout: cc.Layout = null;

    index: number = 1;

    nShip: number = 0;

    @property(ShipController)
    ship: ShipController = null;

    istouch: boolean = true;

    ironsource: boolean = false;

    checkWave2: boolean = false;
    checkWave3: boolean = false;
    check1st: boolean = false;
    check2st: boolean = false;
    check3st: boolean = false;
    check4st: boolean = false;
    check5st: boolean = false;

    nextBoss: number = 0;
    doneChoose: boolean = false;

    firstTouch: boolean = false;
    posX: number;
    posY: number;
    checkEnd: boolean = false;

    isItemVip: boolean = false;
    ready: boolean = false;

    checkMerge: boolean = false;

    checkInteraction: boolean = false;

    checkShipDie: boolean = false;

    //txt load wave
    @property(cc.Node)
    loadLevel: cc.Node = null;

    @property(cc.Node)
    txt_Up: cc.Node = null;

    @property(cc.Node)
    txt_Down: cc.Node = null;

    @property(cc.SpriteFrame)
    txt_Up_Frame: cc.SpriteFrame[] = [];

    @property(cc.SpriteFrame)
    txt_Down_Frame: cc.SpriteFrame[] = [];

    @property(cc.Node)
    menu: cc.Node = null;
    @property(cc.Node)
    next: cc.Node[] = [];
    @property(cc.Node)
    replay: cc.Node = null;
    @property(cc.Node)
    install: cc.Node = null;

    @property(cc.Prefab)
    wave_ggInstant: cc.Prefab[] = []

    @property(cc.Node)
    bulletRotate: cc.Node = null;

    @property({ type: cc.AudioClip })
    bgSound: cc.AudioClip = null;
    temp: number = 0;

    appearShip() {
        this.checkMerge = true;
        this.node.stopAllActions
        this.ship.node.runAction(cc.sequence(cc.moveTo(0.8, cc.v2(0, -270)).easing(cc.easeBackOut()),
            cc.callFunc(() => {
                this.node.getComponent("CanvasTouchManager").enabled = true;

                this.posX = this.ship.node.x;
                this.posY = this.ship.node.y;
                this.gameState = GameState.READY;
                this.startGame();
            })))
    }

    showWaveTxt(number_Up: number, number_Down: number) {
        this.txtMoveShip.active = false;
        this.txt_Up.getComponent(cc.Sprite).spriteFrame = this.txt_Up_Frame[number_Up];
        this.txt_Down.getComponent(cc.Sprite).spriteFrame = this.txt_Down_Frame[number_Down];

        this.loadLevel.opacity = 255;
        this.loadLevel.getComponent(cc.Animation).play();
        this.scheduleOnce(() => {
            this.loadLevel.opacity = 0;
        }, 0.75)
    }

    start() {
        this.temp = cc.audioEngine.play(this.bgSound, true, 1);
        cc.view.enableAutoFullScreen(false);
        this.txtMoveShip.zIndex = 100;
        this.ship.node.zIndex = 1;
        cc.director.getCollisionManager().enabled = false;
        this.menu.zIndex = 200;
        this.gameState = GameState.INIT;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);

        if (this.ironsource) {
            window.NUC.trigger.ready("ready")
        }

        this.appearShip();

        this.Play();
    }

    nextLevel_1() {
        cc.audioEngine.playEffect(G.click, false);
        this.logo.opacity = 255;
        // this.menu.active = false;
        this.next[0].destroy();
        this.menu.active = false;

        this.showWaveTxt(1, 0);
        this.scheduleOnce(() => {
            this.showWaveTxt(3, 3);
            this.scheduleOnce(() => {
                // wave 2 - 1/3
                G.level2 = true;
                this.w2_groupBoss.active = true;
                this.scheduleOnce(() => {
                    cc.director.getCollisionManager().enabled = true;
                    G.menu = false;
                }, 1)
            }, 1)
        }, 1)
    }

    nextLevel_2() {
        cc.audioEngine.playEffect(G.click, false);
        this.logo.opacity = 255;
        // this.menu.active = false;
        this.next[1].destroy();
        this.menu.active = false;

        this.showWaveTxt(2, 0);
        // cc.director.getCollisionManager().enabled = false;
        this.scheduleOnce(() => {
            this.showWaveTxt(3, 6);
            this.scheduleOnce(() => {
                // console.log("2 heart")
                // wave 5 - 1/5
                this.w5_2heart.startLevel();
            }, 1)
        }, 1)
    }

    replay_btn() {
        cc.audioEngine.playEffect(G.click, false);
        G.e_3circleAll = 0;
        G.e_triangle = 0;
        G.e_flower = 0;
        G.e_jump = 0;
        G.e_groupBoss = 0;
        G.bossDie = false;
        G.bulletUpgrade = 0;
        G.e_cirlce_exchange = 0;
        G.level2 = false;

        this.scheduleOnce(() => {
            cc.director.loadScene("GgInstant_2");
        }, 0.5)
    }
    ins_btn() {
        cc.audioEngine.playEffect(G.click, false);
        cc.androidInstant.showInstallPrompt()
    }
    next_2() {
        this.w1_hexagon.onComplete = () => {
            this.txtMoveShip.active = false;
            this.menu.active = true;
            this.logo.opacity = 0;

            //level2
            this.next[0].active = true;
            G.menu = true;
            cc.director.getCollisionManager().enabled = false;
        }
    }

    Play() {
        //level 1
        this.showWaveTxt(0, 0);

        this.scheduleOnce(() => {
            this.showWaveTxt(3, 1);
            this.scheduleOnce(() => {
                //wave 0 - 1/2
                this.w0_heart_1.appear(() => { });
                this.scheduleOnce(() => {
                    cc.director.getCollisionManager().enabled = true;
                }, 2.5)

                this.txtMoveShip.active = true;
                this.hand.node.opacity = 255;
                this.hand.node.zIndex = 12;
                this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 50);
            }, 1)
        }, 1)


        //wave 1 - 2/2
        this.w0_heart_1.onComplete = () => {
            if (!G.delete) {
                this.showWaveTxt(3, 2);
                this.scheduleOnce(() => {
                    this.w1_hexagon.appear(() => { });
                }, 1)
            }
        }

        this.next_2();

        //wave 3/5
        this.w5_2heart.onComplete = () => {
            this.showWaveTxt(3, 8);
            cc.director.getCollisionManager().enabled = false;
            this.scheduleOnce(() => {

                this.scheduleOnce(() => {
                    // wave 6 - 2/5
                    var wave6 = cc.instantiate(this.wave_ggInstant[0]);
                    wave6.parent = cc.Canvas.instance.node;
                    this.scheduleOnce(() => {
                        cc.director.getCollisionManager().enabled = true;
                    }, 1)
                }, 0.5)
            }, 1)
        }
    }

    update(dt) {
        //wave 1/5
        // if (this.ship.hp <= 0 && !G.delete) {
        //     G.delete = true;
        //     this.deleteE();
        // }
        if (!this.checkWave2 && G.e_groupBoss >= 6) {
            this.checkWave2 = true;
            this.showWaveTxt(3, 4);
            cc.director.getCollisionManager().enabled = true;
            this.scheduleOnce(() => {

                // wave 3 - 2/3
                this.w3_exchange.startLevel();
                this.scheduleOnce(() => {
                    this.w3_Circle.active = true;
                }, 1.2)
            }, 0.6)
        }

        if (!this.checkWave3 && G.e_cirlce_exchange >= 26) {
            this.checkWave3 = true;
            this.showWaveTxt(3, 5);
            this.scheduleOnce(() => {

                // wave 4 - 3/3
                this.w4_jump.appear(() => { });
                this.scheduleOnce(() => {
                    cc.director.getCollisionManager().enabled = true;
                    G.menu = false;
                }, 1)
            }, 0.8)
        }

        if (!this.check1st && G.e_3circleAll >= 27) {
            this.txtMoveShip.active = false;
            this.check1st = true;
            this.menu.active = true;
            this.logo.opacity = 0;
            this.next[1].active = true;
            G.menu = true;
        }

        if (!this.check5st && G.e_jump >= 24) {
            this.check5st = true;
            this.txtMoveShip.active = false;
            this.menu.active = true;

            this.logo.opacity = 0;
            this.next[1].active = true;
            G.menu = true;
        }

        if (!this.check2st && G.e_triangle >= 32) {
            this.check2st = true;
            this.showWaveTxt(3, 9);
            this.scheduleOnce(() => {
                var wave8 = cc.instantiate(this.wave_ggInstant[1]);
                wave8.parent = cc.Canvas.instance.node;
                wave8.runAction(cc.spawn(cc.fadeIn(0.4), cc.scaleTo(1, 1)));
            }, 1)
        }

        if (!this.check3st && G.e_flower >= 25) {
            this.check3st = true;
            this.showWaveTxt(3, 10);
            this.scheduleOnce(() => {
                cc.director.getCollisionManager().enabled = false;
                this.waveBoss.active = true;

                this.waveBoss.runAction(cc.sequence(cc.moveTo(1.5, cc.v2(0, 220)), cc.callFunc(() => {
                    G.hpBoss = true;
                    cc.director.getCollisionManager().enabled = true;
                })))

            }, 1)
        }

        if (!this.check4st && G.bossDie) {
            cc.audioEngine.stop(this.temp);
            this.bulletRotate.active = false;
            this.check4st = true;
            this.endGame();

            G.hpBoss = false;
            cc.director.getCollisionManager().enabled = false;
            this.menu.active = true;
            this.logo.opacity = 0;
            this.replay.active = true;
            cc.audioEngine.playEffect(G.finalWin, false);
            this.ship.setEnableBullet(false);
            this.ship.setEnableMove(false);
        }
    }

    onTouchStart(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                if (!G.endG && !G.enableMouse) {
                    this.startGame();
                    this.Interaction();
                }
                break;

            case GameState.PLAYING:
                if (!G.endG && !G.enableMouse) {
                    this.txtMoveShip.active = false;
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                    this.hand.stopSwipeAnim();
                    // this.unscheduleAllCallbacks();
                }
                break;
        }
    }

    onTouchMove(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                if (!G.endG && !G.enableMouse) {
                    this.Interaction();
                    this.startGame();
                }
                break;
            case GameState.PLAYING:
                if (!G.endG && !G.enableMouse) {
                    this.txtMoveShip.active = false;
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                    this.hand.stopSwipeAnim();
                    // this.unscheduleAllCallbacks();
                }
                break;
        }

    }

    onTouchEnd(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!G.endG) {
                    this.ship.setEnableBullet(false);
                    if (!G.menu) {
                        this.txtMoveShip.active = true;
                        this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 50)
                    }
                }
                break;
        }
    }

    onTouchCancel(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!G.endG) {
                    this.ship.setEnableBullet(false);
                    if (!G.menu) {
                        this.txtMoveShip.active = true;
                        this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 50);
                    }
                }
                break;
        }
    }

    Interaction() {
        this.bgLayout.node.active = false;;
        if (this.ironsource && !this.checkInteraction) {
            this.checkInteraction = true
            window.NUC.trigger.interaction();
        }
    }

    startGame() {
        if (this.checkMerge) {
            this.gameState = GameState.PLAYING;
            // this.hand.stopSwipeAnim();
            this.ship.setEnableMove(true);
        }
    }

    deleteE() {
        var findDeep = function (node, nodeName) {
            if (node.name == nodeName) return node;

            for (var i = 0; i < node.children.length; i++) {
                var res = findDeep(node.children[i], nodeName);
                if (res) {
                    res.destroy();
                }
                // return res;
            }
        }
        findDeep(cc.director.getScene(), "z33");
        findDeep(cc.director.getScene(), "e_heart");
        findDeep(cc.director.getScene(), "e_spawn");
        findDeep(cc.director.getScene(), "enemy_cb3");
        findDeep(cc.director.getScene(), "enemy_cb4");
        findDeep(cc.director.getScene(), "e_3circle");
        findDeep(cc.director.getScene(), "e_jump");
        findDeep(cc.director.getScene(), "crop1");
        findDeep(cc.director.getScene(), "crop2");
        findDeep(cc.director.getScene(), "crop3");
        findDeep(cc.director.getScene(), "e_triangle");
        findDeep(cc.director.getScene(), "e_flower");
        this.ship.hp = 1;
    }

    endGame() {
        this.hand.node.active = false;
        this.txtMoveShip.active = false;
        this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    }
}