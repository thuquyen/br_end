import G from "./Global";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Label)
    scoreLabel: cc.Label = null;

    @property(cc.Layout)
    bgLayout: cc.Layout = null;

    @property(cc.Label)
    PauseLabel: cc.Label = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.PauseLabel.node.zIndex = 2;
        cc.audioEngine.playMusic(G.bgSound, true);

        this.node.on("touchstart", ()=>{
            if(G.check){
                G.bg = 180;
                G.text = false;
                G.e = true;
            }else{
                G.bg = 0;
                G.text = false;
                G.e = false;
                G.fire = 1000;
            }    
            return;     
        });

        this.node.on("touchend", ()=>{
            if(G.check){
                G.bg = 180;
                G.text = false;
                G.e = true;
            }else{
                G.bg = 100;
                G.text = true;
                G.e = true;
                G.fire = 1;
            }
            return;
        });
    }

    update (dt) {

        
        this.scoreLabel.string = "SCORE: " + G.score.toString();

        this.bgLayout.node.opacity = G.bg;
        this.PauseLabel.node.active = G.text;

        

        

        if(G.check){
            this.scoreLabel.node.active = false;
            G.bg = 180;
            G.text = false;
            G.e = true;
        }
        
    }
    
}
