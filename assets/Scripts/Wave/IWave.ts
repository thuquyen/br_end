
const {ccclass, property} = cc._decorator;

@ccclass
export default abstract class IWave extends cc.Component {

    public onComplete: Function = null;

    public abstract startWave();

}