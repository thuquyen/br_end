import IWave from "./IWave";
import IEnemy from "../Enemy/IEnemy";

const {ccclass, property} = cc._decorator;

@ccclass
export default class BaseWave extends IWave {

    @property([IEnemy])
    public enemies: IEnemy[] = [];

    startWave () {
        let n = this.enemies.length;
        for(let i=0; i<this.enemies.length; i++) {
            this.enemies[i].onDie = ()=>{
                n--;
                if(n==0) {
                    if(this.onComplete)
                        this.onComplete();
                }
            }
        }
    }

}