import Boss1 from "./Boss1";
import EnemySpawnBullet from "./EnemySpawnBullet";

const {ccclass, property} = cc._decorator;

@ccclass
export default class WaveController_Old extends cc.Component {

    @property(Boss1)
    boss: Boss1 = null;

    @property([EnemySpawnBullet])
    enemy: EnemySpawnBullet[] = [];

    anim: cc.Animation;

    // @property(cc.Prefab)
    // eExplosionPrefab: cc.Prefab = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.anim = this.getComponent(cc.Animation);
        for(let i=0; i<this.enemy.length; i++) {
            this.enemy[i].onDie = (this.onEnemyDie.bind(this));
            //this.enemy[i].explosionPool.prefab = this.eExplosionPrefab;
        }
    }

    appear(onDone: Function = null){
        this.anim.play();
        let time = this.anim.currentClip.duration;
        this.scheduleOnce(function(){
            if(onDone)
                onDone();
        }, time);
    }

    startAttack(){
        this.boss.shieldBoss.showShield();
        for(let i=0; i<this.enemy.length; i++) {
            this.enemy[i].startAttack();
        }
        this.boss.onDie = (boss)=>{
            this.onBossDie(boss);
        };
    }

    onEnemyDie(enemy: EnemySpawnBullet){
        if(this.checkAllEnemyDie()) {
            this.boss.startAttack();
        }
    }

    onBossDie(boss: Boss1){
        this.onFinishWave();
    }

    checkAllEnemyDie(){
        for(let i=0; i<this.enemy.length; i++) {
            if(!this.enemy[i].isDie()){
                return false;
            }
        }
        return true;
    }

    _onFinishWave: Function;
    setOnFinishWave(callback: Function){
        this._onFinishWave = callback;
    }

    onFinishWave(){
        if(this._onFinishWave) {
            this._onFinishWave();
        }
    }

}