


const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property()
    delay: number = 0;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.node.opacity = 0;
    }

    start () {
        this.callHand();
    }

    callHand(){
        this.schedule(() => this.handBlink(), 5, cc.macro.REPEAT_FOREVER, this.delay);
    }

    handBlink(){
        this.node.opacity = 255;
        this.scheduleOnce(() => {
            this.node.opacity = 0;
        }, 1.6);
    }

    // update (dt) {}
}
