import SequencePoints from "./SequencePoints";
import { Helper } from "./Helper";
import BaseEnemy from "./Enemy/BaseEnemy";

const {ccclass, property} = cc._decorator;

@ccclass('PathInfo')
export default class PathInfo {

    @property(SequencePoints)
    public splinePath: SequencePoints = null;
    @property(cc.Prefab)
    public prefab: cc.Prefab = null;
    @property(Number)
    public delay: number = 0;
    @property(Number)
    public speed: number = 200;
    @property(Number)
    public angleOffset: number = 90;
    @property(Boolean)
    public isRotate: boolean = true;
    public enemy: BaseEnemy = null;

    appear(node: cc.Node, onDone: Function): BaseEnemy{
        var e = cc.instantiate(this.prefab);
        e.angle = node.angle;
        e.parent = node;
        if(this.isRotate) {
            Helper.splineFollowAndRotate2(e, this.splinePath.points, this.speed, this.delay, this.angleOffset, onDone);
        } else {
            Helper.splineFollow(e, this.splinePath.points, this.speed, this.delay, this.angleOffset, onDone);
        }
        this.enemy = e.getComponent(BaseEnemy);
        this.enemy.setOnDie(()=>{
            if(this.onComplete)
                this.onComplete();
        });
        return this.enemy;
    }

    onComplete: Function;

}