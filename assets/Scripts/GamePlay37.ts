import WaveQuangCao from "./Wave/WaveQuangCao";
import ShipController from "./Ship36";
import HandController from "./HandController";
import { Helper } from "./Helper";
import G from "./Global";

const { ccclass, property } = cc._decorator;

let GameState = {
    INIT: 0,
    READY: 1,
    PLAYING: 2,
    ENDGAME: 3
}

declare const window: any;

@ccclass
export default class SpaceShooterAd2 extends cc.Component {

    gameState: number = GameState.INIT;
    @property(cc.Node)
    logo: cc.Node = null;

    @property(cc.Node)
    txtMoveShip: cc.Node = null;

    @property(cc.Node)
    wave: cc.Node[] = [];

    @property(HandController)
    hand: HandController = null;

    @property(cc.Layout)
    bgLayout: cc.Layout = null;

    // @property(cc.Node)
    // buttonInstall: cc.Node = null;

    @property(cc.Node)
    titleMini: cc.Node = null;

    index: number = 1;

    nShip: number = 0;

    @property(ShipController)
    ship: ShipController = null;
    istouch: boolean = true;

    ironsource: boolean = false;
    mindworks: boolean = true;

    // @property({ type: cc.AudioClip })
    // bgSound: cc.AudioClip = null;
    // temp: number = 0;

    check1st: boolean = false;
    check2st: boolean = false;
    check3st: boolean = false;

    nextBoss: number = 0;
    doneChoose: boolean = false;
    // @property(cc.Node)
    textFeatured: cc.Node = null;

    firstTouch: boolean = false;
    posX: number;
    posY: number;
    checkEnd: boolean = false;

    @property(cc.Node)
    tempBoss: cc.Node = null;
    isItemVip: boolean = false;
    ready: boolean = false;

    @property(cc.Node)
    cardEvo: cc.Node = null

    @property(cc.Node)
    card: cc.Node[] = []
    @property(cc.Node)
    questionMark: cc.Node[] = []
    @property(cc.Node)
    boder: cc.Node[] = []

    @property(cc.Node)
    question: cc.Node[] = []

    @property(cc.Node)
    hand_choose: cc.Node = null;
    @property(cc.Node)
    flare: cc.Node[] = []
    @property(cc.Node)
    btn_card: cc.Node[] = []

    checkCard_1: boolean = false;
    checkCard_2: boolean = false;
    checkCard_3: boolean = false;
    checkCard_4: boolean = false;

    countChoose: number = 0;
    chooseDone: boolean = false;

    checkMerge: boolean = false;

    @property(sp.SkeletonData)
    listShip: sp.SkeletonData[] = [];
    checkInteraction: boolean = false;

    @property(cc.Node)
    btn_Con: cc.Node = null;

    onLoad() {
        cc.view.enableAutoFullScreen(false);
        cc.director.getCollisionManager().enabled = false;

        this.btn_Con.zIndex = 200;
        this.txtMoveShip.zIndex = 100;
        this.ship.node.zIndex = 1;
        // this.bgLayout.node.zIndex = 2;

        this.scheduleOnce(() => {
            for (var i = 0; i < 4; i++) {
                this.btn_card[i].getComponent(cc.Button).interactable = true;
            }
        }, 1.35)
    }

    open(boderNumber: number, cardNumber: number, questionMarkNum: number) {
        this.cardEvo.getComponent(cc.Animation).stop();
        this.hand_choose.destroy();
        for (var i = 0; i < 4; i++) {
            this.flare[i].destroy();
        }
        this.boder[boderNumber].active = true;
        this.card[cardNumber].getComponent(cc.Animation).play();
        this.questionMark[questionMarkNum].getComponent(cc.Animation).play();
    }

    openCard1() {
        this.Interaction();
        if (!this.checkCard_1 && (this.countChoose < 1)) {
            this.chooseDone = true;
            this.countChoose++;
            this.checkCard_1 = true;
            for (var i = 0; i < 4; i++) {
                this.question[i].destroy();
            }
            this.open(0, 0, 0);
            this.scheduleOnce(() => {
                this.open(0, 1, 1);
                this.open(0, 2, 2);
                this.open(0, 3, 3);
            }, 0.7)
            this.ship.node.scale = 0.8;
            this.ship.node.getComponent(sp.Skeleton).skeletonData = this.listShip[0];
            this.ship.node.getComponent(sp.Skeleton).setAnimation(1, 'attack', true)
            this.appearShip();
        }
    }

    openCard2() {
        this.Interaction();
        if (!this.checkCard_2 && (this.countChoose < 1)) {
            this.chooseDone = true;
            this.countChoose++;
            this.checkCard_2 = true;
            for (var i = 0; i < 4; i++) {
                this.question[i].destroy();
            }
            this.open(1, 1, 1);
            this.scheduleOnce(() => {
                this.open(1, 0, 0);
                this.open(1, 2, 2);
                this.open(1, 3, 3);
            }, 0.7)
            this.ship.node.scale = 0.82;
            this.ship.node.getComponent(sp.Skeleton).skeletonData = this.listShip[1];
            this.ship.node.getComponent(sp.Skeleton).setAnimation(1, 'attack', true)
            this.appearShip();
        }
    }

    openCard3() {//black dragon
        this.Interaction();
        if (!this.checkCard_3 && (this.countChoose < 1)) {
            this.chooseDone = true;
            this.countChoose++;
            this.checkCard_3 = true;
            for (var i = 0; i < 4; i++) {
                this.question[i].destroy();
            }
            this.open(2, 2, 2);
            this.scheduleOnce(() => {
                this.open(2, 0, 0);
                this.open(2, 1, 1);
                this.open(2, 3, 3);
            }, 0.7)
            this.ship.node.scale = 0.75;
            this.ship.node.getComponent(sp.Skeleton).skeletonData = this.listShip[2];
            this.ship.node.getComponent(sp.Skeleton).setAnimation(1, 'attack', true);
            this.appearShip();
        }
    }

    openCard4() {//black dragon
        this.Interaction();
        if (!this.checkCard_4 && (this.countChoose < 1)) {
            this.chooseDone = true;
            this.countChoose++;
            this.checkCard_4 = true;
            for (var i = 0; i < 4; i++) {
                this.question[i].destroy();
            }
            this.open(3, 3, 3);
            this.scheduleOnce(() => {
                this.open(3, 0, 0);
                this.open(3, 1, 1);
                this.open(3, 2, 2);
            }, 0.7)
            this.ship.node.scale = 0.98;
            this.ship.node.getComponent(sp.Skeleton).skeletonData = this.listShip[3];
            this.ship.node.getComponent(sp.Skeleton).setAnimation(1, 'attack', true);
            this.appearShip();
        }
    }

    appearShip() {
        this.scheduleOnce(() => {
            this.checkMerge = true;
            this.cardEvo.active = false;
            this.scheduleOnce(() => {
                G.startSpawnItem = true;
            }, 0.5)
            this.wave[0].active = true;
            this.ship.node.runAction(cc.sequence(cc.moveTo(0.8, cc.v2(0, -280)).easing(cc.easeBackOut()), cc.callFunc(() => {
                this.node.getComponent("CanvasTouchManager").enabled = true;
                this.txtMoveShip.active = true;
                this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
                this.hand.node.opacity = 255;
                this.hand.node.zIndex = 12;
                this.posX = this.ship.node.x;
                this.posY = this.ship.node.y;
                this.gameState = GameState.READY;
                this.startGame();
                cc.director.getCollisionManager().enabled = true;
            })))
            // this.turnOnGuide();
        }, 2)
        // this.buttonInstall.active = true;
    }

    start() {
        // this.temp = cc.audioEngine.play(this.bgSound, true, 1);
        this.gameState = GameState.INIT;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);

        if (this.ironsource) {
            window.NUC.trigger.ready("ready")
        }

        // auto after 6s
        this.scheduleOnce(() => {
            if (!this.chooseDone) {
                this.openCard3()
            }
        }, 7)
    }

    turnOnGuide() {
        this.txtMoveShip.active = true;
        this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
        this.hand.node.opacity = 255;
        this.hand.node.zIndex = 12;
    }

    onTouchStart(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                this.startGame();
                this.Interaction();
                break;
            case GameState.PLAYING:
                if (!G.endG && !G.isBoss && !G.enableMouse) {
                    this.txtMoveShip.active = false;
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                    this.hand.stopSwipeAnim();
                    // this.unscheduleAllCallbacks();
                }
                break;
        }
    }

    onTouchMove(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                this.Interaction();
                this.startGame();
                break;
            case GameState.PLAYING:
                if (!G.endG && !G.isBoss && !G.enableMouse) {
                    this.txtMoveShip.active = false;
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                    this.hand.stopSwipeAnim();
                    // this.unscheduleAllCallbacks();
                }
                break;
        }

    }

    onTouchEnd(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!G.endG && !G.isBoss) {
                    this.ship.setEnableBullet(false);
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
                }
                break;
        }
    }

    onTouchCancel(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!G.endG && !G.isBoss) {
                    this.ship.setEnableBullet(false);
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
                    // console.log('cancel')
                }
                break;
        }
    }

    Interaction() {
        this.bgLayout.node.active = false;;
        if (this.ironsource && !this.checkInteraction) {
            this.checkInteraction = true
            window.NUC.trigger.interaction();
        }
        // cc.audioEngine.playEffect(G.click, false);
    }

    startGame() {
        if (this.checkMerge) {
            this.gameState = GameState.PLAYING;
            this.hand.stopSwipeAnim();
            this.ship.setEnableMove(true);
        }
    }

    update(dt) {
        if (!this.check1st && G.totalE >= 27) {
            this.check1st = true;
            this.ship.setEnableBullet(false);
            cc.director.getCollisionManager().enabled = false;
            G.speedBg = -550;
            G.isBoss = true;
            this.wave[1].active = true;

            this.scheduleOnce(() => {
                cc.director.getCollisionManager().enabled = true;
                G.speedBg = -80;
                G.isBoss = false;
                this.ship.setEnableBullet(true);
            }, 1.2)
        }
        if (!this.check2st && G.totalE >= 59) {
            this.check2st = true;
            cc.director.getCollisionManager().enabled = false;
            G.speedBg = -550;
            G.isBoss = true;
            G.endG = true
            this.ship.setEnableBullet(false);
            this.ship.node.runAction(cc.moveTo(0.5, cc.v2(0, -280)))
            this.ship.setEnableMove(false);
            this.scheduleOnce(() => {
                this.tempBoss.active = true;
                G.speedBg = -80;
                //37 spaceshooter
                // this.tempBoss.runAction(cc.sequence(cc.moveTo(1, cc.v2(0, 150)),
                //     cc.callFunc(() => {
                //         G.hpBoss = true;
                //         this.btn_Con.active = true;
                //         this.endGame();
                //     })
                // ));
                //37 spacehunter
                this.scheduleOnce(() => {
                    this.btn_Con.active = true;
                    this.endGame();
                },1)
            }, 1.2)
        }
    }

    endGame() {
        // cc.audioEngine.stop(this.temp);
        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        var findDeep = function (node, nodeName) {
            if (node.name == nodeName) return node;
            for (var i = 0; i < node.children.length; i++) {
                var res = findDeep(node.children[i], nodeName);
                if (res) {
                    res.destroy();
                }
                // return res;
            }
        }
        findDeep(cc.director.getScene(), "item_Upgrade");
        findDeep(cc.director.getScene(), "Bullet_medusa_3");
        findDeep(cc.director.getScene(), "Bullet_medusa_4");

        G.text = false;
        this.checkEnd = true;

        if (this.ironsource) {
            window.NUC.trigger.endGame('win')
        }

        // if (this.buttonInstall)
        //     this.buttonInstall.active = false;

        this.hand.node.active = false;
        this.txtMoveShip.active = false;
        this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    }
}