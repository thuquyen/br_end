import G from "./Global";

const {ccclass, property} = cc._decorator;

//check dependency
//check method design
//check property editor
@ccclass
export default class HandController extends cc.Component {

    // @property(cc.Layout)
    // bgLayout: cc.Layout = null;

    public runSwipeAnim(x0: number, y0: number){
        this.node.active = true;
        this.node.x = x0;
        this.node.y = y0;
        this.node.opacity = 0;
        this.node.runAction(cc.sequence(
            cc.moveBy(0.5, 100, 0).easing(cc.easeSineOut()),
            cc.moveBy(0.5, -100, 0).easing(cc.easeSineIn()),
            cc.moveBy(0.5, -100, 0).easing(cc.easeSineOut()),
            cc.moveBy(0.5, 100, 0).easing(cc.easeSineIn())
        ).repeatForever());
        this.node.runAction(cc.fadeIn(0.3));
    }

    public stopSwipeAnim(){
        if(this.node.active) {
            this.node.stopAllActions();
            this.node.active = false;    
        }
    }

}