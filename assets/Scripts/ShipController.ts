import G from "./Global";
import HPBar from "./HpBar";
import BasePool from "./common/BasePool";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ShipController extends cc.Component {

    @property()
    private enableMove: boolean = true;
    @property()
    private enableBullet: boolean = true;
    private enableProtect: boolean = false;

    @property(BasePool)
    private bulletPool: BasePool = null;

    @property()
    private fireRate = 200;

    private fireTime = 0;

    bulletShip: cc.Prefab = null;

    @property(cc.Prefab)
    private efxPrefab: cc.Prefab = null;

    @property(cc.Vec2)
    private startPoint: cc.Vec2 = null;

    @property()
    private minY = -500;

    // @property(cc.Node)
    // tim: cc.Node = null;

    public onDie: Function = null;
    public onRevive: Function = null;
    public isDie: boolean = false;

    @property()
    private HPMAX = 1;
    private hp = 0;
    // @property(HPBar)
    public hpBar: HPBar = null;

    @property(Boolean)
    hackNoDie: boolean = false;

    // @property(cc.Prefab)
    // bullet11: cc.Prefab[] = [];

    // @property(cc.Prefab)
    // bullet14: cc.Prefab[] = [];

    @property(cc.Prefab)
    bullet16: cc.Prefab[] = [];

    bullet: cc.Prefab[] = [];

    // @property(cc.Prefab)
    // fxItem: cc.Prefab = null;

    // @property(sp.SkeletonData)
    // changeShip: sp.SkeletonData[] = [];

    @property(cc.Node)
    shieldEffect: cc.Node = null;
    // @property(cc.Node)
    // bgShip: cc.Node = null;

    onLoad() {
        this.bulletPool.prefab = this.bullet16[0];
        this.bullet = this.bullet16;
        this.node.zIndex = 1;
        this.hp = this.HPMAX;
        G.ship = this;
    }

    setEnableMove(enable: boolean) {
        this.enableMove = enable;
    }

    setEnableBullet(enable: boolean) {
        this.enableBullet = enable;
    }

    setEnableProtect(enable: boolean) {
        this.enableProtect = enable;
    }

    update(dt) {
        if (this.enableBullet) {
            this.updateBullet();
        }

        if (this.enableMove) {
            this.updateMove();
        }

    }

    lerp(a: number, b: number, r: number) {
        return a + (b - a) * r;
    }

    private updateMove() {
        if (G.touchPos != null) {
            this.node.x = this.lerp(this.node.x, G.touchPos.x, 0.3);
            this.node.y = this.lerp(this.node.y, G.touchPos.y + 20, 0.3);
            if (this.node.y < this.minY) this.node.y = this.minY;
        }
    }

    private updateBullet() {
        if (Date.now() > this.fireTime + this.fireRate) {
            this.fireTime = Date.now();
            this.fireBullet();
        }
    }

    private fireBullet() {
        cc.audioEngine.playEffect(G.enemyShoot, false);
        var newBulletShip = this.bulletPool.createObject(cc.Canvas.instance.node);
        newBulletShip.x = this.node.x;
        newBulletShip.y = this.node.y + 30;
    }

    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        if (other.node.group == "item") {
            if (other.node.name == 'item_Upgrade')
                this.itemUpgrade(other);

            // if (other.node.name == 'ItemShip11') {
            //     this.itemChangeShip11(other);
            //     this.bullet = this.bullet11;
            // }

            // if (other.node.name == 'ItemShip14') {
            //     this.itemChangeShip14(other);
            //     this.bullet = this.bullet14;
            // }

            if (other.node.name == 'ItemShip16') {
                // this.itemChangeShip16(other);
                this.bullet = this.bullet16;
            }

            this.changeBulletShip(this.bullet);
        }

        if (this.enableProtect)
            return;
        if (other.node.group == 'boss') {
            this.onHitBoss(other.node);
        }
        else if (other.node.group == 'bossBullet') {
            if (G.hitBullet != null)
                cc.audioEngine.playEffect(G.hitBullet, false);
            this.onHitBossBullet(other.node);
        }
        else if (other.node.group == 'e' || other.node.group == 'eSpecial') {
            if (G.hitBullet != null)
                cc.audioEngine.playEffect(G.hitBullet, false);
            this.onHitEnemy(other.node);
        }
        else if (other.node.group == 'eBullet') {
            this.onHitEnemyBullet(other.node);
        }
    }

    // itemChangeShip11(other: cc.Collider) {
    //     this.node.getComponent(sp.Skeleton).skeletonData = this.changeShip[0];
    //     this.node.getComponent(sp.Skeleton).setAnimation(0, 'fly', true);
    //     other.node.destroy();
    // }

    // itemChangeShip14(other: cc.Collider) {
    //     this.node.getComponent(sp.Skeleton).skeletonData = this.changeShip[1];
    //     this.node.getComponent(sp.Skeleton).setAnimation(0, 'fly', true);
    //     other.node.destroy();
    // }

    // itemChangeShip16(other: cc.Collider) {
    //     this.node.getComponent(sp.Skeleton).skeletonData = this.changeShip[2];
    //     this.node.getComponent(sp.Skeleton).setAnimation(0, 'fly', true);
    //     other.node.destroy();
    // }

    itemUpgrade(other: cc.Collider) {
        G.bulletUpgrade++;
        G.score += 10;
        other.node.destroy();
        // this.spawnFxItem();
    }

    private onHitBoss(node: cc.Node) {
        this.loseHp(1);
    }

    private onHitBossBullet(node: cc.Node) {
        node.destroy();
        this.loseHp(1);
    }

    private onHitEnemy(node: cc.Node) {
        this.loseHp(1);
    }

    private onHitEnemyBullet(node: cc.Node) {
        node.destroy();
        this.loseHp(1);
    }


    private loseHp(n: number) {
        G.bulletDie = true;
        G.lazeShip = false;
        this.hp -= n;
        if (this.hpBar) {
            this.hpBar.setPercent(this.hp / this.HPMAX);
        }
        this.showLife(this.hp);
        // if (this.hp <= 0) {
        //     if (this.hackNoDie) {
        //         this.hp = this.HPMAX;
        //         this.hpBar.setPercent(this.hp / this.HPMAX);
        //     } else {
        //         this.hp = 0;
        //         this.onShipDie();
        //     }
        // }
    }

    private onShipDie() {
        this.isDie = true;
        this.explosion();
        if (this.onDie) {
            this.onDie();
        }
    }

    showLife(n: number) {
        this.explosion();
        this.node.opacity = 255;
        // if (this.tim.children[n])
        //     this.tim.children[n].active = false;
        this.node.setPosition(cc.v2(0, -598));
        this.shieldEffect.opacity = 255;
        // this.shieldEffect.runAction(cc.spawn(cc.fadeIn(0.2), cc.scaleTo(0.2, 1)));
        this.enableProtect = true;
        G.touchPos = null;
        this.node.runAction(cc.sequence(cc.moveTo(0.3, cc.v2(0, -293)).easing(cc.easeQuarticActionIn()), cc.callFunc(() => {
            this.getComponent(cc.CircleCollider).enabled = true;
            this.scheduleOnce(() => {
                this.enableProtect = false;
                // this.shieldEffect.runAction(cc.spawn(cc.fadeOut(0.3), cc.scaleTo(0.3, 0)));
                this.shieldEffect.opacity = 0;
            }, 0.5)
            // this.enableBullet = true;
            G.touchPos = null;
            this.enableMove = true;
            G.bulletDie = false;
        })));
    }

    private explosion() {
        var efx = cc.instantiate(this.efxPrefab);
        efx.parent = this.node.parent;
        efx.x = this.node.x;
        efx.y = this.node.y;
        this.node.opacity = 0;
        this.enableBullet = false;
        this.enableMove = false;
        this.getComponent(cc.CircleCollider).enabled = false;
    }

    public revive() {
        this.enableProtect = true;
        G.touchPos = null;
        this.node.setPosition(this.startPoint);
        this.node.opacity = 255;
        this.enableBullet = true;
        this.enableMove = true;
        this.getComponent(cc.CircleCollider).enabled = true;
        this.isDie = false;
        this.scheduleOnce(() => {
            this.enableProtect = false;
        }, 1);
        this.hp = this.HPMAX;
        if (this.onRevive) {
            this.onRevive();
        }
    }

    flyOut(done: Function = null) {
        G.checkWing = false;
        G.touchPos = null;
        this.enableMove = false;
        this.enableProtect = true;
        this.enableBullet = false;
        this.node.runAction(cc.sequence(cc.spawn(
            cc.moveBy(1.1, 0, 2000).easing(cc.easeBackIn()), cc.fadeOut(1.1)),
            cc.callFunc(() => {
                this.enableMove = false;
                G.touchPos = null;
                if (done)
                    done();
            })
        ));
    }

    changeBulletShip(prefabs) {
        switch (G.bulletUpgrade) {
            case 1: {
                this.bulletPool.prefab = prefabs[0];
                break;
            }

            case 2: {
                this.bulletPool.prefab = prefabs[1];
                break;
            }

            case 3: {
                this.bulletPool.prefab = prefabs[2];
                G.checkWing = true;
                G.stateItem = false;
                // G.lazeShip = true;
                break;
            }

            default: {
                this.bulletPool.prefab = prefabs[2];
                break;
            }
        }
    }

    // spawnFxItem() {
    //     var newFxItem = cc.instantiate(this.fxItem);
    //     newFxItem.parent = cc.Canvas.instance.node;
    //     newFxItem.x = this.node.x;
    //     newFxItem.y = this.node.y;
    // }

}