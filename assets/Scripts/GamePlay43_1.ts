import WaveQuangCao from "./Wave/WaveQuangCao";
import ShipController from "./Ship31";
import HandController from "./HandController";
import { Helper } from "./Helper";
import G from "./Global";

const { ccclass, property } = cc._decorator;

let GameState = {
    INIT: 0,
    READY: 1,
    PLAYING: 2,
    ENDGAME: 3
}

declare const window: any;

@ccclass
export default class SpaceShooterAd2 extends cc.Component {

    gameState: number = GameState.INIT;

    @property(cc.Node)
    endG: cc.Node = null;

    @property(cc.Node)
    txtMoveShip: cc.Node = null;

    @property(cc.Node)
    wave: cc.Node[] = []

    @property(HandController)
    hand: HandController = null;

    // @property(cc.Node)
    // buttonInstall: cc.Node = null;

    @property(cc.Node)
    titleMini: cc.Node = null;

    index: number = 1;

    nShip: number = 0;

    @property(ShipController)
    ship: ShipController = null;
    istouch: boolean = true;

    ironsource: boolean = false;

    // @property({ type: cc.AudioClip })
    // bgSound: cc.AudioClip = null;
    // temp: number = 0;

    check1st: boolean = false;
    check2st: boolean = false;
    check3st: boolean = false;

    nextBoss: number = 0;
    doneChoose: boolean = false;
    // @property(cc.Node)
    textFeatured: cc.Node = null;

    firstTouch: boolean = false;
    posX: number;
    posY: number;
    checkEnd: boolean = false;

    @property(cc.Node)
    effectShip: cc.Node = null;

    @property(cc.Node)
    tempBoss: cc.Node = null;
    isItemVip: boolean = false;
    ready: boolean = false;

    @property(cc.Node)
    menu: cc.Node = null;

    check_play: boolean = false;

    onLoad() {
        this.txtMoveShip.zIndex = 100;
        cc.view.enableAutoFullScreen(false);
        cc.director.getCollisionManager().enabled = true;
        this.ship.node.zIndex = 1;
        this.endG.zIndex = 3;
    }

    btnPlay() {
        this.check_play = true;
        this.menu.destroy();
        this.appearShip();
    }

    appearShip() {
        // this.buttonInstall.active=true;
        this.wave[0].active = true;
        this.wave[1].active = true;
        this.ship.node.runAction(cc.sequence(cc.moveTo(0.8, cc.v2(0, -290)).easing(cc.easeBackOut()), cc.callFunc(() => {
            this.ship.setEnableBullet(true);
            G.startSpawnItem = true;
            this.ship.tranform1();
            this.effectShip.active = false;
            this.posX = this.ship.node.x;
            this.posY = this.ship.node.y;
            this.node.getComponent("CanvasTouchManager").enabled = true;
            this.gameState = GameState.READY;
        })))
        this.scheduleOnce(() => {
            this.turnOnGuide();
        }, 2.5)
    }

    start() {
        // this.temp = cc.audioEngine.play(this.bgSound, true, 1);
        this.scheduleOnce(() => {
            if (!this.check_play) {
                this.check_play = true;
                this.btnPlay();
            }
        }, 5.5)
    
        this.gameState = GameState.INIT;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
        if (this.ironsource) {
            window.NUC.trigger.ready("ready")
        }
    }

    turnOnGuide() {
        this.txtMoveShip.active = true;
        this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
        this.hand.node.zIndex = 12;
    }

    endGame() {
        // this.buttonInstall.active=false;
        // cc.audioEngine.stop(this.temp);
        var findDeep = function (node, nodeName) {
            if (node.name == nodeName) return node;
            for (var i = 0; i < node.children.length; i++) {
                var res = findDeep(node.children[i], nodeName);
                if (res)
                    res.destroy();
                // return res;
            }
        }
        findDeep(cc.director.getScene(), "item_Upgrade");

        this.ship.node.runAction(cc.sequence(
            cc.moveBy(1.1, 0, 2000).easing(cc.easeBackIn()),
            cc.callFunc(() => {
                findDeep(cc.director.getScene(), "ship");
                findDeep(cc.director.getScene(), "Bullet_ship22new");
                findDeep(cc.director.getScene(), "Bullet1_ship22new");
                findDeep(cc.director.getScene(), "Bullet2_ship22new");
                findDeep(cc.director.getScene(), "Bullet3_ship22new");
                findDeep(cc.director.getScene(), "BulletRainbow");
            })
        )
        )
        G.text = false;
        this.checkEnd = true;

        if (this.ironsource) {
            window.NUC.trigger.endGame('win')
        }
        this.scheduleOnce(() => {
            this.endG.active = true;
        }, 2)

        // if (this.buttonInstall)
        //     this.buttonInstall.active = false;
        // this.titleMini.active = false;

        this.hand.node.active = false;
        this.txtMoveShip.active = false;
        this.ship.setEnableMove(false);
        this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    }

    onTouchStart(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                G.clickStart = true;
                this.startGame();
                break;
            case GameState.PLAYING:
                this.txtMoveShip.active = false;
                this.hand.stopSwipeAnim();
                if (this.check_play) {
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                    this.wave[0].active = true;
                    this.wave[1].active = true;
                }
                break;
        }
    }

    onTouchMove(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                G.clickStart = true;
                this.startGame();
                break;
            case GameState.PLAYING:
                this.txtMoveShip.active = false;
                this.hand.stopSwipeAnim();
                if (this.check_play) {
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                    this.wave[0].active = true;
                    this.wave[1].active = true;
                }
                break;
        }
    }

    onTouchEnd(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!this.ship.isDie && this.check_play) {
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
                    this.ship.setEnableBullet(false);
                }
                break;
        }
    }

    onTouchCancel(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!this.ship.isDie && this.check_play) {
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
                    this.ship.setEnableBullet(false);
                }
                break;
        }
    }

    startGame() {
        if (this.ironsource)
            window.NUC.trigger.interaction();
        this.gameState = GameState.PLAYING;
        this.hand.stopSwipeAnim();
        this.ship.setEnableMove(true);

    }

    clickIns1() {
        if (this.ironsource) {
            window.NUC.trigger.convert()
            window.NUC.event.send('clickIns', 'btnInsStart')
        }
    }
    clickIns2() {
        if (this.ironsource) {
            window.NUC.trigger.convert()
            window.NUC.event.send('clickIns', 'btnEnd')
        }
    }

    update(dt) {

        if (G.totalE >= 13 && !this.check2st) {
            this.check2st = true;
            G.speedBg = -300;

            G.isBoss = true;
            this.scheduleOnce(() => {
                // G.speedBg = -100;
                this.tempBoss.active = true;
                this.tempBoss.runAction(cc.sequence(cc.moveTo(1, cc.v2(0, 270)),
                    cc.callFunc(() => {
                        G.hpBoss = true;
                        G.isBoss = false;
                        this.txtMoveShip.active = true;
                        this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);;
                    })
                ));
                this.ship.setEnableBullet(true);
            }, 1.2)
            this.ship.setEnableBullet(false);
        }
        if (!G.endG && G.bossDie) {
            G.endG = true;
            this.endGame();
        }
        if (this.ship.startFire4 && !this.isItemVip) {
            this.isItemVip = true;
            this.scheduleOnce(() => {
                this.txtMoveShip.active = true;
                this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
            }, 0.3)
        }
    }
}