
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    strAnimCurrent: string = '';
    @property()
    timeLoop: number = 0;
    @property()
    timeWait: number = 0;
    @property(cc.Node)
    skill: cc.Node = null;
    onLoad() {
        this.getComponent(sp.Skeleton).setMix('idle_appear', 'appear', 0.1);
        this.getComponent(sp.Skeleton).setMix('appear', 'idle', 0.1);
        this.getComponent(sp.Skeleton).setMix('idle', 'attack', 0.1);
        this.getComponent(sp.Skeleton).setMix('idle', 'attack', 0.1);
        this.skill.getComponent(sp.Skeleton).setMix('1', '2', 0.1);
        this.skill.getComponent(sp.Skeleton).setMix('2', '1', 0.1);
    }

    start() {
        this.node.runAction(cc.sequence(cc.moveTo(1, cc.v2(0, 170)).easing(cc.easeBackOut()),
            cc.callFunc(() => {
                this.scheduleOnce(() => {
                    this.changeAnimation(1, 'appear', true);
                    this.scheduleOnce(() => {
                        this.changeAnimation(1, 'idle', true);
                        this.schedule(() => {
                            this.skill.opacity = 255;
                            this.skill.getComponent(sp.Skeleton).setAnimation(1, '1', true);
                            this.changeAnimation(1, 'attack', true);
                            this.skill.getComponent(sp.Skeleton).setAnimation(1, '2', true);
                            this.scheduleOnce(() => {
                                this.changeAnimation(1, 'idle', true);
                                this.skill.opacity = 0;
                            }, 1.3)
                        }, this.timeLoop, cc.macro.REPEAT_FOREVER, this.timeWait)
                    }, 0.15)
                }, 0.2)

            })))


    }

    changeAnimation(track: number, str: string, bool: boolean) {
        if (str === this.strAnimCurrent) {
            return;
        }
        this.getComponent(sp.Skeleton).setAnimation(track, str, bool);
        this.strAnimCurrent = str;
    }
}
