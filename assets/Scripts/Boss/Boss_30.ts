import G from "../Global";
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property()
    interval: number = 0;

    @property()
    delay: number = 0;

    @property()
    iBullet: number = 0;

    @property(cc.Prefab)
    bullet: cc.Prefab[] = [];

    strAnimCurrent: string = '';

    private skillBoss: cc.Prefab = null;

    @property(cc.Node)
    bullet_lazer: cc.Node = null;


    // onLoad () {}

    start() {
        this.node.parent.runAction(cc.sequence(cc.moveTo(1.5, cc.v2(0, 0)), cc.callFunc(() => {
            G.hpBoss = true;
            cc.director.getCollisionManager().enabled = true;
        })))
        this.schedule(() => {
            let x = (Math.random() * 2 - 1) * 50;
            let y = (Math.random() * 2 - 1) * 50;
            this.node.runAction(cc.sequence(cc.moveTo(1, cc.v2(x, 188 + y)), cc.callFunc(() => {
                // this.bulletBoss();
                // if (G.hpBoss)
                // this.bulletBoss();
            })));
        }, 1, cc.macro.REPEAT_FOREVER,2)

        this.getComponent(sp.Skeleton).setMix('idle', 'skill6_1', 0.1);
        this.getComponent(sp.Skeleton).setMix('skill6_1', 'skill4_1', 0.1);
        this.getComponent(sp.Skeleton).setMix('skill4_1', 'skill4_2', 0.1);
        this.getComponent(sp.Skeleton).setMix('skill4_2', 'skill6_3', 0.1);
        this.getComponent(sp.Skeleton).setMix('skill6_3', 'idle', 0.1);
        this.getComponent(sp.Skeleton).setMix('skill6_1', 'skill6_2', 0.1);
        this.getComponent(sp.Skeleton).setMix('skill6_2', 'skill6_3', 0.1);


            this.schedule(() => {
                if (G.hpBoss) {
                    this.bulletBoss();
                }
            }, this.interval, cc.macro.REPEAT_FOREVER, this.delay);

        let spine = this.node.getComponent(sp.Skeleton);
        spine.setCompleteListener((trackEntry, loopCount) => {
            let name = trackEntry.animation ? trackEntry.animation.name : '';
            if (name === 'skill6_3') {
                this.changeAnimation(1, 'idle', true);
            }

            // if (name === 'skill6_1' && this.iBullet == 3) {
            //     this.changeAnimation(1, 'skill4_1', true);
            // }

            if (name === 'skill4_1') {
                this.bullet_lazer.active = true;
                this.bullet_lazer.getComponent(cc.Animation).play();

                this.changeAnimation(1, 'skill4_2', true);

                this.scheduleOnce(() => {
                    this.changeAnimation(1, 'idle', true);
                    this.bullet_lazer.active = false;
                    this.bullet_lazer.getComponent(cc.Animation).pause();
                }, 2.5)
            }
        });

        this.changeAnimation(1, 'idle', true);
    }

    update(dt) {
        this.bullet_lazer.x = this.node.x;
        this.bullet_lazer.y = this.node.y;
    }

    changeAnimation(track: number, str: string, bool: boolean) {
        if (str === this.strAnimCurrent) {
            return;
        }
        this.getComponent(sp.Skeleton).setAnimation(track, str, bool);
        this.strAnimCurrent = str;
    }

    public bulletBoss() {

        this.skill1();

        this.iBullet++;
        this.skill2();

        if (this.iBullet == this.bullet.length) {
            this.iBullet = 1;
        }
    }
    skill1() {
        if (this.iBullet === 1) {
            this.changeAnimation(1, 'skill6_1', false);

            this.scheduleOnce(() => {
                this.instant_bullet(2);
                this.scheduleOnce(() => {
                    this.instant_bullet(2);
                }, 0.3)
            }, 0.15)
            this.scheduleOnce(() => {
                this.changeAnimation(1, 'skill6_2', true);
                this.instant_bullet(0)
                this.scheduleOnce(() => {
                    this.instant_bullet(1);

                    this.scheduleOnce(() => {
                        this.instant_bullet(0);

                        this.scheduleOnce(() => {
                            this.instant_bullet(1)
                            this.changeAnimation(1, 'skill6_3', false);
                        }, 0.5)
                    }, 0.42)
                }, 0.42)
            }, 1.4);
        }
        else if (this.iBullet === 2) {
            this.changeAnimation(1, 'idle', true);
        }
    }

    instant_bullet(iBullet: number) {
        this.skillBoss = this.bullet[iBullet];
        var newBullet = cc.instantiate(this.skillBoss);
        newBullet.parent = cc.Canvas.instance.node;
        newBullet.x = this.node.x;
        newBullet.y = this.node.y;
    }

    skill2() {
        if (this.iBullet == 3) {
            this.changeAnimation(1, 'skill6_1', false);
            this.scheduleOnce(() => {
                this.changeAnimation(1, 'skill4_1', true);
            }, 0.8)

        }

    }

}
