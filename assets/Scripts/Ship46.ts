import G from "./Global";
import HPBar from "./HpBar";
import BasePool from "./common/BasePool";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ShipController extends cc.Component {

    @property()
    private enableMove: boolean = true;
    // @property()
    private enableBullet: boolean = false;
    private enableProtect: boolean = false;

    @property(BasePool)
    private bulletPool: BasePool = null;

    @property()
    private fireRate = 150;

    private fireTime = 0;
    private fireTime2 = 0;

    bulletShip: cc.Prefab = null;

    @property(cc.Prefab)
    private efxPrefab: cc.Prefab = null;

    @property(cc.Vec2)
    private startPoint: cc.Vec2 = null;

    @property()
    private minY = -500;

    public onDie: Function = null;
    public onRevive: Function = null;
    public isDie: boolean = false;

    @property()
    private HPMAX = 1;
    private hp = 0;
    // @property(HPBar)
    public hpBar: HPBar = null;

    @property(Boolean)
    hackNoDie: boolean = false;

    @property(cc.Prefab)
    bullet16: cc.Prefab[] = [];

    bullet: cc.Prefab[] = [];

    @property(cc.Node)
    shieldEffect: cc.Node = null;
    startFire1: boolean = false;
    startFire2: boolean = false;

    strAnimCurrent: string = '';

    isTranform: boolean = false;

    check: boolean = false;
    check1: boolean = false;

    @property(cc.Prefab)
    bullet_bezier: cc.Prefab[] = [];

    subTime: boolean = false;

    start() {
        this.newSkill();

        this.bulletPool.prefab = this.bullet16[0];
        this.bullet = this.bullet16;
        this.node.zIndex = 1;
        this.hp = this.HPMAX;
        G.ship = this;
    }

    changeAnimation(track: number, str: string, bool: boolean) {
        if (str === this.strAnimCurrent) {
            return;
        }
        this.getComponent(sp.Skeleton).setAnimation(track, str, bool);
        this.strAnimCurrent = str;
    }

    setEnableMove(enable: boolean) {
        this.enableMove = enable;
    }

    setEnableBullet(enable: boolean) {
        this.enableBullet = enable;
    }

    setEnableProtect(enable: boolean) {
        this.enableProtect = enable;
    }

    public newSkill() {
        this.startFire1 = true;
        this.scheduleOnce(() => {
            this.startFire2 = true;
        }, 1)
    }
    update(dt) {

        if (this.enableBullet) {
            this.updateBullet();
            this.updateBullet1();
        }

        if (this.enableMove) {
            this.updateMove();
        }
    }

    lerp(a: number, b: number, r: number) {
        return a + (b - a) * r;
    }

    private updateMove() {
        if (G.touchPos != null) {
            this.node.x = this.lerp(this.node.x, G.touchPos.x, 0.3);
            this.node.y = this.lerp(this.node.y, G.touchPos.y + 20, 0.3);
            if (this.node.y < this.minY) this.node.y = this.minY;
        }
    }

    private updateBullet() {
        if (Date.now() > this.fireTime + this.fireRate) {
            this.fireTime = Date.now();
            this.fireBullet();
        }
    }
    private updateBullet1() {
        if (this.startFire2) {
            if (Date.now() > this.fireTime2 + this.fireRate + 2000) {
                this.fireTime2 = Date.now();
                this.fireBezier();
                this.scheduleOnce(()=>{
                    this.fireBezier();
                },0.2)
            }
        }
    }

    private fireBezier() {
        var bullet = cc.instantiate(this.bullet_bezier[0]);
        bullet.parent = this.node.parent;
        bullet.x = this.node.x;
        bullet.y = this.node.y;
        var bullet = cc.instantiate(this.bullet_bezier[1]);
        bullet.parent = this.node.parent;
        bullet.x = this.node.x;
        bullet.y = this.node.y;
        var bullet = cc.instantiate(this.bullet_bezier[2]);
        bullet.parent = this.node.parent;
        bullet.x = this.node.x - 25;
        bullet.y = this.node.y;
        var bullet = cc.instantiate(this.bullet_bezier[3]);
        bullet.parent = this.node.parent;
        bullet.x = this.node.x + 25;
        bullet.y = this.node.y;

        // var bullet = cc.instantiate(this.bullet_bezier[4]);
        // bullet.parent = this.node.parent;
        // bullet.x = this.node.x + 20;
        // bullet.y = this.node.y;
        // var bullet = cc.instantiate(this.bullet_bezier[5]);
        // bullet.parent = this.node.parent;
        // bullet.x = this.node.x - 20;
        // bullet.y = this.node.y;
    }

    private fireBullet() {
        var newBulletShip = this.bulletPool.createObject(cc.Canvas.instance.node);
        newBulletShip.x = this.node.x;
        newBulletShip.y = this.node.y;
    }

    onCollisionEnter(other: cc.Collider, self: cc.Collider) {

        if (this.enableProtect)
            return;
        if (other.node.group == 'boss') {
            // self.destroy();
            this.onHitBoss(other.node);
        }
        else if (other.node.group == 'bossBullet') {
            if (G.hitBullet != null)
                this.onHitBossBullet(other.node);
        }
        else if (other.node.group == 'e' || other.node.group == 'eSpecial') {
            this.onHitEnemy(other.node);
        }
        else if (other.node.group == 'eBullet') {
            this.onHitEnemyBullet(other.node);
        }
        else if (other.node.group == 'lazer') {
            this.loseHp(1);
        }
    }

    itemUpgrade(other: cc.Collider) {
        G.bulletUpgrade++;
        this.changeBulletShip(this.bullet);
        other.node.destroy();
    }

    private onHitBoss(node: cc.Node) {
        this.loseHp(1);
    }

    private onHitBossBullet(node: cc.Node) {
        node.destroy();
        this.loseHp(1);
    }

    private onHitEnemy(node: cc.Node) {
        this.loseHp(1);
    }

    private onHitEnemyBullet(node: cc.Node) {
        node.destroy();
        this.loseHp(1);
    }

    private loseHp(n: number) {
        // cc.audioEngine.playEffect(G.flyDie, false);
        G.bulletDie = true;
        G.lazeShip = false;
        if (this.hpBar) {
            this.hpBar.setPercent(this.hp / this.HPMAX);
        }
        this.node.opacity = 0;
        this.isDie = true;
        this.explosion();
        G.enableMouse = true;
        this.showLife();

    }

    private onShipDie() {
        this.isDie = true;
        this.explosion();
        if (this.onDie) {
            this.onDie();
        }
    }
 
    showLife() {
        this.node.setPosition(cc.v2(0, -598));
        this.node.opacity = 255;

        this.shieldEffect.opacity = 255;
        this.enableProtect = true;
        G.touchPos = null;
        this.scheduleOnce(() => {
            G.enableMouse = false;
            this.enableMove = true;
        }, 0.3)
        this.node.runAction(cc.sequence(cc.moveTo(0.3, cc.v2(0, -280)).easing(cc.easeQuarticActionIn()), cc.callFunc(() => {
            this.getComponent(cc.CircleCollider).enabled = true;
            this.scheduleOnce(() => {
                this.enableProtect = false;
                this.shieldEffect.opacity = 0;

            }, 0.5)
            G.touchPos = null;
            G.bulletDie = false;
        })));
    }

    private explosion() {
        var efx = cc.instantiate(this.efxPrefab);
        efx.parent = this.node.parent;
        efx.x = this.node.x;
        efx.y = this.node.y;
        this.node.opacity = 0;
        this.enableBullet = false;
        this.enableMove = false;
        this.getComponent(cc.CircleCollider).enabled = false;
    }

    public revive() {
        this.enableProtect = true;
        G.touchPos = null;
        this.node.setPosition(this.startPoint);
        this.node.opacity = 255;
        this.enableBullet = true;

        this.getComponent(cc.CircleCollider).enabled = true;
        this.isDie = false;
        this.scheduleOnce(() => {
            this.enableProtect = false;
        }, 1);
        this.hp = this.HPMAX;
        if (this.onRevive) {
            this.onRevive();
        }
    }

    flyOut(done: Function = null) {
        G.checkWing = false;
        G.touchPos = null;
        this.enableMove = false;
        this.enableProtect = true;
        this.enableBullet = false;
        this.node.runAction(cc.sequence(cc.spawn(
            cc.moveBy(1.1, 0, 2000).easing(cc.easeBackIn()), cc.fadeOut(1.1)),
            cc.callFunc(() => {
                this.enableMove = false;
                G.touchPos = null;
                if (done)
                    done();
            })
        ));
    }

    changeBulletShip(prefabs) {
        switch (G.bulletUpgrade) {
            case 2: {
                this.bulletPool.prefab = prefabs[1];
                break;
            }
            default: {
                break;
            }
        }
    }
}