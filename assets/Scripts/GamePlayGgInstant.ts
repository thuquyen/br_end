import WaveQuangCao from "./Wave/WaveQuangCao";
import LevelManager from "./level/LevelManager";
import ShipController from "./ShipInstant";
import HandController from "./HandController";
import { Helper } from "./Helper";
import G from "./Global";

const { ccclass, property } = cc._decorator;

let GameState = {
    INIT: 0,
    READY: 1,
    PLAYING: 2,
    ENDGAME: 3
}

declare const window: any;

@ccclass
export default class SpaceShooterAd2 extends cc.Component {

    gameState: number = GameState.INIT;
    @property(cc.Node)
    logo: cc.Node = null;

    @property(cc.Node)
    txtMoveShip: cc.Node = null;

    @property(WaveQuangCao)
    w0_rectange: WaveQuangCao = null

    @property(LevelManager)
    w1_heart: LevelManager = null;

    @property(LevelManager)
    w2_spawn: LevelManager = null;

    @property(WaveQuangCao)
    w3_scroll: WaveQuangCao = null

    @property(WaveQuangCao)
    w5_jump: WaveQuangCao = null

    @property(LevelManager)
    w6_2heart: LevelManager = null;

    @property(HandController)
    hand: HandController = null;

    @property(cc.Layout)
    bgLayout: cc.Layout = null;

    index: number = 1;

    nShip: number = 0;

    @property(ShipController)
    ship: ShipController = null;

    istouch: boolean = true;

    ironsource: boolean = false;

    check1st: boolean = false;
    check2st: boolean = false;
    check3st: boolean = false;
    check4st: boolean = false;
    check5st: boolean = false;

    nextBoss: number = 0;
    doneChoose: boolean = false;

    firstTouch: boolean = false;
    posX: number;
    posY: number;
    checkEnd: boolean = false;

    @property(cc.Node)
    menuEnd: cc.Node = null;

    isItemVip: boolean = false;
    ready: boolean = false;

    checkMerge: boolean = false;

    checkInteraction: boolean = false;

    checkShipDie: boolean = false;

    //txt load wave
    @property(cc.Node)
    loadLevel: cc.Node = null;

    @property(cc.Node)
    txt_Up: cc.Node = null;

    @property(cc.Node)
    txt_Down: cc.Node = null;

    @property(cc.SpriteFrame)
    txt_Up_Frame: cc.SpriteFrame[] = [];

    @property(cc.SpriteFrame)
    txt_Down_Frame: cc.SpriteFrame[] = [];

    @property(cc.Node)
    menu: cc.Node = null;
    @property(cc.Node)
    next: cc.Node[] = [];
    @property(cc.Node)
    replay: cc.Node = null;
    @property(cc.Node)
    install: cc.Node = null;

    @property(cc.Prefab)
    wave_ggInstant: cc.Prefab[] = []

    @property({ type: cc.AudioClip })
    bgSound: cc.AudioClip = null;
    temp: number = 0;

    onLoad() {

    }

    appearShip() {
        this.checkMerge = true;
        this.node.stopAllActions
        this.ship.node.runAction(cc.sequence(cc.moveTo(0.8, cc.v2(0, -270)).easing(cc.easeBackOut()),
            cc.callFunc(() => {
                this.node.getComponent("CanvasTouchManager").enabled = true;
                cc.director.getCollisionManager().enabled = true;

                this.posX = this.ship.node.x;
                this.posY = this.ship.node.y;
                this.gameState = GameState.READY;
                this.startGame();
            })))
    }

    showWaveTxt(number_Up: number, number_Down: number) {
        this.txtMoveShip.active = false;
        this.txt_Up.getComponent(cc.Sprite).spriteFrame = this.txt_Up_Frame[number_Up];
        this.txt_Down.getComponent(cc.Sprite).spriteFrame = this.txt_Down_Frame[number_Down];

        this.loadLevel.opacity = 255;
        this.loadLevel.getComponent(cc.Animation).play();
        this.scheduleOnce(() => {
            this.loadLevel.opacity = 0;
        }, 0.75)
    }

    start() {
        this.temp = cc.audioEngine.play(this.bgSound, true, 1);
        cc.view.enableAutoFullScreen(false);
        this.txtMoveShip.zIndex = 100;
        this.ship.node.zIndex = 1;
        // this.bgLayout.node.zIndex = 2; 
        cc.director.getCollisionManager().enabled = false;
        this.menu.zIndex = 200;
        this.gameState = GameState.INIT;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);

        if (this.ironsource) {
            window.NUC.trigger.ready("ready")
        }

        this.appearShip();
        this.Play();
    }

    nextLevel_1() {
        cc.audioEngine.playEffect(G.click, false);
        this.logo.opacity = 255;
        this.menu.active = false;
        this.next[0].destroy();

        this.showWaveTxt(1, 0);
        this.scheduleOnce(() => {
            this.showWaveTxt(3, 3);
            this.scheduleOnce(() => {
                this.w2_spawn.startLevel();

                this.scheduleOnce(() => {
                    cc.director.getCollisionManager().enabled = true;
                    G.menu = false;
                }, 1)
            }, 1)
        }, 1)
    }

    nextLevel_2() {
        cc.audioEngine.playEffect(G.click, false);
        this.logo.opacity = 255;
        this.menu.active = false;
        this.next[1].destroy();

        this.showWaveTxt(2, 0);
        // cc.director.getCollisionManager().enabled = false;
        this.scheduleOnce(() => {
            this.showWaveTxt(3, 6);
            this.scheduleOnce(() => {
                this.w5_jump.appear(() => { });
                this.scheduleOnce(() => {
                    cc.director.getCollisionManager().enabled = true;
                    G.menu = false;
                }, 1)
            }, 1)
        }, 1)
    }

    replay_btn() {
        cc.audioEngine.playEffect(G.click, false);
        G.e_3circleAll = 0;
        G.e_triangle = 0;
        G.e_flower = 0;
        G.e_jump = 0;
        G.bossDie = false;
        G.bulletUpgrade = 1;
        // this.menu.destroy();
        // this.menu.scale = 0;
        // var findDeep = function (node, nodeName) {
        //     if (node.name == nodeName) return node;

        //     for (var i = 0; i < node.children.length; i++) {
        //         var res = findDeep(node.children[i], nodeName);
        //         if (res) {
        //             res.destroy();
        //         }
        //         // return res;
        //     }
        // }
        // findDeep(cc.director.getScene(), "Menu");
        this.scheduleOnce(() => {
            cc.director.loadScene("GgInstant_Main");
        }, 0.5)

    }
    ins_btn() {
        cc.audioEngine.playEffect(G.click, false);
        cc.androidInstant.showInstallPrompt()
    }
    next_2() {
        //level2
        //wave 1/3
        this.w1_heart.onComplete = () => {
            this.txtMoveShip.active = false;
            this.menu.active = true;
            this.menu.runAction((cc.scaleTo(0.6, 1, 1)));
            this.logo.opacity = 0;
            this.next[0].active = true;
            G.menu = true;
            cc.director.getCollisionManager().enabled = false;
        }
    }

    Play() {
        //level 1
        this.showWaveTxt(0, 0);

        // wave 1/2
        this.scheduleOnce(() => {
            this.showWaveTxt(3, 1);
            this.scheduleOnce(() => {

                //  thíw0_rectange = cc.instantiate(this.wave_ggInstant[4]);
                // w0_rectange.parent = cc.Canvas.instance.node;
                // this.w0_rectange.appear(() => { });

                this.w0_rectange.appear(() => { });
                this.txtMoveShip.active = true;
                this.hand.node.opacity = 255;
                this.hand.node.zIndex = 12;
                this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 50);
            }, 1)
        }, 1)


        //wave 2/2
        this.w0_rectange.onComplete = () => {
            if (!G.delete) {
                this.showWaveTxt(3, 2);
                this.scheduleOnce(() => {
                    this.w1_heart.startLevel();
                }, 1)
            }
        }

        this.next_2();

        //wave 2/3
        this.w2_spawn.onComplete = () => {
            this.showWaveTxt(3, 4);
            cc.director.getCollisionManager().enabled = true;
            this.scheduleOnce(() => {
                this.w3_scroll.appear(() => { });
            }, 1)
        }

        //wave 3/3
        this.w3_scroll.onComplete = () => {
            this.showWaveTxt(3, 5);
            this.scheduleOnce(() => {
                var wave4 = cc.instantiate(this.wave_ggInstant[0]);
                wave4.parent = cc.Canvas.instance.node;
                wave4.runAction(cc.spawn(cc.fadeIn(0.4), cc.scaleTo(1, 1)));
                // this.w4_3circle.active = true;
                // this.w4_3circle.runAction(cc.spawn(cc.fadeIn(0.4), cc.scaleTo(1, 1)));
            }, 1)
        }
        // this.temp = cc.audioEngine.play(this.bgSound, true, 1);  

        //wave 3/5
        this.w6_2heart.onComplete = () => {
            this.showWaveTxt(3, 8);
            cc.director.getCollisionManager().enabled = false;
            this.scheduleOnce(() => {
                // this.w7_triangle.active = true;
                var wave7 = cc.instantiate(this.wave_ggInstant[1]);
                wave7.parent = cc.Canvas.instance.node;

                this.scheduleOnce(() => {
                    cc.director.getCollisionManager().enabled = true;
                }, 1)
            }, 1)
        }
    }

    update(dt) {
        //wave 1/5
        // if (this.ship.hp <= 0 && !G.delete) {
        //     G.delete = true;
        //     this.deleteE();
        // }

        if (!this.check1st && G.e_3circleAll >= 27) {
            this.txtMoveShip.active = false;
            this.check1st = true;
            this.menu.active = true;
            this.menu.runAction((cc.scaleTo(0.6, 1, 1)));
            this.logo.opacity = 0;
            this.next[1].active = true;
            G.menu = true;
        }

        if (!this.check5st && G.e_jump >= 24) {
            this.check5st = true;
            this.showWaveTxt(3, 7);
            this.scheduleOnce(() => {
                this.w6_2heart.startLevel();
            }, 1)
        }

        if (!this.check2st && G.e_triangle >= 32) {
            this.check2st = true;
            this.showWaveTxt(3, 9);
            this.scheduleOnce(() => {
                var wave8 = cc.instantiate(this.wave_ggInstant[2]);
                wave8.parent = cc.Canvas.instance.node;
                wave8.runAction(cc.spawn(cc.fadeIn(0.4), cc.scaleTo(1, 1)));
            }, 1)
        }

        if (!this.check3st && G.e_flower >= 25) {
            this.check3st = true;
            this.showWaveTxt(3, 10);
            this.scheduleOnce(() => {
                cc.director.getCollisionManager().enabled = false;
                var waveBoss = cc.instantiate(this.wave_ggInstant[3]);
                waveBoss.parent = cc.Canvas.instance.node;

                waveBoss.runAction(cc.sequence(cc.moveTo(2.2, cc.v2(0, 220)), cc.callFunc(() => {
                    G.hpBoss = true;
                    cc.director.getCollisionManager().enabled = true;
                    G.boss1 = true;
                })))

                // this.scheduleOnce(() => {
                //     G.hpBoss = true;
                //     cc.director.getCollisionManager().enabled = true;
                //     G.boss1 = true;
                // }, 2)

                // this.w9_bossEvent.active = true;
                // cc.director.getCollisionManager().enabled = false;
                // this.w9_bossEvent.runAction(cc.sequence(cc.moveTo(2.2, cc.v2(0, 220)), cc.callFunc(() => {
                //     G.hpBoss = true;
                //     cc.director.getCollisionManager().enabled = true;
                //     G.boss1 = true;
                // })))
            }, 1)
        }

        if (!this.check4st && G.bossDie) {
            this.check4st = true;
            // this.endGame();
            cc.audioEngine.stop(this.temp);
            G.hpBoss = false;
            cc.director.getCollisionManager().enabled = false;
            this.menu.active = true;
            this.menu.runAction((cc.scaleTo(0.6, 1, 1)));
            this.logo.opacity = 0;
            this.replay.active = true;
            cc.audioEngine.playEffect(G.finalWin, false);
            this.ship.setEnableBullet(false);
            this.ship.setEnableMove(false);
        }
    }

    onTouchStart(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                if (!G.endG && !G.enableMouse) {
                    this.startGame();
                    this.Interaction();
                }
                break;

            case GameState.PLAYING:
                if (!G.endG && !G.enableMouse) {
                    this.txtMoveShip.active = false;
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                    this.hand.stopSwipeAnim();
                    // this.unscheduleAllCallbacks();
                }
                break;
        }
    }

    onTouchMove(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                if (!G.endG && !G.enableMouse) {
                    this.Interaction();
                    this.startGame();
                }
                break;
            case GameState.PLAYING:
                if (!G.endG && !G.enableMouse) {
                    this.txtMoveShip.active = false;
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                    this.hand.stopSwipeAnim();
                    // this.unscheduleAllCallbacks();
                }
                break;
        }

    }

    onTouchEnd(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!G.endG) {
                    this.ship.setEnableBullet(false);
                    if (!G.menu) {
                        this.txtMoveShip.active = true;
                        this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 50)
                    }
                }
                break;
        }
    }

    onTouchCancel(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!G.endG) {
                    this.ship.setEnableBullet(false);
                    if (!G.menu) {
                        this.txtMoveShip.active = true;
                        this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 50);
                    }
                }
                break;
        }
    }

    Interaction() {
        this.bgLayout.node.active = false;;
        if (this.ironsource && !this.checkInteraction) {
            this.checkInteraction = true
            window.NUC.trigger.interaction();
        }
    }

    startGame() {
        if (this.checkMerge) {
            this.gameState = GameState.PLAYING;
            // this.hand.stopSwipeAnim();
            this.ship.setEnableMove(true);
        }
    }

    deleteE() {
        var findDeep = function (node, nodeName) {
            if (node.name == nodeName) return node;

            for (var i = 0; i < node.children.length; i++) {
                var res = findDeep(node.children[i], nodeName);
                if (res) {
                    res.destroy();
                }
                // return res;
            }
        }
        findDeep(cc.director.getScene(), "z33");
        findDeep(cc.director.getScene(), "e_heart");
        findDeep(cc.director.getScene(), "e_spawn");
        findDeep(cc.director.getScene(), "enemy_cb3");
        findDeep(cc.director.getScene(), "enemy_cb4");
        findDeep(cc.director.getScene(), "e_3circle");
        findDeep(cc.director.getScene(), "e_jump");
        findDeep(cc.director.getScene(), "crop1");
        findDeep(cc.director.getScene(), "crop2");
        findDeep(cc.director.getScene(), "crop3");
        findDeep(cc.director.getScene(), "e_triangle");
        findDeep(cc.director.getScene(), "e_flower");
        this.ship.hp = 1;
    }

    endGame() {
        var findDeep = function (node, nodeName) {
            if (node.name == nodeName) return node;
            for (var i = 0; i < node.children.length; i++) {
                var res = findDeep(node.children[i], nodeName);
                if (res) {
                    res.destroy();
                }
                // return res;
            }
        }
        findDeep(cc.director.getScene(), "Bullet_42_2");
        findDeep(cc.director.getScene(), "bulletBoss6");
        this.ship.node.runAction(cc.sequence(
            cc.moveBy(1.1, 0, 2000).easing(cc.easeBackIn()),
            cc.callFunc(() => {
                findDeep(cc.director.getScene(), "ship");
                this.menuEnd.active = true;
            }))
        )
        G.text = false;
        this.checkEnd = true;

        if (this.ironsource) {
            window.NUC.trigger.endGame('win')
        }

        // if (this.buttonInstall)
        //     this.buttonInstall.active = false;

        this.hand.node.active = false;
        this.txtMoveShip.active = false;
        this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    }
}