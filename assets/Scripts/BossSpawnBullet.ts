import BaseEnemy from "./Enemy/BaseEnemy";

const {ccclass, property} = cc._decorator;

@ccclass
export default class EnemySpawnBullet extends BaseEnemy {

    @property(cc.Prefab)
    prefabBullet: cc.Prefab = null;

    @property()
    fireRate = 5;

    @property()
    fireDelay = 1;

    @property(cc.Layout)
    barHealthy: cc.Layout = null;

    @property()
    barScale: number = 0;

    @property()
    healthy: number = 0;

    onLoad(){
        this.barScale = this.barHealthy.node.scaleX;
    }

    update(dt){
        this.barHealthy.node.scaleX = this.hp/this.healthy * this.barScale;
    }


}