import BasePool from "./common/BasePool";

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    bullet: cc.Node[] = [];

    @property()
    public timeFire: number = 0;
    @property()
    oppositeX: number = 1;
    @property()
    oppositeY: number = 1;
    @property()
    virusWar: boolean = false;
    @property(Number)
    NumLeft: number = 0;
    @property(Number)
    NumRight: number = 0;

    @property()
    tamgiac: boolean = false;

    @property()
    bullet_dai: boolean = false;

    onLoad() {
        // if (this.node.name == 'Bullet_40_3') {
        //     for (let i = 0; i < this.bullet.length; i++) {
        //         this.bulletZZ(this.bullet[i]);
        //     }
        // }
        // else {
        for (let i = 0; i < this.bullet.length; i++) {
            this.bulletShip(this.bullet[i]);
        }
        if (this.virusWar) {
            for (let i = 1; i < this.NumLeft; i++) {
                this.MovingBullet(this.bullet[i], this.timeFire, 20 * i, 1000);
            }
            for (let i = this.NumLeft; i < this.NumRight; i++) {
                this.MovingBullet(this.bullet[i], this.timeFire, -20 * (i - (this.NumLeft - 1)), 1000);
            }
            this.MovingBullet(this.bullet[0], this.timeFire, 0, 1000);
        }

        else if (this.bullet_dai) {

        }
    }

    MovingBullet(Temp: cc.Node, firetime: number, x: number, y: number) {
        Temp.runAction(cc.moveBy(0.23, cc.v2(x, 40)));
        //Temp.runAction(cc.moveBy(0.25, cc.v2(x, 40)));
        if (this.tamgiac) {
            Temp.runAction(cc.sequence(cc.delayTime(0.1), cc.moveBy(0.1, cc.v2(-x, 60))));
        }
        else if (this.bullet_dai) {
            Temp.runAction(cc.sequence(cc.moveBy(firetime, cc.v2(x * Math.tan(Temp.angle * Math.PI / 180), y)), cc.callFunc(() => {
                Temp.destroy();
            })));
        }
        else
            Temp.runAction(cc.sequence(cc.moveBy(firetime, cc.v2(x * Math.tan(Temp.angle * Math.PI / 180), y)), cc.callFunc(() => {
                Temp.destroy();
            })));
    }

    bulletZZ(fire: cc.Node) {
        var ran = Math.round((Math.random())) * 2 - 1;
        fire.runAction(cc.sequence(cc.moveBy(0.6, cc.v2((Math.random() * 2 - 1) * 70, 50)),
            cc.moveBy(2, cc.v2(ran * this.node.parent.width / 2, 100)),
            cc.moveBy(2, cc.v2(-ran * this.node.parent.width / 2, 200)),
            cc.moveBy(this.timeFire, cc.v2(2000 * Math.tan(fire.angle * Math.PI / 180), 2000)),
            cc.callFunc(() => {
                fire.destroy();
            })));
    }
    private bulletShip(fire: cc.Node) {
        // fire.x = this.node.x;
        // fire.y = this.node.y + 15;
        fire.runAction(cc.sequence(cc.moveBy(this.timeFire, cc.v2(this.oppositeX * 2000 * Math.tan(fire.angle * Math.PI / 180), 2000 * this.oppositeY)), cc.callFunc(() => {
            fire.destroy();
        })));
    }

    // update (dt) {}
}
