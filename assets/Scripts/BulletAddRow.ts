const { ccclass, property } = cc._decorator;

@ccclass
export default class BulletFireWork extends cc.Component {
    @property(cc.Node)
    private fire: cc.Node = null;
    @property()
    public timeFire: number = 0;

    start() {
        this.getFire();
    }
    getFire() {
        this.fire.runAction(cc.sequence(cc.moveBy(2, 0, 1000), cc.callFunc(() => {
            this.fire.destroy();
        })));

    }
}
