import G from "./Global";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ScoreLabelBR8 extends cc.Component {

    @property(cc.Layout)
    bgLayout: cc.Layout = null;

    @property(cc.Node)
    public PauseLabel: cc.Node = null;

    @property(cc.Node)
    public wing: cc.Node = null;

    @property(cc.Node)
    public FxPow: cc.Node = null;

    @property(cc.Node)
    laze: cc.Node = null;

    @property(cc.Node)
    logo: cc.Node = null;

    stateLaze: boolean = true;

    stateStart: boolean = false;

    start() {
        this.logo.zIndex = 3;
        this.laze.zIndex = 0;
        this.wing.zIndex = 2;
        this.FxPow.zIndex = 2;
        this.PauseLabel.zIndex = 2;
        cc.audioEngine.playMusic(G.bgSound, true);

        this.node.on("touchstart", () => {
            if (G.check) {
                G.bg = 180;
                G.text = false;
                G.e = true;
            } else {
                this.stateStart = true;
                G.bg = 0;
                G.text = false;
                G.e = false;
                G.fire = 1000;
                if (G.lazeShip) {
                    this.laze.active = true;
                    this.laze.runAction(cc.fadeIn(0.2));
                }
                else {
                    this.laze.runAction(cc.fadeOut(0.1));
                }
            }
            return;
        });

        this.node.on("touchend", () => {
            if (G.check) {
                G.bg = 180;
                G.text = false;
                G.e = true;
            } else {
                this.stateStart = false;
                G.bg = 100;
                G.text = true;
                G.e = true;
                G.fire = 1;
                this.laze.runAction(cc.fadeOut(0.1));
            }
            return;
        });
    }

    update(dt) {

        if (G.lazeShip && this.stateLaze && this.stateStart) {
            this.stateLaze = false;
            this.laze.active = true;
            this.laze.runAction(cc.fadeIn(0.2));
        }

        this.bgLayout.node.opacity = G.bg;
        this.PauseLabel.active = G.text;

        if (G.checkWing) {
            this.FxPow.active = true;
            this.wing.active = true;
            // G.bulletUpgrade = 2;
        } else {
            this.FxPow.active = false;
            this.wing.active = false;
            // G.bulletUpgrade = 1;
        }

        if (G.check) {
            G.bg = 180;
            G.text = false;
            G.e = true;
        }

    }

}
