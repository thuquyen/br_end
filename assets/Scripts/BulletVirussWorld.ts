
const { ccclass, property } = cc._decorator;

@ccclass
export default class BulletVirussWorld extends cc.Component {
    @property(Number)
    public fireTime: number = 0;
    @property(cc.Node)
    public Bullet: cc.Node[] = [];
    @property(Number)
    NumLeft: number = 0;
    @property(Number)
    NumRight: number = 0;

    start() {
        // for (let i = 0; i < this.Bullet.length; i++) {
        //     this.MovingBullet(this.Bullet[i], this.fireTime, -2000, 3000);
        // }
        for (let i = 1; i < this.NumLeft; i++) {
            this.MovingBullet(this.Bullet[i], this.fireTime, 20 * i, 1000);
        }
        for (let i = this.NumLeft; i < this.NumRight; i++) {
            this.MovingBullet(this.Bullet[i], this.fireTime, -20 * (i - (this.NumLeft - 1)), 1000);
        }
        this.MovingBullet(this.Bullet[0], this.fireTime, 0, 1000);
    }

    MovingBullet(Temp: cc.Node, firetime: number, x: number, y: number) {
        Temp.runAction(cc.moveBy(0.23, cc.v2(x, 40)));
        //Temp.runAction(cc.moveBy(0.25, cc.v2(x, 40)));
        // Temp.runAction(cc.sequence(cc.delayTime(0.43), cc.moveBy(0.43, cc.v2(-x, 60))));
        Temp.runAction(cc.sequence(cc.moveBy(firetime, cc.v2(x * Math.tan(Temp.angle * Math.PI / 180), y)), cc.callFunc(() => {
            Temp.destroy();
        })));
    }
}
