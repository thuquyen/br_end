import WaveQuangCao from "./Wave/WaveQuangCao";
import ShipController from "./Ship47";
import HandController from "./HandController";
import { Helper } from "./Helper";
import G from "./Global";


const { ccclass, property } = cc._decorator;

let GameState = {
    INIT: 0,
    READY: 1,
    PLAYING: 2,
    ENDGAME: 3
}

declare const window: any;

@ccclass
export default class SpaceShooterAd2 extends cc.Component {

    gameState: number = GameState.INIT;

    @property(WaveQuangCao)
    wave: WaveQuangCao = null;

    @property(cc.Node)
    endG: cc.Node = null;

    @property(cc.Node)
    txtMoveShip: cc.Node = null;

    @property(HandController)
    hand: HandController = null;

    // @property(cc.Node)
    // buttonInstall: cc.Node = null;

    @property(cc.Node)
    titleMini: cc.Node = null;

    index: number = 1;

    @property(ShipController)
    ship: ShipController = null;

    istouch: boolean = true;

    ironsource: boolean = false;

    mindworks: boolean = true;

    // @property({ type: cc.AudioClip })
    // bgSound: cc.AudioClip = null;
    // temp: number = 0;

    check1st: boolean = false;
    check2st: boolean = false;

    firstTouch: boolean = false;
    posX: number;
    posY: number;
    checkEnd: boolean = false;

    @property(cc.Node)
    tempBoss: cc.Node = null;

    @property(cc.Node)
    menu: cc.Node = null;

    check_play: boolean = false;

    @property(cc.Node)
    coinTxt: cc.Node = null;

    point: number = 0;

    @property(cc.Node)
    wall: cc.Node = null;

    onLoad() {
        cc.view.enableAutoFullScreen(false);
    }

    start() {
        // this.temp = cc.audioEngine.play(this.bgSound, true, 1);
        //auto play
        this.scheduleOnce(() => {
            if (!this.check_play) {
                this.check_play = true;
                this.btnPlay();
            }
        }, 6)

        this.txtMoveShip.zIndex = 100;
        cc.director.getCollisionManager().enabled = true;
        this.ship.node.zIndex = 1;
        // this.endG.zIndex = 3;

        this.gameState = GameState.INIT;
        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
        if (this.ironsource) {
            window.NUC.trigger.ready("ready")
        }

        this.wave.onComplete = () => {
            G.endG = true;
            this.endGame();
        }
    }

    btnPlay() {
        // this.coin.active = true;
        this.menu.destroy();
        this.wall.getComponent(cc.Animation).play();
        this.scheduleOnce(() => {
            this.appearShip();
            this.tempBoss.runAction(cc.sequence(cc.moveBy(0.5, cc.v2(0, -50)), cc.scaleTo(0.5, 0, 0)))
        }, 0.5)
        G.allow_fire = false;
        this.check_play = true;

    }

    appearShip() {
        // this.buttonInstall.active = true;
        this.ship.node.runAction(cc.sequence(cc.moveTo(0.8, cc.v2(0, -280)).easing(cc.easeBackOut()), cc.callFunc(() => {
            this.turnOnGuide();
            G.startSpawnItem = true;
            this.posX = this.ship.node.x;
            this.posY = this.ship.node.y;
            this.node.getComponent("CanvasTouchManager").enabled = true;
            this.gameState = GameState.READY;
            this.wave.appear(() => { })
        })))
        this.scheduleOnce(() => {

            this.ship.setEnableBullet(true);
        }, 1)
    }

    turnOnGuide() {
        this.txtMoveShip.active = true;
        this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
        this.hand.node.zIndex = 12;
    }
    update(dt) {
        if (G.totalE > this.point) {
            this.coinTxt.getComponent(cc.Label).string = (this.point + 5).toString() + "K";
            this.point = G.totalE;
        }
    }

    endGame() {

        // if (G.enemyDie != null)
        // cc.audioEngine.playEffect(G.finalWin, false);

        if (this.mindworks) {
            window.gameEnd && window.gameEnd();
        }
        this.ship.flyOut();
        // this.buttonInstall.active = false;
        // cc.audioEngine.stop(this.temp);
        // cc.audioEngine.playEffect(G.finalWin, false);

        G.text = false;
        this.checkEnd = true;

        if (this.ironsource) {
            window.NUC.trigger.endGame('win')
        }
        this.scheduleOnce(() => {
            this.endG.active = true;
        }, 1)

        this.hand.node.active = false;
        this.txtMoveShip.active = false;
        this.ship.setEnableMove(false);

        this.node.off(cc.Node.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    }

    onTouchStart(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                G.clickStart = true;
                this.startGame();
                break;
            case GameState.PLAYING:
                this.txtMoveShip.active = false;
                this.hand.stopSwipeAnim();
                if (this.check_play) {
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                }
                break;
        }
    }

    onTouchMove(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.READY:
                G.clickStart = true;
                this.startGame();
                break;
            case GameState.PLAYING:
                this.txtMoveShip.active = false;
                this.hand.stopSwipeAnim();
                if (this.check_play) {
                    this.ship.setEnableMove(true);
                    this.ship.setEnableBullet(true);
                }
                break;
        }
    }

    onTouchEnd(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!this.ship.isDie && this.check_play) {
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
                    this.ship.setEnableBullet(false);
                }
                break;
        }
    }

    onTouchCancel(event: cc.Touch) {
        switch (this.gameState) {
            case GameState.PLAYING:
                if (!this.ship.isDie && this.check_play) {
                    this.txtMoveShip.active = true;
                    this.hand.runSwipeAnim(this.ship.node.x + 30, this.ship.node.y - 75);
                    this.ship.setEnableBullet(false);
                }
                break;
        }
    }

    startGame() {
        if (this.ironsource)
            window.NUC.trigger.interaction();
        this.gameState = GameState.PLAYING;
        this.hand.stopSwipeAnim();
        this.ship.setEnableMove(true);

    }
}