import G from "./Global";
import HPBar from "./HpBar";
import BasePool from "./common/BasePool";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ShipController extends cc.Component {

    @property()
    private enableMove: boolean = true;
    // @property()
    private enableBullet: boolean = false;
    private enableProtect: boolean = false;

    @property(BasePool)
    private bulletPool: BasePool = null;

    @property()
    private fireRate = 180;

    private fireTime = 0;

    bulletShip: cc.Prefab = null;

    @property(cc.Prefab)
    private efxPrefab: cc.Prefab = null;

    @property(cc.Vec2)
    private startPoint: cc.Vec2 = null;

    @property()
    private minY = -500;

    public onDie: Function = null;
    public onRevive: Function = null;
    public isDie: boolean = false;

    @property()
    private HPMAX = 1;
    public hp = 0;
    // @property(HPBar)
    public hpBar: HPBar = null;

    @property(Boolean)
    hackNoDie: boolean = false;

    @property(cc.Prefab)
    bullet16: cc.Prefab[] = [];

    @property(cc.Prefab)
    bullet17: cc.Prefab[] = [];

    bullet: cc.Prefab[] = [];

    @property(cc.Node)
    shieldEffect: cc.Node = null;

    @property(cc.Node)
    wing: cc.Node = null;
    @property(sp.SkeletonData)
    wing_chongchong: sp.SkeletonData = null;

    time2S: boolean = false;
    strAnimCurrent: string = '';

    @property(cc.Node)
    txtGuide: cc.Node = null;

    @property(sp.SkeletonData)
    shipGreen: sp.SkeletonData = null;

    isShipGreen: boolean = false;

    start() {
        this.wing.getComponent(sp.Skeleton).setMix('appear', 'idle', 0.1);
        this.wing.getComponent(sp.Skeleton).setMix('idle', 'out', 0.1);

        this.bulletPool.prefab = this.bullet16[0];
        this.bullet = this.bullet16;
        this.node.zIndex = 1;
        this.hp = this.HPMAX;
        G.ship = this;
        // this.node.getComponent(sp.Skeleton).setAnimation(1, 'fly', true);

        let spine = this.wing.getComponent(sp.Skeleton);
        spine.setCompleteListener((trackEntry, loopCount) => {
            let name = trackEntry.animation ? trackEntry.animation.name : '';
            //Skill_1
            if (name === 'appear') {
                this.wing.getComponent(sp.Skeleton).setAnimation(1, 'idle', true);
            }
            if (name === 'idle') {
                this.wing.getComponent(sp.Skeleton).setAnimation(1, 'out', false);
            }
        });
    }

    setEnableMove(enable: boolean) {
        this.enableMove = enable;
    }

    setEnableBullet(enable: boolean) {
        this.enableBullet = enable;
    }

    setEnableProtect(enable: boolean) {
        this.enableProtect = enable;
    }

    update(dt) {
        if (this.enableBullet) {
            this.updateBullet();

            // if (this.startFire1) {
            //     this.updateBullet1();
            // }
        }

        if (this.enableMove) {
            this.updateMove();
        }
    }

    lerp(a: number, b: number, r: number) {
        return a + (b - a) * r;
    }

    private updateMove() {
        if (G.touchPos != null) {
            this.node.x = this.lerp(this.node.x, G.touchPos.x, 0.3);
            this.node.y = this.lerp(this.node.y, G.touchPos.y + 20, 0.3);
            if (this.node.y < this.minY) this.node.y = this.minY;
        }
    }

    private updateBullet() {
        if (Date.now() > this.fireTime + this.fireRate) {
            this.fireTime = Date.now();
            this.fireBullet();
        }
    }

    private fireBullet() {

        var newBulletShip = this.bulletPool.createObject(cc.Canvas.instance.node);
        newBulletShip.parent = cc.Canvas.instance.node;
        newBulletShip.x = this.node.x;
        newBulletShip.y = this.node.y + 15;
        // newBulletShip.scale = 0.1
        newBulletShip.zIndex = 10;
    }

    onCollisionEnter(other: cc.Collider, self: cc.Collider) {
        if (other.node.group == "item") {
            cc.audioEngine.playEffect(G.item, false);
            if (other.node.name == 'item_Upgrade_instant') {
                this.itemUpgrade(other);
                this.wing.active = true;
                this.wing.getComponent(sp.Skeleton).setAnimation(1, 'appear', true);
            }

            if (other.node.name == 'ItemShip_Instant') {
                this.isShipGreen = true;
                other.node.destroy();
                this.node.getComponent(sp.Skeleton).skeletonData = this.shipGreen;
                this.node.scale = 1.3;
                this.getComponent(sp.Skeleton).setSkin('2');
                this.getComponent(sp.Skeleton).setAnimation(1, '1_idle', true);
                this.getComponent(cc.CircleCollider).radius = 35;
                this.getComponent(cc.CircleCollider).offset.y = -10;
                this.wing.getComponent(sp.Skeleton).skeletonData = this.wing_chongchong;
                this.wing.scale = 0.55;
                this.wing.active = false;

                this.shieldEffect.scale = 0.9

                G.bulletUpgrade = 0;
                this.bullet = this.bullet17;
                this.fireRate = 210;
                this.bulletPool.prefab = this.bullet17[0];
                this.changeBulletShip(this.bullet);
            }
        }
        else if (other.node.group == 'lazer') {
            this.loseHp(1);
        }
        if (this.enableProtect)
            return;
        if (other.node.group == 'boss') {
            // self.destroy();
            this.onHitBoss(other.node);
        }
        else if (other.node.group == 'bossBullet') {
            if (G.hitBullet != null)
                this.onHitBossBullet(other.node);

        }
        else if (other.node.group == 'e' || other.node.group == 'eSpecial') {
            // if (G.hitBullet != null)
            this.onHitEnemy(other.node);
        }
        else if (other.node.group == 'eBullet') {
            this.onHitEnemyBullet(other.node);
        }

    }

    itemUpgrade(other: cc.Collider) {
        G.bulletUpgrade++;
        this.fireRate = 165;
        this.changeBulletShip(this.bullet);
        other.node.destroy();
    }

    changeBulletShip(prefabs) {
        switch (G.bulletUpgrade) {
            case 1: {
                this.bulletPool.prefab = prefabs[1];
                break;
            }
            case 2: {
                this.bulletPool.prefab = prefabs[2];
                break;
            }
            default: {
                break;
            }
        }
    }

    private onHitBoss(node: cc.Node) {
        this.loseHp(1);
    }

    private onHitBossBullet(node: cc.Node) {
        node.destroy();
        this.loseHp(1);
    }

    private onHitEnemy(node: cc.Node) {
        this.loseHp(1);
    }

    private onHitEnemyBullet(node: cc.Node) {
        node.destroy();
        this.loseHp(1);
    }

    private loseHp(n: number) {
        this.hp -= n;

        G.bulletDie = true;
        G.lazeShip = false;
        if (this.hpBar) {
            this.hpBar.setPercent(this.hp / this.HPMAX);
        }
        this.node.opacity = 0;

        this.isDie = true;
        this.explosion();
        G.enableMouse = true;
        this.showLife();
        // if (this.hp <= 0) {
        //     this.menu.active = true;
        //     this.logo.opacity = 0;
        //     this.replay.active = true;
        // }
    }

    private onShipDie() {
        this.isDie = true;
        this.explosion();
        if (this.onDie) {
            this.onDie();
        }
    }

    showLife() {
        cc.audioEngine.playEffect(G.flyDie, false);
        this.node.setPosition(cc.v2(0, -598));
        this.node.opacity = 255;
        this.shieldEffect.opacity = 255;
        this.enableProtect = true;
        G.touchPos = null;
        this.scheduleOnce(() => {
            G.enableMouse = false;
            this.enableMove = true;
        }, 0.3)
        this.node.runAction(cc.sequence(cc.moveTo(0.3, cc.v2(0, -270)).easing(cc.easeQuarticActionIn()), cc.callFunc(() => {
            this.getComponent(cc.CircleCollider).enabled = true;
            this.scheduleOnce(() => {
                this.enableProtect = false;
                this.shieldEffect.opacity = 0;
            }, 0.5)
            G.touchPos = null;
            G.bulletDie = false;
        })));
    }

    private explosion() {
        var efx = cc.instantiate(this.efxPrefab);
        efx.parent = this.node.parent;
        efx.x = this.node.x;
        efx.y = this.node.y;
        this.node.opacity = 0;
        this.enableBullet = false;
        this.enableMove = false;
        this.getComponent(cc.CircleCollider).enabled = false;
    }

    public revive() {
        this.enableProtect = true;
        G.touchPos = null;
        this.node.setPosition(this.startPoint);
        this.node.opacity = 255;
        this.enableBullet = true;

        this.getComponent(cc.CircleCollider).enabled = true;
        this.isDie = false;
        this.scheduleOnce(() => {
            this.enableProtect = false;
        }, 1);
        this.hp = this.HPMAX;
        if (this.onRevive) {
            this.onRevive();
        }
    }

    flyOut(done: Function = null) {
        G.checkWing = false;
        G.touchPos = null;
        this.enableMove = false;
        this.enableProtect = true;
        this.enableBullet = false;
        this.node.runAction(cc.sequence(cc.spawn(
            cc.moveBy(1.1, 0, 2000).easing(cc.easeBackIn()), cc.fadeOut(1.1)),
            cc.callFunc(() => {
                this.enableMove = false;
                G.touchPos = null;
                if (done)
                    done();
            })
        ));
    }

}