import G from "./Global";
const { ccclass, property } = cc._decorator;

@ccclass
export default class SpawnItem extends cc.Component {


    @property(cc.Prefab)
    item: cc.Prefab[] = [];
    check1: number = 0;
    check2: number = 0;
    check3: number = 0;

    @property(cc.Prefab)
    txt: cc.Prefab[] = [];
    // LIFE-CYCLE CALLBACKS:
    txt2: boolean = false;
    txt1: boolean = false;

    start() {
        this.updateItem();
    }

    spawnItem() {
        this.scheduleOnce(() => {
            var newItem = cc.instantiate(this.item[0]);
            newItem.parent = this.node;
            newItem.zIndex = 1;
            newItem.x = (this.node.x - 100) * (2 * Math.random() - 1);
            newItem.y = (this.node.y - 20) * Math.random();

            if (!this.txt1) {
                this.txt1 = true;
                var newItem1 = cc.instantiate(this.txt[0]);
                newItem1.parent = this.node;
                newItem1.zIndex = 1;
                newItem1.x = -20;
                newItem1.y = -150;
                newItem1.runAction(cc.sequence(cc.moveBy(2, cc.v2(0, -80)),
                    cc.callFunc(() => {
                        newItem1.destroy();
                    })));
            }
        }, 3)

        // if (this.check1 <= 1) {
        //     this.check1++;
        //     var newItem = cc.instantiate(this.item[0]);
        //     newItem.parent = this.node;
        //     newItem.zIndex = 10;
        //     newItem.x = (this.node.x - 90) * (2 * Math.random() - 1);
        //     newItem.y = (this.node.y - 20) * Math.random();

        //     if (!this.txt2) {
        //         this.txt2 = true;
        //         var newItem1 = cc.instantiate(this.txt[0]);
        //         newItem1.parent = this.node;
        //         newItem1.zIndex = 1;
        //         newItem1.x = -20;
        //         newItem1.y = -150;
        //         newItem1.runAction(cc.sequence(cc.moveBy(2, cc.v2(0, -80)),
        //             cc.callFunc(() => {
        //                 newItem1.destroy();
        //             })));
        //     }
        // }

        if (this.check2 <= 0) {
            this.check2++;
            this.scheduleOnce(() => {
                var newItem = cc.instantiate(this.item[1]);
                newItem.parent = this.node;
                newItem.zIndex = 10;
                newItem.x = 0
                newItem.y = -80

                var newItem1 = cc.instantiate(this.txt[1]);
                newItem1.parent = this.node;
                newItem1.zIndex = 1;
                newItem1.x = -40;
                newItem1.y = -150;
                newItem1.runAction(cc.sequence(cc.moveBy(2, cc.v2(0, -80)),
                    cc.callFunc(() => {
                        newItem1.destroy();
                    })));

                this.scheduleOnce(() => {
                    this.check3++;
                    var newItem = cc.instantiate(this.item[0]);
                    newItem.parent = this.node;
                    newItem.zIndex = 10;
                    newItem.x = (this.node.x - 90) * (2 * Math.random() - 1);
                    newItem.y = (this.node.y - 20) * Math.random();
                    this.scheduleOnce(() => {
                        this.check3++;
                        var newItem = cc.instantiate(this.item[0]);
                        newItem.parent = this.node;
                        newItem.zIndex = 10;
                        newItem.x = (this.node.x - 90) * (2 * Math.random() - 1);
                        newItem.y = (this.node.y - 20) * Math.random();

                        this.scheduleOnce(() => {
                            this.check3++;
                            var newItem = cc.instantiate(this.item[0]);
                            newItem.parent = this.node;
                            newItem.zIndex = 10;
                            newItem.x = (this.node.x - 90) * (2 * Math.random() - 1);
                            newItem.y = (this.node.y - 20) * Math.random();

                        }, 3.5)
                    }, 5)
                }, 5)
            }, 18)
        }
    }

    updateItem() {
        this.schedule(() => {
            if (!G.endG && G.startSpawnItem) {
                this.spawnItem();
            }
        }, 10, cc.macro.REPEAT_FOREVER, 0.01);
    }
}
