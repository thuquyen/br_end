

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    onLoad () {
        this.node.scale = 0;
        this.node.runAction(cc.sequence(cc.scaleTo(0.2, 1), cc.moveBy(25, cc.v2(0, -2000))));
    }

    start () {

    }
    // update (dt) {}
}
