import G from "./Global";
import HPBar from "./HpBar";
import BasePool from "./common/BasePool";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ShipController extends cc.Component {

    @property()
    private enableMove: boolean = true;
    // @property()
    private enableBullet: boolean = false;
    private enableProtect: boolean = false;

    @property(BasePool)
    private bulletPool: BasePool = null;

    @property()
    private fireRate = 150;

    private fireTime = 0;

    bulletShip: cc.Prefab = null;

    @property(cc.Prefab)
    private efxPrefab: cc.Prefab = null;

    @property(cc.Vec2)
    private startPoint: cc.Vec2 = null;

    @property()
    private minY = -500;

    public onDie: Function = null;
    public onRevive: Function = null;
    public isDie: boolean = false;

    @property()
    private HPMAX = 1;
    private hp = 0;
    // @property(HPBar)
    public hpBar: HPBar = null;

    @property(Boolean)
    hackNoDie: boolean = false;

    @property(cc.Prefab)
    bullet16: cc.Prefab[] = [];
    bullet: cc.Prefab[] = [];

    @property(cc.Node)
    shieldEffect: cc.Node = null;

    time2S: boolean = false;

    onLoad() {
        this.bulletPool.prefab = this.bullet16[0];
        this.bullet = this.bullet16;
        this.node.zIndex = 1;
        this.hp = this.HPMAX;
        G.ship = this;
    }

    setEnableMove(enable: boolean) {
        this.enableMove = enable;
    }

    setEnableBullet(enable: boolean) {
        this.enableBullet = enable;
    }

    setEnableProtect(enable: boolean) {
        this.enableProtect = enable;
    }

    update(dt) {
        if (this.enableBullet) {
            this.updateBullet();
        }

        if (this.enableMove) {
            this.updateMove();
        }
    }

    lerp(a: number, b: number, r: number) {
        return a + (b - a) * r;
    }

    private updateMove() {
        if (G.touchPos != null) {
            this.node.x = this.lerp(this.node.x, G.touchPos.x, 0.3);
            this.node.y = this.lerp(this.node.y, G.touchPos.y + 20, 0.3);
            if (this.node.y < this.minY) this.node.y = this.minY;
        }
    }

    private updateBullet() {
        if (Date.now() > this.fireTime + this.fireRate) {
            this.fireBullet();
            this.fireTime = Date.now();
        }
    }


    private fireBullet() {
        var newBulletShip = this.bulletPool.createObject(cc.Canvas.instance.node);
        newBulletShip.x = this.node.x;
        newBulletShip.y = this.node.y;//50;
    }

    onCollisionEnter(other: cc.Collider, self: cc.Collider) {

        if (this.enableProtect)
            return;
        if (other.node.group == 'boss') {
            this.onHitBoss(other.node);
        }
        else if (other.node.group == 'eBullet' || other.node.group == 'e') {
            this.onHitEnemyBullet(other.node);
        }
    }

    private onHitBoss(node: cc.Node) {
        this.loseHp(1);
    }

    private onHitBossBullet(node: cc.Node) {
        node.destroy();
        this.loseHp(1);
    }

    private onHitEnemy(node: cc.Node) {
        this.loseHp(1);
    }

    private onHitEnemyBullet(node: cc.Node) {
        node.destroy();
        this.loseHp(1);
    }


    private loseHp(n: number) {
        G.bulletDie = true;
        G.lazeShip = false;
        this.hp -= n;
        if (this.hpBar) {
            this.hpBar.setPercent(this.hp / this.HPMAX);
        }

        // if (this.hp <= 0) {
        //     if (this.hackNoDie) {
        //         this.hp = this.HPMAX;
        //         this.hpBar.setPercent(this.hp / this.HPMAX);
        //     } else {
        //         this.hp = 0;
        this.onShipDie();

        //     }
        // }
    }

    private onShipDie() {
        this.node.setPosition(cc.v2(0, -598));
        this.showLife(this.hp);
        this.isDie = true;
        this.explosion();
        if (this.onDie) {
            this.onDie();
        }
    }

    showLife(n: number) {
        cc.audioEngine.playEffect(G.flyDie, false);
        this.enableMove = false;
        this.explosion();
        this.node.opacity = 0;
        // if (this.tim.children[n])
        //     this.tim.children[n].active = false;
        this.node.setPosition(cc.v2(0, -598));
        this.shieldEffect.active = true;
        this.shieldEffect.runAction(cc.spawn(cc.fadeIn(0.2), cc.scaleTo(0.2, 1)));
        this.enableProtect = true;
        this.node.runAction(cc.sequence(cc.moveTo(0.4, cc.v2(0, -280)).easing(cc.easeQuarticActionIn()), cc.callFunc(() => {
            this.node.opacity = 255;
            this.getComponent(cc.CircleCollider).enabled = true;
            this.scheduleOnce(() => {
                this.enableProtect = false;
                this.shieldEffect.runAction(cc.spawn(cc.fadeOut(0.3), cc.scaleTo(0.3, 0)));
                this.enableMove = true;
                this.enableBullet = true;
                G.touchPos = null;
            }, 1.5)
            G.bulletDie = false;
        })));
    }

    private explosion() {
        var efx = cc.instantiate(this.efxPrefab);
        efx.parent = this.node.parent;
        efx.x = this.node.x;
        efx.y = this.node.y;
        this.node.opacity = 0;
        this.enableBullet = false;
        this.enableMove = false;
        this.getComponent(cc.CircleCollider).enabled = false;
    }

    public revive() {
        this.enableProtect = true;
        G.touchPos = null;
        this.node.setPosition(this.startPoint);
        this.node.opacity = 255;
        // this.enableBullet = true;
        this.enableMove = true;
        this.getComponent(cc.CircleCollider).enabled = true;
        this.isDie = false;
        this.scheduleOnce(() => {
            this.enableProtect = false;
        }, 1);
        this.hp = this.HPMAX;
        if (this.onRevive) {
            this.onRevive();
        }
    }

    flyOut(done: Function = null) {
        G.checkWing = false;
        G.touchPos = null;
        this.enableMove = false;
        this.enableProtect = true;
        this.enableBullet = false;
        this.node.runAction(cc.sequence(cc.spawn(
            cc.moveBy(1.1, 0, 2000).easing(cc.easeBackIn()), cc.fadeOut(1.1)),
            cc.callFunc(() => {
                this.enableMove = false;
                this.destroy();
                G.touchPos = null;
                if (done)
                    done();
            })
        ));
    }
}