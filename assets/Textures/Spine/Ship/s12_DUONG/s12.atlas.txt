
s12.png
size: 1024,128
format: RGBA8888
filter: Linear,Linear
repeat: none
Valentine/1
  rotate: true
  xy: 991, 90
  size: 36, 30
  orig: 36, 30
  offset: 0, 0
  index: -1
Valentine/2
  rotate: true
  xy: 493, 2
  size: 43, 36
  orig: 43, 36
  offset: 0, 0
  index: -1
Valentine/3
  rotate: true
  xy: 577, 2
  size: 45, 39
  orig: 45, 39
  offset: 0, 0
  index: -1
Valentine/4
  rotate: true
  xy: 319, 2
  size: 18, 48
  orig: 18, 48
  offset: 0, 0
  index: -1
Valentine/5
  rotate: false
  xy: 531, 3
  size: 42, 42
  orig: 42, 42
  offset: 0, 0
  index: -1
Valentine/6
  rotate: true
  xy: 493, 47
  size: 28, 82
  orig: 28, 82
  offset: 0, 0
  index: -1
Valentine/7
  rotate: true
  xy: 2, 10
  size: 116, 158
  orig: 116, 158
  offset: 0, 0
  index: -1
Valentine/8
  rotate: false
  xy: 683, 36
  size: 98, 90
  orig: 98, 90
  offset: 0, 0
  index: -1
body
  rotate: true
  xy: 426, 77
  size: 49, 149
  orig: 49, 149
  offset: 0, 0
  index: -1
body2
  rotate: false
  xy: 883, 37
  size: 76, 89
  orig: 76, 89
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 319, 22
  size: 105, 104
  orig: 105, 104
  offset: 0, 0
  index: -1
ring
  rotate: false
  xy: 618, 5
  size: 42, 42
  orig: 42, 42
  offset: 0, 0
  index: -1
ring_tail
  rotate: false
  xy: 961, 44
  size: 28, 82
  orig: 28, 82
  offset: 0, 0
  index: -1
skin/Group 515 copy 3
  rotate: true
  xy: 369, 3
  size: 17, 34
  orig: 17, 34
  offset: 0, 0
  index: -1
skin/Group 535
  rotate: true
  xy: 577, 49
  size: 77, 104
  orig: 77, 104
  offset: 0, 0
  index: -1
skin/Layer 525 copy 3
  rotate: false
  xy: 991, 64
  size: 27, 24
  orig: 27, 24
  offset: 0, 0
  index: -1
skin/Layer 541
  rotate: true
  xy: 426, 26
  size: 49, 65
  orig: 49, 65
  offset: 0, 0
  index: -1
wing
  rotate: true
  xy: 162, 11
  size: 115, 155
  orig: 115, 155
  offset: 0, 0
  index: -1
wing2
  rotate: false
  xy: 783, 36
  size: 98, 90
  orig: 98, 90
  offset: 0, 0
  index: -1
