
ship13.png
size: 365,245
format: RGBA8888
filter: Linear,Linear
repeat: none
body
  rotate: false
  xy: 229, 33
  size: 42, 90
  orig: 42, 90
  offset: 0, 0
  index: -1
center1
  rotate: false
  xy: 273, 5
  size: 49, 79
  orig: 49, 79
  offset: 0, 0
  index: -1
center2
  rotate: false
  xy: 341, 113
  size: 21, 67
  orig: 21, 67
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 277, 86
  size: 53, 70
  orig: 53, 70
  offset: 0, 0
  index: -1
head4
  rotate: false
  xy: 0, 13
  size: 180, 232
  orig: 180, 232
  offset: 0, 0
  index: -1
light1
  rotate: false
  xy: 341, 182
  size: 24, 63
  orig: 24, 63
  offset: 0, 0
  index: -1
light2
  rotate: false
  xy: 324, 20
  size: 28, 45
  orig: 28, 45
  offset: 0, 0
  index: -1
light3
  rotate: false
  xy: 332, 67
  size: 32, 44
  orig: 32, 44
  offset: 0, 0
  index: -1
wing1
  rotate: false
  xy: 182, 93
  size: 39, 152
  orig: 39, 152
  offset: 0, 0
  index: -1
wing2
  rotate: false
  xy: 223, 125
  size: 52, 120
  orig: 52, 120
  offset: 0, 0
  index: -1
wing3
  rotate: false
  xy: 277, 158
  size: 62, 87
  orig: 62, 87
  offset: 0, 0
  index: -1
wing4
  rotate: false
  xy: 182, 0
  size: 45, 91
  orig: 45, 91
  offset: 0, 0
  index: -1
