
F2 Ship21.png
size: 519,142
format: RGBA8888
filter: Linear,Linear
repeat: none
1/11
  rotate: false
  xy: 448, 19
  size: 16, 15
  orig: 16, 15
  offset: 0, 0
  index: -1
1/1body
  rotate: false
  xy: 265, 0
  size: 25, 78
  orig: 25, 78
  offset: 0, 0
  index: -1
1/1deco
  rotate: false
  xy: 403, 35
  size: 41, 48
  orig: 41, 48
  offset: 0, 0
  index: -1
1/1duoi
  rotate: false
  xy: 174, 3
  size: 33, 19
  orig: 33, 19
  offset: 0, 0
  index: -1
1/1light deco
  rotate: false
  xy: 327, 84
  size: 57, 58
  orig: 57, 58
  offset: 0, 0
  index: -1
1/1ongP
  rotate: false
  xy: 155, 1
  size: 17, 43
  orig: 17, 43
  offset: 0, 0
  index: -1
1/1sungP
  rotate: false
  xy: 209, 5
  size: 21, 23
  orig: 21, 23
  offset: 0, 0
  index: -1
1/1wing
  rotate: false
  xy: 292, 9
  size: 36, 69
  orig: 36, 69
  offset: 0, 0
  index: -1
1/1wing base
  rotate: false
  xy: 0, 2
  size: 52, 41
  orig: 52, 41
  offset: 0, 0
  index: -1
2/1body
  rotate: false
  xy: 105, 44
  size: 44, 98
  orig: 44, 98
  offset: 0, 0
  index: -1
2/1deco
  rotate: false
  xy: 330, 29
  size: 45, 53
  orig: 45, 53
  offset: 0, 0
  index: -1
2/1duoi
  rotate: false
  xy: 330, 2
  size: 43, 25
  orig: 43, 25
  offset: 0, 0
  index: -1
2/1duoi1
  rotate: false
  xy: 54, 9
  size: 49, 37
  orig: 49, 37
  offset: 0, 0
  index: -1
2/1light deco
  rotate: false
  xy: 265, 80
  size: 60, 62
  orig: 60, 62
  offset: 0, 0
  index: -1
2/1ong xa
  rotate: false
  xy: 105, 0
  size: 23, 42
  orig: 23, 42
  offset: 0, 0
  index: -1
2/1ongP
  rotate: false
  xy: 377, 24
  size: 24, 58
  orig: 24, 58
  offset: 0, 0
  index: -1
2/1sungP
  rotate: false
  xy: 375, 3
  size: 22, 19
  orig: 22, 19
  offset: 0, 0
  index: -1
2/1wing
  rotate: false
  xy: 0, 45
  size: 52, 97
  orig: 52, 97
  offset: 0, 0
  index: -1
3/11
  rotate: false
  xy: 174, 24
  size: 20, 20
  orig: 20, 20
  offset: 0, 0
  index: -1
3/1base deco
  rotate: false
  xy: 437, 92
  size: 50, 50
  orig: 50, 50
  offset: 0, 0
  index: -1
3/1body
  rotate: false
  xy: 151, 46
  size: 43, 96
  orig: 43, 96
  offset: 0, 0
  index: -1
3/1deco
  rotate: false
  xy: 386, 85
  size: 49, 57
  orig: 49, 57
  offset: 0, 0
  index: -1
3/1duoi
  rotate: false
  xy: 470, 6
  size: 47, 36
  orig: 47, 36
  offset: 0, 0
  index: -1
3/1duoi1
  rotate: false
  xy: 470, 44
  size: 49, 37
  orig: 49, 37
  offset: 0, 0
  index: -1
3/1duoi2
  rotate: false
  xy: 446, 36
  size: 22, 54
  orig: 22, 54
  offset: 0, 0
  index: -1
3/1light deco
  rotate: false
  xy: 196, 73
  size: 67, 69
  orig: 67, 69
  offset: 0, 0
  index: -1
3/1ong xa
  rotate: false
  xy: 130, 1
  size: 23, 41
  orig: 23, 41
  offset: 0, 0
  index: -1
3/1ongP
  rotate: false
  xy: 489, 83
  size: 29, 59
  orig: 29, 59
  offset: 0, 0
  index: -1
3/1sungP
  rotate: false
  xy: 232, 9
  size: 22, 19
  orig: 22, 19
  offset: 0, 0
  index: -1
3/1wing
  rotate: false
  xy: 54, 48
  size: 49, 94
  orig: 49, 94
  offset: 0, 0
  index: -1
3/1wing base
  rotate: false
  xy: 196, 30
  size: 52, 41
  orig: 52, 41
  offset: 0, 0
  index: -1
3/1wing2
  rotate: false
  xy: 403, 1
  size: 43, 32
  orig: 43, 32
  offset: 0, 0
  index: -1
