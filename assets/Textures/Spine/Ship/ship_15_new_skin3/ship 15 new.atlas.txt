
ship 15 new.png
size: 3966,1960
format: RGBA8888
filter: Linear,Linear
repeat: none
3/1body
  rotate: false
  xy: 1843, 4
  size: 463, 478
  orig: 463, 478
  offset: 0, 0
  index: -1
3/1tail
  rotate: false
  xy: 3494, 1235
  size: 467, 725
  orig: 467, 725
  offset: 0, 0
  index: -1
3/1wing1
  rotate: false
  xy: 3707, 551
  size: 259, 682
  orig: 259, 682
  offset: 0, 0
  index: -1
3/1wing3
  rotate: false
  xy: 2622, 902
  size: 434, 1058
  orig: 434, 1058
  offset: 0, 0
  index: -1
6/1armor
  rotate: false
  xy: 2622, 0
  size: 588, 900
  orig: 588, 900
  offset: 0, 0
  index: -1
6/1head
  rotate: false
  xy: 0, 259
  size: 1841, 1701
  orig: 1841, 1701
  offset: 0, 0
  index: -1
6/1horn
  rotate: false
  xy: 2308, 124
  size: 309, 358
  orig: 309, 358
  offset: 0, 0
  index: -1
6/1wing3
  rotate: false
  xy: 3058, 902
  size: 434, 1058
  orig: 434, 1058
  offset: 0, 0
  index: -1
6/1wing4
  rotate: false
  xy: 1843, 484
  size: 777, 1476
  orig: 777, 1476
  offset: 0, 0
  index: -1
6/1wing5
  rotate: false
  xy: 3212, 43
  size: 493, 857
  orig: 493, 857
  offset: 0, 0
  index: -1
