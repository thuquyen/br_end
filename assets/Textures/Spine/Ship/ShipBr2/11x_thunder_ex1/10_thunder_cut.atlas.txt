
10_thunder_cut.png
size: 241,210
format: RGBA8888
filter: Linear,Linear
repeat: none
1
  rotate: false
  xy: 166, 108
  size: 42, 102
  orig: 42, 102
  offset: 0, 0
  index: -1
10
  rotate: false
  xy: 210, 56
  size: 22, 68
  orig: 22, 68
  offset: 0, 0
  index: -1
11
  rotate: false
  xy: 0, 31
  size: 60, 60
  orig: 60, 60
  offset: 0, 0
  index: -1
12
  rotate: false
  xy: 144, 64
  size: 42, 42
  orig: 42, 42
  offset: 0, 0
  index: -1
13
  rotate: false
  xy: 62, 1
  size: 32, 31
  orig: 32, 31
  offset: 0, 0
  index: -1
2
  rotate: false
  xy: 210, 126
  size: 31, 84
  orig: 31, 84
  offset: 0, 0
  index: -1
3
  rotate: false
  xy: 0, 93
  size: 60, 117
  orig: 60, 117
  offset: 0, 0
  index: -1
4
  rotate: false
  xy: 62, 108
  size: 102, 102
  orig: 102, 102
  offset: 0, 0
  index: -1
5
  rotate: false
  xy: 103, 46
  size: 39, 60
  orig: 39, 60
  offset: 0, 0
  index: -1
6
  rotate: false
  xy: 103, 2
  size: 32, 42
  orig: 32, 42
  offset: 0, 0
  index: -1
7
  rotate: false
  xy: 62, 34
  size: 39, 72
  orig: 39, 72
  offset: 0, 0
  index: -1
8
  rotate: false
  xy: 144, 5
  size: 41, 49
  orig: 41, 49
  offset: 0, 0
  index: -1
9
  rotate: false
  xy: 187, 0
  size: 21, 54
  orig: 21, 54
  offset: 0, 0
  index: -1
m
  rotate: false
  xy: 234, 107
  size: 7, 17
  orig: 7, 17
  offset: 0, 0
  index: -1
