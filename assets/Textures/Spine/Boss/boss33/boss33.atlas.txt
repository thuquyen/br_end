
boss33.png
size: 2048,2048
format: RGBA8888
filter: Linear,Linear
repeat: none
1
  rotate: false
  xy: 1685, 1189
  size: 217, 309
  orig: 217, 309
  offset: 0, 0
  index: -1
2
  rotate: false
  xy: 934, 38
  size: 215, 312
  orig: 215, 312
  offset: 0, 0
  index: -1
3
  rotate: false
  xy: 1685, 1500
  size: 361, 359
  orig: 361, 359
  offset: 0, 0
  index: -1
back_gun
  rotate: false
  xy: 0, 16
  size: 259, 82
  orig: 259, 82
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 0, 769
  size: 1072, 1090
  orig: 1072, 1090
  offset: 0, 0
  index: -1
body2
  rotate: false
  xy: 0, 100
  size: 666, 667
  orig: 666, 667
  offset: 0, 0
  index: -1
eye
  rotate: false
  xy: 1074, 1249
  size: 609, 610
  orig: 609, 610
  offset: 0, 0
  index: -1
eye2
  rotate: false
  xy: 668, 352
  size: 366, 415
  orig: 366, 415
  offset: 0, 0
  index: -1
gun
  rotate: false
  xy: 1074, 949
  size: 466, 298
  orig: 466, 298
  offset: 0, 0
  index: -1
gun_top
  rotate: false
  xy: 668, 0
  size: 264, 350
  orig: 264, 350
  offset: 0, 0
  index: -1
