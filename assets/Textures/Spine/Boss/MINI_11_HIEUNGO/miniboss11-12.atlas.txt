
miniboss11-12.png
size: 182,281
format: RGBA8888
filter: Linear,Linear
repeat: none
mini boss11/arm
  rotate: false
  xy: 66, 26
  size: 53, 57
  orig: 53, 57
  offset: 0, 0
  index: -1
mini boss11/body
  rotate: false
  xy: 106, 145
  size: 63, 76
  orig: 63, 76
  offset: 0, 0
  index: -1
mini boss11/hand
  rotate: false
  xy: 0, 57
  size: 64, 89
  orig: 64, 89
  offset: 0, 0
  index: -1
mini boss11/head
  rotate: false
  xy: 106, 223
  size: 76, 58
  orig: 76, 58
  offset: 0, 0
  index: -1
mini boss11/light
  rotate: false
  xy: 0, 0
  size: 56, 55
  orig: 56, 55
  offset: 0, 0
  index: -1
mini boss11/mouth
  rotate: false
  xy: 121, 51
  size: 26, 32
  orig: 26, 32
  offset: 0, 0
  index: -1
mini boss11/shoulder
  rotate: false
  xy: 66, 85
  size: 67, 58
  orig: 67, 58
  offset: 0, 0
  index: -1
mini boss11/throne
  rotate: false
  xy: 149, 60
  size: 31, 31
  orig: 31, 31
  offset: 0, 0
  index: -1
mini boss11/wing
  rotate: false
  xy: 0, 148
  size: 104, 133
  orig: 104, 133
  offset: 0, 0
  index: -1
mini boss11/wing_small
  rotate: false
  xy: 135, 93
  size: 45, 50
  orig: 45, 50
  offset: 0, 0
  index: -1
