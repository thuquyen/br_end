
boss35.png
size: 255,523
format: RGBA8888
filter: Linear,Linear
repeat: none
Arm
  rotate: false
  xy: 127, 116
  size: 38, 16
  orig: 38, 16
  offset: 0, 0
  index: -1
Blade
  rotate: false
  xy: 72, 192
  size: 41, 234
  orig: 41, 234
  offset: 0, 0
  index: -1
Body
  rotate: false
  xy: 115, 331
  size: 128, 95
  orig: 128, 95
  offset: 0, 0
  index: -1
Body_back
  rotate: false
  xy: 115, 231
  size: 65, 98
  orig: 65, 98
  offset: 0, 0
  index: -1
Door
  rotate: false
  xy: 0, 0
  size: 45, 34
  orig: 45, 34
  offset: 0, 0
  index: -1
Ear
  rotate: false
  xy: 42, 107
  size: 23, 95
  orig: 23, 95
  offset: 0, 0
  index: -1
Finger
  rotate: false
  xy: 127, 103
  size: 26, 11
  orig: 26, 11
  offset: 0, 0
  index: -1
Gun4
  rotate: false
  xy: 127, 134
  size: 46, 72
  orig: 46, 72
  offset: 0, 0
  index: -1
Laser
  rotate: false
  xy: 198, 52
  size: 33, 51
  orig: 33, 51
  offset: 0, 0
  index: -1
Mask
  rotate: false
  xy: 228, 156
  size: 25, 43
  orig: 25, 43
  offset: 0, 0
  index: -1
Node_0
  rotate: false
  xy: 233, 473
  size: 18, 23
  orig: 18, 23
  offset: 0, 0
  index: -1
Pipe
  rotate: false
  xy: 198, 1
  size: 40, 49
  orig: 40, 49
  offset: 0, 0
  index: -1
Saw
  rotate: false
  xy: 194, 201
  size: 55, 55
  orig: 55, 55
  offset: 0, 0
  index: -1
Shoulder
  rotate: false
  xy: 115, 208
  size: 77, 21
  orig: 77, 21
  offset: 0, 0
  index: -1
Wing2
  rotate: false
  xy: 0, 204
  size: 70, 222
  orig: 70, 222
  offset: 0, 0
  index: -1
door_1
  rotate: false
  xy: 99, 17
  size: 29, 17
  orig: 29, 17
  offset: 0, 0
  index: -1
door_2
  rotate: false
  xy: 74, 10
  size: 23, 24
  orig: 23, 24
  offset: 0, 0
  index: -1
door_3
  rotate: false
  xy: 130, 17
  size: 29, 17
  orig: 29, 17
  offset: 0, 0
  index: -1
door_4
  rotate: false
  xy: 233, 498
  size: 22, 25
  orig: 22, 25
  offset: 0, 0
  index: -1
face
  rotate: false
  xy: 182, 258
  size: 73, 71
  orig: 73, 71
  offset: 0, 0
  index: -1
gun
  rotate: false
  xy: 67, 101
  size: 58, 89
  orig: 58, 89
  offset: 0, 0
  index: -1
gun_wing1
  rotate: false
  xy: 175, 105
  size: 44, 36
  orig: 44, 36
  offset: 0, 0
  index: -1
moc
  rotate: false
  xy: 175, 143
  size: 51, 56
  orig: 51, 56
  offset: 0, 0
  index: -1
rocket_wing
  rotate: false
  xy: 233, 449
  size: 18, 22
  orig: 18, 22
  offset: 0, 0
  index: -1
vay
  rotate: false
  xy: 47, 1
  size: 25, 33
  orig: 25, 33
  offset: 0, 0
  index: -1
wing
  rotate: false
  xy: 0, 428
  size: 231, 95
  orig: 231, 95
  offset: 0, 0
  index: -1
wing1_back
  rotate: false
  xy: 42, 36
  size: 154, 63
  orig: 154, 63
  offset: 0, 0
  index: -1
wing2_back
  rotate: false
  xy: 0, 47
  size: 40, 155
  orig: 40, 155
  offset: 0, 0
  index: -1
