
boss 004.png
size: 388,384
format: RGBA8888
filter: Linear,Linear
repeat: none
body
  rotate: true
  xy: 146, 297
  size: 86, 140
  orig: 86, 140
  offset: 0, 0
  index: -1
hary
  rotate: true
  xy: 1, 3
  size: 25, 64
  orig: 25, 64
  offset: 0, 0
  index: -1
head
  rotate: true
  xy: 297, 134
  size: 79, 81
  orig: 79, 81
  offset: 0, 0
  index: -1
head2
  rotate: false
  xy: 146, 246
  size: 102, 49
  orig: 102, 49
  offset: 0, 0
  index: -1
horn
  rotate: true
  xy: 1, 30
  size: 30, 204
  orig: 30, 204
  offset: 0, 0
  index: -1
jaw
  rotate: false
  xy: 126, 71
  size: 38, 41
  orig: 38, 41
  offset: 0, 0
  index: -1
tongue
  rotate: false
  xy: 297, 215
  size: 79, 168
  orig: 79, 168
  offset: 0, 0
  index: -1
vai copy
  rotate: true
  xy: 1, 62
  size: 160, 123
  orig: 160, 123
  offset: 0, 0
  index: -1
wing1
  rotate: false
  xy: 126, 114
  size: 169, 108
  orig: 169, 108
  offset: 0, 0
  index: -1
wing2
  rotate: false
  xy: 1, 224
  size: 143, 159
  orig: 143, 159
  offset: 0, 0
  index: -1
wing3
  rotate: true
  xy: 207, 10
  size: 102, 178
  orig: 102, 178
  offset: 0, 0
  index: -1
