
A.png
size: 2048,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
1
  rotate: false
  xy: 381, 44
  size: 158, 148
  orig: 158, 148
  offset: 0, 0
  index: -1
2
  rotate: false
  xy: 408, 194
  size: 169, 302
  orig: 169, 302
  offset: 0, 0
  index: -1
Armor_inside
  rotate: false
  xy: 579, 161
  size: 167, 378
  orig: 167, 378
  offset: 0, 0
  index: -1
Armor_outside
  rotate: false
  xy: 1476, 456
  size: 215, 548
  orig: 215, 548
  offset: 0, 0
  index: -1
Decor
  rotate: false
  xy: 748, 168
  size: 198, 170
  orig: 198, 170
  offset: 0, 0
  index: -1
Decor2
  rotate: false
  xy: 1693, 669
  size: 272, 335
  orig: 272, 335
  offset: 0, 0
  index: -1
back
  rotate: false
  xy: 1239, 457
  size: 235, 547
  orig: 235, 547
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 85, 498
  size: 395, 506
  orig: 395, 506
  offset: 0, 0
  index: -1
body2
  rotate: false
  xy: 482, 541
  size: 428, 463
  orig: 428, 463
  offset: 0, 0
  index: -1
face
  rotate: false
  xy: 1693, 479
  size: 275, 188
  orig: 275, 188
  offset: 0, 0
  index: -1
face2
  rotate: false
  xy: 85, 199
  size: 321, 297
  orig: 321, 297
  offset: 0, 0
  index: -1
gun
  rotate: false
  xy: 273, 78
  size: 106, 119
  orig: 106, 119
  offset: 0, 0
  index: -1
gun2
  rotate: false
  xy: 1172, 480
  size: 48, 74
  orig: 48, 74
  offset: 0, 0
  index: -1
hat
  rotate: false
  xy: 912, 556
  size: 325, 448
  orig: 325, 448
  offset: 0, 0
  index: -1
hip
  rotate: false
  xy: 85, 67
  size: 186, 130
  orig: 186, 130
  offset: 0, 0
  index: -1
hip2
  rotate: false
  xy: 917, 356
  size: 253, 198
  orig: 253, 198
  offset: 0, 0
  index: -1
leg
  rotate: false
  xy: 1099, 192
  size: 138, 162
  orig: 138, 162
  offset: 0, 0
  index: -1
leg2
  rotate: false
  xy: 948, 179
  size: 149, 175
  orig: 149, 175
  offset: 0, 0
  index: -1
tail
  rotate: false
  xy: 1970, 261
  size: 39, 743
  orig: 39, 743
  offset: 0, 0
  index: -1
tail2
  rotate: false
  xy: 0, 114
  size: 83, 890
  orig: 83, 890
  offset: 0, 0
  index: -1
tail_top
  rotate: false
  xy: 1172, 411
  size: 53, 67
  orig: 53, 67
  offset: 0, 0
  index: -1
tail_top2
  rotate: false
  xy: 0, 0
  size: 77, 112
  orig: 77, 112
  offset: 0, 0
  index: -1
wing2
  rotate: false
  xy: 748, 340
  size: 167, 199
  orig: 167, 199
  offset: 0, 0
  index: -1
