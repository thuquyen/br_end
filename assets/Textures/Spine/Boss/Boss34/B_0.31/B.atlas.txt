
B.png
size: 265,240
format: RGBA8888
filter: Linear,Linear
repeat: none
Decor
  rotate: false
  xy: 155, 45
  size: 61, 53
  orig: 61, 53
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 14, 83
  size: 122, 157
  orig: 122, 157
  offset: 0, 0
  index: -1
face
  rotate: false
  xy: 14, 23
  size: 85, 58
  orig: 85, 58
  offset: 0, 0
  index: -1
gun
  rotate: false
  xy: 215, 6
  size: 33, 37
  orig: 33, 37
  offset: 0, 0
  index: -1
gun2
  rotate: false
  xy: 250, 23
  size: 15, 23
  orig: 15, 23
  offset: 0, 0
  index: -1
hat
  rotate: false
  xy: 138, 100
  size: 101, 140
  orig: 101, 140
  offset: 0, 0
  index: -1
hip
  rotate: false
  xy: 155, 3
  size: 58, 40
  orig: 58, 40
  offset: 0, 0
  index: -1
leg
  rotate: false
  xy: 218, 48
  size: 43, 50
  orig: 43, 50
  offset: 0, 0
  index: -1
tail
  rotate: false
  xy: 0, 10
  size: 12, 230
  orig: 12, 230
  offset: 0, 0
  index: -1
tail_top
  rotate: false
  xy: 14, 0
  size: 16, 21
  orig: 16, 21
  offset: 0, 0
  index: -1
wing2
  rotate: false
  xy: 101, 19
  size: 52, 62
  orig: 52, 62
  offset: 0, 0
  index: -1
