
muc.png
size: 512,512
format: RGBA8888
filter: Linear,Linear
repeat: none
body
  rotate: false
  xy: 260, 402
  size: 60, 74
  orig: 60, 74
  offset: 0, 0
  index: -1
light
  rotate: false
  xy: 0, 0
  size: 215, 216
  orig: 215, 216
  offset: 0, 0
  index: -1
ring
  rotate: false
  xy: 0, 218
  size: 258, 258
  orig: 258, 258
  offset: 0, 0
  index: -1
tail
  rotate: false
  xy: 217, 151
  size: 16, 65
  orig: 16, 65
  offset: 0, 0
  index: -1
