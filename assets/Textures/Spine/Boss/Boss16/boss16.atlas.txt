
boss16.png
size: 288,288
format: RGBA8888
filter: Linear,Linear
repeat: none
arm
  rotate: false
  xy: 210, 59
  size: 76, 40
  orig: 76, 40
  offset: 0, 0
  index: -1
armor
  rotate: true
  xy: 126, 149
  size: 40, 113
  orig: 40, 113
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 2, 191
  size: 246, 95
  orig: 246, 95
  offset: 0, 0
  index: -1
deco
  rotate: false
  xy: 236, 13
  size: 43, 44
  orig: 43, 44
  offset: 0, 0
  index: -1
face
  rotate: false
  xy: 134, 55
  size: 74, 44
  orig: 74, 44
  offset: 0, 0
  index: -1
fire
  rotate: true
  xy: 187, 5
  size: 48, 47
  orig: 48, 47
  offset: 0, 0
  index: -1
foot
  rotate: false
  xy: 2, 58
  size: 62, 131
  orig: 62, 131
  offset: 0, 0
  index: -1
hair
  rotate: false
  xy: 250, 224
  size: 26, 62
  orig: 26, 62
  offset: 0, 0
  index: -1
hand
  rotate: false
  xy: 66, 87
  size: 58, 102
  orig: 58, 102
  offset: 0, 0
  index: -1
leg
  rotate: true
  xy: 73, 5
  size: 80, 59
  orig: 80, 59
  offset: 0, 0
  index: -1
light
  rotate: true
  xy: 126, 101
  size: 46, 101
  orig: 46, 101
  offset: 0, 0
  index: -1
mouth
  rotate: false
  xy: 134, 2
  size: 51, 51
  orig: 51, 51
  offset: 0, 0
  index: -1
pipe
  rotate: false
  xy: 2, 6
  size: 69, 50
  orig: 69, 50
  offset: 0, 0
  index: -1
