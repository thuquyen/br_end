
boss 03.png
size: 224,224
format: RGBA8888
filter: Linear,Linear
repeat: none
arm1
  rotate: true
  xy: 189, 71
  size: 26, 13
  orig: 26, 13
  offset: 0, 0
  index: -1
arm2
  rotate: false
  xy: 163, 47
  size: 23, 16
  orig: 23, 16
  offset: 0, 0
  index: -1
arm3
  rotate: false
  xy: 169, 95
  size: 18, 29
  orig: 18, 29
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 57, 41
  size: 49, 83
  orig: 49, 83
  offset: 0, 0
  index: -1
bread
  rotate: false
  xy: 189, 129
  size: 31, 91
  orig: 31, 91
  offset: 0, 0
  index: -1
eye
  rotate: false
  xy: 204, 77
  size: 18, 20
  orig: 18, 20
  offset: 0, 0
  index: -1
face
  rotate: false
  xy: 82, 5
  size: 22, 34
  orig: 22, 34
  offset: 0, 0
  index: -1
horn
  rotate: false
  xy: 2, 41
  size: 53, 83
  orig: 53, 83
  offset: 0, 0
  index: -1
horn2
  rotate: true
  xy: 108, 43
  size: 20, 53
  orig: 20, 53
  offset: 0, 0
  index: -1
jaw
  rotate: false
  xy: 189, 99
  size: 19, 28
  orig: 19, 28
  offset: 0, 0
  index: -1
neck
  rotate: false
  xy: 169, 65
  size: 18, 28
  orig: 18, 28
  offset: 0, 0
  index: -1
neck back
  rotate: true
  xy: 2, 5
  size: 34, 78
  orig: 34, 78
  offset: 0, 0
  index: -1
neck back2
  rotate: false
  xy: 108, 65
  size: 59, 59
  orig: 59, 59
  offset: 0, 0
  index: -1
wing
  rotate: false
  xy: 2, 126
  size: 185, 94
  orig: 185, 94
  offset: 0, 0
  index: -1
