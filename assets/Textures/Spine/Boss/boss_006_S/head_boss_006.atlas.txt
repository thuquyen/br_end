
head_boss_006.png
size: 204,204
format: RGBA8888
filter: Linear,Linear
repeat: none
head
  rotate: false
  xy: 1, 99
  size: 127, 104
  orig: 127, 104
  offset: 0, 0
  index: -1
horn1
  rotate: false
  xy: 1, 1
  size: 114, 96
  orig: 114, 96
  offset: 0, 0
  index: -1
jaw
  rotate: true
  xy: 117, 5
  size: 92, 80
  orig: 92, 80
  offset: 0, 0
  index: -1
