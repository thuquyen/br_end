
boss halloween.png
size: 512,512
format: RGBA8888
filter: Linear,Linear
repeat: none
ao choang
  rotate: false
  xy: 289, 365
  size: 122, 30
  orig: 122, 30
  offset: 0, 0
  index: -1
chan
  rotate: true
  xy: 435, 407
  size: 103, 75
  orig: 103, 75
  offset: 0, 0
  index: -1
co
  rotate: false
  xy: 289, 316
  size: 90, 19
  orig: 90, 19
  offset: 0, 0
  index: -1
co tay phai
  rotate: false
  xy: 2, 74
  size: 105, 164
  orig: 105, 164
  offset: 0, 0
  index: -1
dong tu trai
  rotate: false
  xy: 413, 378
  size: 17, 17
  orig: 17, 17
  offset: 0, 0
  index: -1
flare
  rotate: false
  xy: 211, 201
  size: 58, 37
  orig: 58, 37
  offset: 0, 0
  index: -1
flare mat phai
  rotate: false
  xy: 165, 137
  size: 42, 27
  orig: 42, 27
  offset: 0, 0
  index: -1
flare mat trai
  rotate: true
  xy: 77, 30
  size: 42, 30
  orig: 42, 30
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 289, 397
  size: 144, 113
  orig: 144, 113
  offset: 0, 0
  index: -1
khuyu tay phai
  rotate: false
  xy: 109, 95
  size: 54, 69
  orig: 54, 69
  offset: 0, 0
  index: -1
luoi hai
  rotate: false
  xy: 2, 240
  size: 285, 270
  orig: 285, 270
  offset: 0, 0
  index: -1
mieng
  rotate: false
  xy: 289, 337
  size: 110, 26
  orig: 110, 26
  offset: 0, 0
  index: -1
mui
  rotate: false
  xy: 381, 319
  size: 15, 16
  orig: 15, 16
  offset: 0, 0
  index: -1
tay trai
  rotate: true
  xy: 2, 3
  size: 69, 73
  orig: 69, 73
  offset: 0, 0
  index: -1
than
  rotate: false
  xy: 109, 166
  size: 100, 72
  orig: 100, 72
  offset: 0, 0
  index: -1
trong mat phai
  rotate: false
  xy: 77, 2
  size: 35, 26
  orig: 35, 26
  offset: 0, 0
  index: -1
trong mat trai
  rotate: false
  xy: 289, 285
  size: 39, 29
  orig: 39, 29
  offset: 0, 0
  index: -1
