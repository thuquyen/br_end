
z37.png
size: 96,96
format: RGBA8888
filter: Linear,Linear
repeat: none
Layer 100 copy
  rotate: false
  xy: 2, 41
  size: 79, 53
  orig: 79, 53
  offset: 0, 0
  index: -1
Layer 112
  rotate: true
  xy: 63, 25
  size: 14, 30
  orig: 14, 30
  offset: 0, 0
  index: -1
Layer 90
  rotate: false
  xy: 2, 13
  size: 29, 26
  orig: 29, 26
  offset: 0, 0
  index: -1
Layer 90 copy
  rotate: false
  xy: 33, 2
  size: 19, 19
  orig: 19, 19
  offset: 0, 0
  index: -1
Layer 97 copy
  rotate: true
  xy: 33, 23
  size: 16, 28
  orig: 16, 28
  offset: 0, 0
  index: -1
Shape 4 copy 2
  rotate: true
  xy: 83, 73
  size: 21, 11
  orig: 21, 11
  offset: 0, 0
  index: -1
