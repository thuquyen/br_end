
z17.png
size: 96,92
format: RGBA8888
filter: Linear,Linear
repeat: none
armor
  rotate: true
  xy: 81, 76
  size: 14, 11
  orig: 14, 11
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 70, 67
  size: 9, 23
  orig: 9, 23
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 40, 59
  size: 28, 31
  orig: 28, 31
  offset: 0, 0
  index: -1
outerglow
  rotate: false
  xy: 2, 8
  size: 36, 82
  orig: 36, 82
  offset: 0, 0
  index: -1
stomatch
  rotate: true
  xy: 62, 30
  size: 27, 26
  orig: 27, 26
  offset: 0, 0
  index: -1
wing
  rotate: true
  xy: 40, 19
  size: 38, 20
  orig: 38, 20
  offset: 0, 0
  index: -1
