
z34.png
size: 96,96
format: RGBA8888
filter: Linear,Linear
repeat: none
Ellipse 2 copy 3
  rotate: false
  xy: 66, 51
  size: 12, 12
  orig: 12, 12
  offset: 0, 0
  index: -1
Ellipse 2 copy 4
  rotate: false
  xy: 36, 28
  size: 12, 12
  orig: 12, 12
  offset: 0, 0
  index: -1
Layer 28 copy 2
  rotate: false
  xy: 50, 29
  size: 11, 11
  orig: 11, 11
  offset: 0, 0
  index: -1
Layer 31 copy 2
  rotate: true
  xy: 41, 65
  size: 26, 36
  orig: 26, 36
  offset: 0, 0
  index: -1
Layer 33 copy
  rotate: false
  xy: 2, 57
  size: 37, 34
  orig: 37, 34
  offset: 0, 0
  index: -1
Layer 33 copy 2
  rotate: false
  xy: 41, 42
  size: 23, 21
  orig: 23, 21
  offset: 0, 0
  index: -1
Layer 35 copy
  rotate: false
  xy: 2, 2
  size: 28, 19
  orig: 28, 19
  offset: 0, 0
  index: -1
Layer 45 copy 2
  rotate: false
  xy: 2, 23
  size: 32, 32
  orig: 32, 32
  offset: 0, 0
  index: -1
Layer 54 copy
  rotate: false
  xy: 79, 77
  size: 11, 14
  orig: 11, 14
  offset: 0, 0
  index: -1
