
z35.png
size: 144,144
format: RGBA8888
filter: Linear,Linear
repeat: none
111111
  rotate: false
  xy: 111, 112
  size: 31, 28
  orig: 31, 28
  offset: 0, 0
  index: -1
Ellipse 1 copy
  rotate: false
  xy: 53, 85
  size: 56, 55
  orig: 56, 55
  offset: 0, 0
  index: -1
Ellipse 2 copy
  rotate: true
  xy: 2, 7
  size: 3, 13
  orig: 3, 13
  offset: 0, 0
  index: -1
Layer 23 copy
  rotate: false
  xy: 53, 49
  size: 53, 34
  orig: 53, 34
  offset: 0, 0
  index: -1
Layer 24 copy
  rotate: false
  xy: 2, 12
  size: 37, 53
  orig: 37, 53
  offset: 0, 0
  index: -1
Layer 25 copy
  rotate: false
  xy: 2, 67
  size: 49, 73
  orig: 49, 73
  offset: 0, 0
  index: -1
Layer 31 copy
  rotate: false
  xy: 108, 51
  size: 21, 20
  orig: 21, 20
  offset: 0, 0
  index: -1
Layer 42 copy
  rotate: false
  xy: 111, 73
  size: 24, 37
  orig: 24, 37
  offset: 0, 0
  index: -1
