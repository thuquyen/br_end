
z27.png
size: 256,32
format: RGBA8888
filter: Linear,Linear
repeat: none
Layer 19 copy
  rotate: false
  xy: 59, 3
  size: 42, 22
  orig: 42, 22
  offset: 0, 0
  index: -1
Layer 21 copy
  rotate: true
  xy: 211, 12
  size: 13, 21
  orig: 13, 21
  offset: 0, 0
  index: -1
Layer 23 copy
  rotate: true
  xy: 103, 4
  size: 7, 23
  orig: 7, 23
  offset: 0, 0
  index: -1
Layer 34 copy
  rotate: false
  xy: 145, 16
  size: 40, 9
  orig: 40, 9
  offset: 0, 0
  index: -1
Layer 35
  rotate: false
  xy: 103, 13
  size: 40, 12
  orig: 40, 12
  offset: 0, 0
  index: -1
Layer 38 copy
  rotate: false
  xy: 187, 7
  size: 22, 18
  orig: 22, 18
  offset: 0, 0
  index: -1
Layer 8 copy
  rotate: true
  xy: 2, 2
  size: 23, 55
  orig: 23, 55
  offset: 0, 0
  index: -1
