
enemy_31.png
size: 156,156
format: RGBA8888
filter: Linear,Linear
repeat: none
1
  rotate: false
  xy: 106, 65
  size: 4, 8
  orig: 4, 8
  offset: 0, 0
  index: -1
2
  rotate: false
  xy: 2, 2
  size: 59, 61
  orig: 59, 61
  offset: 0, 0
  index: -1
3
  rotate: true
  xy: 106, 75
  size: 20, 44
  orig: 20, 44
  offset: 0, 0
  index: -1
4
  rotate: true
  xy: 106, 97
  size: 56, 37
  orig: 56, 37
  offset: 0, 0
  index: -1
5
  rotate: false
  xy: 115, 33
  size: 13, 18
  orig: 13, 18
  offset: 0, 0
  index: -1
6
  rotate: false
  xy: 2, 65
  size: 102, 88
  orig: 102, 88
  offset: 0, 0
  index: -1
7
  rotate: false
  xy: 63, 13
  size: 50, 50
  orig: 50, 50
  offset: 0, 0
  index: -1
a1
  rotate: false
  xy: 115, 53
  size: 20, 20
  orig: 20, 20
  offset: 0, 0
  index: -1
