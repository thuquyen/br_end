
enemyzone3.png
size: 256,256
format: RGBA8888
filter: Linear,Linear
repeat: none
eye
  rotate: false
  xy: 199, 195
  size: 5, 10
  orig: 5, 10
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 2, 26
  size: 61, 50
  orig: 61, 50
  offset: 0, 0
  index: -1
mouth
  rotate: false
  xy: 2, 2
  size: 51, 22
  orig: 51, 22
  offset: 0, 0
  index: -1
mouth2
  rotate: false
  xy: 148, 157
  size: 49, 48
  orig: 49, 48
  offset: 0, 0
  index: -1
oar
  rotate: false
  xy: 148, 124
  size: 12, 31
  orig: 12, 31
  offset: 0, 0
  index: -1
oar2
  rotate: false
  xy: 65, 58
  size: 36, 18
  orig: 36, 18
  offset: 0, 0
  index: -1
outerglow
  rotate: false
  xy: 2, 78
  size: 144, 127
  orig: 144, 127
  offset: 0, 0
  index: -1
