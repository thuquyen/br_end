class CustomGizmo extends Editor.Gizmo {

    init () {
        // Khởi tạo một số tham số
    }

    onCreateRoot () {
        // Tạo một callback cho nút gốc svg, bạn có thể tạo công cụ svg của mình tại đây
        // this._root có thể lấy nút gốc svg được tạo bởi Editor.Gizmo

        // ví dụ:

        // Tạo một công cụ svg
        // group documentation : http://documentup.com/wout/svg.js#groups
        this._tool = this._root.group();

        // vẽ một vòng tròn
        // circle documentation : http://documentup.com/wout/svg.js#circle
        let polyLine = this._tool.polyline([[0, 0], [0, 0]]).fill('none').stroke({ width: 1, color: '#f00'});

        // Xác định hàm paint cho công cụ, có thể là một tên khác
        this._tool.plot = (points, color) => {
            //this._tool.move(position.x, position.y);
            polyLine.plot(points).fill('none').stroke({width: 1, color: color});
        };
    }

    onUpdate () {
        // Cập nhật công cụ svg trong hàm này
        let points = this.target.startPath;
        let color = '#000';
        if(this.target.color) {
            color = this.target.color.toCSS('#rrggbb');
        }
        //Editor.log(JSON.stringify(points));
        if(points && points.length>=2) {
            //Editor.log(JSON.stringify(points));
            this._tool.plot(this.convertPointsToSvgSpace(points), color);
        }
    }

    convertToSvgSpace(p){
        let worldPosition = this.node.convertToWorldSpaceAR(p);
        let viewPosition = this.worldToPixel(worldPosition);
        let k = Editor.GizmosUtils.snapPixelWihVec2(viewPosition);
        return k;
    }

    convertPointsToSvgSpace(points){
        let newPoints = [];
        for(let i=0; i<points.length; i++) {
            let p = this.convertToSvgSpace(points[i]);
            newPoints.push([p.x, p.y]);
        }
        return newPoints;
    }

// 如果需要自定义 Gizmo 显示的时机，重写 visible 函数即可
//    visible () {
//        return this.selecting || this.editing;
//    }

// Gizmo 创建在哪个 Layer 中 : foreground, scene, background
// 默认创建在 scene Layer
//    layer () {
//        return 'scene';
//    }

// 如果 Gizmo 需要参加 点击测试，重写 rectHitTest 函数即可
//    rectHitTest (rect, testRectContains) {
//        return false;
//    }
}

module.exports = CustomGizmo;